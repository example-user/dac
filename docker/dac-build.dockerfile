FROM alpine:3.4

# Install SASS, Node/NPM, and misc utils
RUN echo "@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk --update add curl bash sassc@testing nodejs && \
    rm -f /var/cache/apk/*

# Install Hugo
ENV HUGO_VERSION 0.16
ENV HUGO_TAR hugo_${HUGO_VERSION}_linux-64bit.tgz

WORKDIR /tmp/
RUN mkdir hugo && cd hugo && \
    curl -L https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/${HUGO_TAR} -o ${HUGO_TAR} && \
    tar xzf ${HUGO_TAR} && \
    cp hugo /usr/local/bin/ && \
    cd .. && rm -rf hugo
