FROM debian:jessie

RUN apt-get update && \
    apt-get -y install apache2 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY micros/apache2 /etc/apache2
COPY public /var/www/html
RUN echo '{"status":"OK"}' > /var/www/html/healthcheck.json

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

EXPOSE 8080

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
