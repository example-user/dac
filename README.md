# DAC Next Generation re-platforming

This repository contains the code (and some test content) of the new
 `developer.atlassian.com` (aka `DAC-NG`). Please report all bugs here
 on [ean](https://ecosystem.atlassian.net/browse/DAC)

## Overview

This repository contains themes, templates and configuration for running
 a local server (Hugo) for static content that is generated from markdown
 files. The markdown in use includes a number of files that are
 automatically exported from selected spaces on DAC. The export scripts
 live in the `export/` directory and can be used to extract and transform
 fresh content. There are also docker/micros build scripts that define
 automated deployment, notably to a staging server where our team will
 collaborate throughout the project. 
 
In summary, there are components in this repo that cover each of the
steps in the migration lifecycle:

* Build React components (requires node.js, npm, sass)
* Export Confluence space as HTML from production DAC (requires python3)
* Clean HTML, rewriting links and cutting out extraneous stuff (requires python3)
* Transform the content into markdown (requires Pandoc)
* Build the static site with Hugo (requires Hugo)
* Run the static site in a web server (Hugo provides a small convenience-server for dev mode)
* Runtime integration with Atlassian ID and Discourse forum (_requires patience_)

### OSX users: 

Just get [homebrew](http://brew.sh/). It's a command-line tool that makes
 installing stuff way simpler. Only disregard this if you know you know
 better. Installation instructions as per homebrew web site:

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

### Linux users:

Help us make this doc better, best guess is the `sudo apt-get install <foo>`
 incantation, replacing `<foo>` for each dependency. 

## Basic installation

You need access to a command line or terminal of your choice. OSX and
 Linux have been verified, Windows is completely untested. 

In order to run the content in locally in the server, you need Hugo.
 Download the latest version of [Hugo](http://gohugo.io) from the
 [release page](https://github.com/spf13/hugo/releases) for your
 operating system.

It is a single binary that needs to be saved anywhere in your `PATH`.

For OS specific instructions read below.

### OSX Instructions

If you're using [homebrew](http://brew.sh/) you can install Hugo with:

    brew install hugo

Without `homebrew` you can just download the right binary
 [here](https://github.com/spf13/hugo/releases/download/v0.16/hugo_0.16_osx-64bit.tgz)
 and place it in a folder in your `PATH`.

### Python Configuration

To check if things are working for you, just run the automated tests:

    ./scripts/test.sh

If the tests fail it may be due to not having either Python 3.x or
 `virtualenv`. The content migration scripts are written in Python 3.x
 and it is advised to use `virtualenv` to avoid entanglement with
 Python 2.x. Python 3 is one louder. 

Assuming you have [homebrew](http://brew.sh/):

    brew install python3
    pip install virtualenv

Try running the tests again. In fact, make that a habit ;)

To configure python3 with virtualenv, do the following:

First time only, in the repo root directory:

    virtualenv -p python3 ENV
    source ENV/bin/activate
    pip install -r requirements.txt

Once in subsequent (bash) sessions, activate the python 3 virtualenv:

    source ENV/bin/activate

This puts the right versions of python and related tools (pip, py.test)
 first on your path. It also puts the right versions of the library
 dependencies in your `PYTHONPATH`, stored in the *environment directory*
 called, in this case, __ENV__. It also appears to prefix `PS1` with the
 environment name __ENV__. If you come back one day and everthing is
 messed up, check yourself. You may need to activate your `virtualenv`.

If you want your old python back (probably 2.6~) you probably already
 know this, but you just `. ENV/bin/deactivate` or start a new shell.
 

## Working on themes

If you want to be able to work on the look and feel of the project and
 modify the theme and stylesheets you will need to install also 
 [Sass](http://sass-lang.com/).

To understand how to work on the templates and change the look and feel
 have a look at the [Hugo Documentation](http://gohugo.io/templates/overview/).

### OSX Instructions

If you're using [homebrew](http://brew.sh/) you can install Sass with:

    brew install sassc

## Run the project

Run a live-reloading version of the project with:

    hugo server

This will output something like:

    $ hugo server
    
    Started building site
    0 draft content
    0 future content
    231 pages created
    485 non-page files copied
    144 paginator pages created
    139 categories created
    0 tags created
    in 570 ms
    Watching for changes in /Users/np/p/dacng/{data,content,static,themes}
    Serving pages from memory
    Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
    Press Ctrl+C to stop

And you will be able to access the generated site at <http://localhost:1313/>

### Generate the static site

To generate the static site from the theme, templates and markdowns type:

    hugo

### Export a Confluence space

You don't have to fetch fresh DAC content since there is a copy of some
 previously exported content from the *jiracloud* space. However, you can
 export and transform fresh content using the following command, where
 <spacename> is optional (it defaults to *jiracloud*):

    ./scripts/export-space.sh <spacename>

You will be prompted for your atlassian id password to authenticate with
 DAC - space export permission is not given to unauthenticated users. If
 your local username is not the same as your atlassian id username add 
 the `-u <username>` option.

The result of this export is fresh markdown content in the `content/`
 dir. Any existing or subsequent `hugo server` process will then be 
 serving the fresh content.


### Build React Components

The dynamic parts of the DAC website are rendered using [React](https://facebook.github.io/react/)
 and [Redux](https://github.com/reactjs/redux) (and others). If you're a
 developer contributing to the functionality of DAC here's how you build the
 frontend source and have a live reload development server.

- Install Node and npm if you don't already have them.
- Install the Javascript dependencies:

    cd themes/adg3
    npm install

- Run a live reload server with two shells open:

    #Shell 1 on the root of the `dacng` project
    cd <parent>/dacng
    hugo server
    
    #Shell 2
    cd themes/adg3
    npm run dev

The above will output something like:

    > adg3-react-theme@0.0.1 dev /Users/np/p/dacng/themes/adg3
    > webpack -d --watch
    
    Hash: 74aea38e46a11c05b6fa
    Version: webpack 1.13.1
    Time: 1328ms
            Asset    Size  Chunks             Chunk Names
        bundle.js  772 kB       0  [emitted]  main
    bundle.js.map  850 kB       0  [emitted]  main
        + 173 hidden modules

The above process will compress all the Javascript components into a
 single (soon) minified `bundle.js` saved at `themes/adg3/static/js/bundle.js`.
 This `bundle.js` is then referenced by the theme templates, right now
 from the `baseof.ace` template.

### Build in a Docker image

**Fill in**

### Run in a Docker image

**Fill in**

## Build and Deployment

DAC is hosted on Micros, in a Docker image containing the generated content and
 an Apache instance to serve it. Apache is used as this supports the existing
 redirects from the previous DAC, and has more flexibility for future
 redirects. The Micros instance includes CloudFront, so raw performance is not
 critical.

Important information:

* The Micros Dockerfile is `docker/micros-image.dockerfile`
* The Micros service descriptors are under `micros/service-descriptor.*.yaml`, one
  per environment.
* The relevant Apache config is under `micros/apache/`

## Continuous Deployment

Continuous Deployment is performed by Bamboo; see the plan `DAC NG
 Deploy`. Pipelines does not currently support building Docker images, and it's
 not clear if Git LFS is supported.  We *DO NOT* use the Bamboo Micros template,
 as we require some custom handling of our repo (mostly for Git LFS, see below),
 but it mostly follows the same pattern at that.

### Build phase

The main non-standard Bamboo setup we use is a custom clone/update script rather
 than Bamboo's native source checkout task. This is because Bamboo does not
 support Git LFS, and will in-fact break if used on an agent that has had `git
 lfs install` invoked on it. So we bypass this step completely. Instead, a script
 is used to invoke `git lfs clone` directly. Some important notes:

* The script is in `scripts/bamboo-git-lfs-clone.sh`
* The script must be copied to an inline script task in the Bamboo job.
* The script is documented in the header comment, however some key points:
    * An SSH key for the checkout must be placed the variable `dac.deploy.key.password`.
    * This key must be base-64 encoded to avoid Bamboo mangling; see header comment for the process.
    * This key *cannot* be a deployment key, as these are not supported by Bitbucket Git LFS.

Once checked-out, the content is built using a custom Docker image; see
 `scripts/build-site-in-docker.sh` for details. The Dockerfile for this image is
 `docker/dac-build.dockerfile`, and the script `scripts/build-build-env.sh`
 builds it if necessary.

Once the content is generated the Micros image is built and pushed with the script
 `/scripts/build-micros-image.sh`.

### Deploy phase

The deployment project `DAC Micros Deployment` is attached to the build project
 and will handle deploying images to Micros. This uses the same mechanisms as the
 Micros template. The main thing to be aware of is that a Micros deployment key
 is required; this is provided on the creation of the Micros instance and placed
 in the per-environment variable `micros.token.password`.
