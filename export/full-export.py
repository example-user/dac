#!/usr/bin/env python

import argparse
import logging as log
import os

import sys

import dac
import spider


def get_options():

    parser = argparse.ArgumentParser(description='Export to markdown a Confluence space')
    parser.add_argument('target',
                        metavar='target',
                        help='the target root page to extract navigation from')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        help='verbose: print out debug information')
    parser.add_argument('-c',
                        '--collect',
                        action='store_true',
                        help='verbose: collect all links in a space')
    parser.add_argument('-d',
                        '--download',
                        action='store_true',
                        help='verbose: download all pages')
    parser.add_argument('-i',
                        '--images',
                        action='store_true',
                        help='verbose: download images')
    parser.add_argument('-t',
                        '--convert',
                        action='store_true',
                        help='verbose: convert downloaded htmls to markdown')
    parser.add_argument('-n',
                        '--navigation',
                        action='store_true',
                        help='verbose: generate yaml of hierarchy')
    options = parser.parse_args()

    log.basicConfig(level=log.DEBUG if options.verbose else log.INFO, format=dac.LOG_FORMAT)
    return options


def main():
    host = 'https://developer.atlassian.com'
    enforce_prefix = '/jiracloud'
    enforce_suffix = '.html'      # used for identifying "valid" links
    tmp_export_path = './tmp' + enforce_prefix

    unique_pages = {}
    opt = get_options()
    s = spider.Spider(host, enforce_prefix, enforce_suffix, 0)
    root = opt.target
    if opt.collect:
        unique_pages = s.collect_unique_links(root)
    if opt.download and unique_pages:
        os.makedirs(tmp_export_path, exist_ok=True)
        s.download_links(unique_pages, tmp_export_path, opt.images)
    if opt.convert:
        t = dac.Transformer(tmp_export_path)
        t.transform_all_html(tmp_export_path)
        t.convert_html_to_markdown('./content/jiracloud')
    if opt.navigation:
        os.system(
            './export/api-walker.py https://developer.atlassian.com/rest/api/content/39375886 > data/hierarchy.yml')

if __name__ == '__main__':
    sys.exit(main())
