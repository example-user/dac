#!/usr/bin/env python

import argparse
import logging as log
import sys

import dac

"""

Cleans the html from a Confluence space export and converts it to markdown.

"""


def get_options():
    parser = argparse.ArgumentParser(description='Clean a Confluence HTML export tree')
    parser.add_argument('root', metavar='root', help='the root dir of the Confluence HTML space export')
    parser.add_argument('base', metavar='base', help='the base url for the space')
    parser.add_argument('dest', metavar='dest', help='the dest dir for the converted markdown')
    parser.add_argument('-v', '--verbose', action='store_true', help='verbose: print out debug information')
    options = parser.parse_args()
    log.basicConfig(level=log.DEBUG if options.verbose else log.INFO, format=dac.LOG_FORMAT)
    return options


def main():
    opt = get_options()
    t = dac.Transformer(opt.root, opt.base)
    t.backup_html = True
    t.transform_all_html(opt.dest)

    if len(t.content_problems) > 0:
        log.info("generating problem report")
        with open('content-problem-report.html', 'w+') as html_file:
            print(t.content_problem_report(), file=html_file)

    log.info("generating link report")
    link_report = '\n'.join(sorted(t.all_links))
    with open('link-report.txt', 'w+') as markdown_file:
        print(link_report, file=markdown_file)

    t.convert_html_to_markdown(opt.dest)

if __name__ == '__main__':
    sys.exit(main())
