import glob
import io
import logging as log
import os
import re
import shutil
import xmlrpc.client
import zipfile
from distutils.dir_util import copy_tree
from distutils.spawn import find_executable
from html import escape

import pypandoc
import requests
from pyquery import PyQuery
from bs4 import BeautifulSoup
from requests.auth import HTTPBasicAuth

"""
Library functions for handling Confluence space export and transform to static markdown tree.
"""

# per-file YAML metadata:
MARKDOWN_HEADER = '''---
title: %s
aliases:
    - %s
---
'''

CONTENT_PROBLEM_REPORT_TEMPLATE = """
<html><head><title>DAC Problem Report</title></head><body>
<h1>DAC Problem Report</h1>
<dl>
%s
</dl></body></html>
"""

BUFFER_SIZE = 1024  # in bytes
REPORT_DELTA = 1024 * 1024  # only log every meg downloaded
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


def unzip(dest, r):
    """
    Processor that unzips the response stream to the destination folder.
    :param dest: target dir for unzipping into.
    :param r: "Requests" response object form which the content is ready to be streamed.
    """
    fname = re.findall("filename=(.+)", r.headers['content-disposition']) or "export.zip"
    log.info("unzipping file %s", fname)
    export_zip = zipfile.ZipFile(io.BytesIO(r.content))
    export_zip.extractall(dest)


def save(dest, r):
    """
    Processor that saves the file stream into its filename into the given filename.
    :param dest: target directory for saving.
    :param r: "Requests" response object from which the content is ready to be streamed.
    """
    written = 0
    filename = re.findall("filename=(.+)", r.headers['content-disposition']) or "export.zip"
    with open(dest + "/" + filename, 'wb') as fd:
        for chunk in r.iter_content(BUFFER_SIZE):
            written += BUFFER_SIZE
            if written % REPORT_DELTA == 0:
                log.debug("%s: downloading %d bytes", filename, written)
            fd.write(chunk)


def get_stream(url, auth, dest, processor=unzip):
    """
    Retreive the given `url` and process it with the given processor.
    :param url: the url to process
    :param auth: any authentication
    :param dest: the destination folder for the content to be stored.
    :param processor: callback function that takes args destination, response.
    """
    # disble gzip Content-Encoding because borked with the iter_content streaming method
    r = requests.get(url, auth=auth, stream=True, headers={'Accept-Encoding': 'identity'})
    if r.status_code == 200:
        processor(dest, r)
    else:
        log.warning("FAILED to download, got error: %s %s", r.status_code, r.content)
        raise Exception("Failed to get url %s response %s", url, r.status_code)


class Confluence:
    def __init__(self, base_url, username, password):
        if not base_url.startswith("https://"):
            log.warning("INSECURE BASE URL may not work: %s", base_url)
        self.base_url = base_url
        self.conf = xmlrpc.client.ServerProxy(base_url + "/rpc/xmlrpc")
        self.bauth = HTTPBasicAuth(username, password)
        log.debug("Authenticating")
        self.token = self.conf.confluence1.login(username, password)

    def export_space_html(self, space):
        return self.conf.confluence1.exportSpace(self.token, space, "TYPE_HTML")

    def get_spaces(self):
        return self.conf.confluence1.getSpaces(self.token)

    def fetch_space(self, space, destination, handler):
        download_url = self.export_space_html(space)
        log.info("downloading export of '%s' space from %s", space, download_url)
        get_stream(download_url, self.bauth, destination, handler)


class Transformer:
    MAIN_HEADING = 'h1#title-heading'
    MAIN_CONTENT = 'div#main-content'

    def __init__(self, root, base=""):
        """
        Robots in de skiez.
        :param root: root of the local file tree to transform.
        :param base: prefix of the urls to which local links are rewritten to.
        """
        self.root = root
        self.base = base

        self.all_links = set()

        # list of file, problem tuples
        self.content_problems = []

        # list of (src,dest) tuples
        self.link_graph = []

        # current src for next found link
        self.current_file = None
        self.backup_html = True

    def transform_all_html(self, dest):
        """
        Do all cleaning and link rewriting for the root to `dest`
        """

        log.debug("copying images and other attachments")
        self.copy_binaries(dest)

        log.info("Transforming all html from %s to %s", self.root, dest)
        self.for_all_html(self.detect_content_problems,
                          self.clean_dom,
                          self.clean_spans,
                          self.rewrite_image_links,
                          self.transform_links,
                          self.remove_unwanted_classes)

        log.debug("Cleaning filenames")
        self.clean_filenames()

    def copy_binaries(self, dest):
        for d in ["/images", "/attachments", "/styles"]:
            copy_tree(self.root + d, dest + d)

    def convert_html_to_markdown(self, dest):
        """
            Convert all html to correspondingly named markdown in `dest`.
        """
        if not find_executable("pandoc"):
            log.error("You need to install pandoc!")
        for fpath in glob.glob('%s/*.html' % self.root):
            # write the markdown header
            fname = os.path.split(fpath)[-1]
            basename = os.path.splitext(fname)[0]
            new_name = '%s/%s' % (dest, basename + '.md')
            # TODO use the <h1> instead of the filename

            title = self.titlefy(basename)
            markdown = MARKDOWN_HEADER % (title, self.base + "/" + fname)
            log.info("Converting HTML to markdown: %s", new_name)
            with open(fpath) as f:
                html = f.read()
            markdown += self.html_string_to_markdown(html)
            with open(new_name, 'w+') as markdown_file:
                print(markdown, file=markdown_file)

    def clean_filenames(self):
        """
            Rename all the html files in the given directory to the dac url convention
        """
        # for each file rename, lowercase and reduce multi dashes to one
        for f in glob.glob('%s/*.html' % self.root):
            fname = os.path.split(f)[-1]
            # all lower case and reduce multiple dashes to single dash characters
            os.rename(self.root + '/' + fname, self.root + '/' + self.confluence_link_nicer(fname))

    def for_all_html(self, *processors):
        """
        For all html files under root, transform their content using each of the
        given whole file content processors.
        :param processors: fn(string=>string) which modifies html.
        """
        for f in glob.glob('%s/*.html' % self.root):
            log.info("processing file %s", f)
            self.current_file = f
            if self.backup_html:
                self.backup_file(f)
            with open(f) as html:
                content = html.read()
                in_count = content.count('\n')
                for processor in processors:
                    content = processor(content)
                out_count = content.count('\n')
                log.debug("file reduction: %d -> %d (%d)", in_count, out_count, out_count - in_count)
            with open(f, 'w+') as html_file:
                print(content, file=html_file)

    def for_all_html_lines(self, line_processor):
        """
            For all html files under root, transform the contents using `line_processor`
            :param line_processor: function that takes an iterable of lines and returns lines
        """

        def line_to_whole(whole):
            lines = whole.split('\n')
            processed = line_processor(lines)
            return '\n'.join(processed)

        self.for_all_html(line_to_whole)

    def detect_content_problems(self, html):
        """
        HTML whole file processor that returns its input unmodified, collects content problems for reporting
        on in self.content_problem_report()
        :param html: the html file to check for problems in
        :return: the html unchanged.
        """
        expand_macro = re.compile(r'class="[^"]*\bexpand-control\b.*"', re.S | re.I)
        if len(re.findall(expand_macro, html)) > 0:
            self.register_content_problem("expand macro found")
        d = PyQuery(html)

        for expected in [Transformer.MAIN_CONTENT, Transformer.MAIN_HEADING]:
            count = len(d(expected))
            if count != 1:
                self.register_content_problem("expected 1 '%s', found %s" % (expected, count))

        # TODO: test for html macro (if possible) / consider using REST API for content checking?
        return html

    def register_content_problem(self, desc):
        self.content_problems.append((self.current_file, desc))

    def rewrite_image_links(self, html):
        log.debug("rewriting image links")
        pat = re.compile(r'(<img[^>]*\ssrc=")([^">]+)("[^>]*>)', re.I | re.S)
        repl = r'\1%s/\2\3' % self.base
        if re.match(pat, html):
            log.debug("MATCH IMAGE")
        new_html = re.sub(pat, repl, html)
        return new_html

    def transform_links(self, html):
        internal_link = re.compile(r'(<a[^>]+href=")([^">]+\.html)("[^>]*>)', re.I | re.S)
        result = re.sub(internal_link, self.fix_internal_link, html)
        return result

    def store_link(self, link):
        self.all_links.add(link)
        if self.current_file:
            self.link_graph.append((self.current_file, link))

    def content_problem_report(self):
        """
        Returns a rendered html report of content problems found during file processing.
        """
        item = "<dt>%s</dt><dd>%s</dd>"
        report = CONTENT_PROBLEM_REPORT_TEMPLATE
        return report % "\n".join(map(lambda p: item % (escape(p[0]), escape(p[1])), self.content_problems))

    @staticmethod
    def clean_line(l):
        # remove extraneous confluence classes
        cuts = [
            ' class="conf-macro output-inline"',
            ' class="confluence-embedded-image conf-macro output-inline"',
            ' class="confluence-embedded-image confluence-content-image-border"',
            ' class="confluence-embedded-image confluence-content-image-border conf-macro output-inline"',
            ' class="emoticon emoticon-information"',
            ' class="emoticon emoticon-warning"',
        ]
        for c in cuts:
            l = l.replace(c, '')
        # remove noisy spans
        l = re.sub('<span[^>]*?>\s*(.*?)\s*</span>', '\\1', l)
        # TODO: review these two transforms, they no longer look like a good idea
        # strip widths out of (some) img tags and height out of lots of stuff
        # l = re.sub('<img(\s*width="[^"]*")', '<img', l)
        # l = re.sub('\s*height="[^"]*"', '', l)
        return l

    def clean_lines(self, lines):
        """
            Process the given lines of html, returning cleaned lines.
            Cuts out everything before the first h1 tag and everything from the feedback block (like this content?)
            onwards.
            :param lines: iterable of lines to process
            :returns: iterable of cleaned lines
        """
        # TODO remove this method
        result = []
        skip = True
        found_header = False
        found_footer = False
        for l in lines:
            if skip and l.count('<h1') == 1:
                skip = False
                found_header = True
            if not skip and (l.count('id="feedback-block"') > 0 or l.count('<div id="footer"') > 0):
                found_footer = True
                skip = True
            elif not skip:
                if l.count('<div') <= 0:
                    l = self.clean_line(l)
                result.append(l)
        if not found_header:
            log.warning("Never found HEADER when cleaning html")
        if not found_footer:
            log.warning("Never found FOOTER when cleaning html")
        in_count = len(lines)
        out_count = len(result)
        log.debug("Cleaned lines from %d to %d (%d)", in_count, out_count, out_count - in_count)
        return result

    def fix_internal_link(self, match):
        link = match.group(2)
        self.store_link(link)
        if os.path.isfile(os.path.join(self.root, link)):
            log.debug("Rewriting link %s", link)
            nicer = self.confluence_link_nicer(link)
            return match.group(1) + self.base + '/' + nicer + match.group(3)
        else:
            return match.group(0)

    @staticmethod
    def remove_unwanted_classes(html):
        unwanted_classes = [
            'conf-macro',
            'confluence-embedded-image',
            'output-inline',
            'confluence-content-image-border',
            'emoticon',
            'emoticon-information',
            'emoticon-warning',
        ]
        d = PyQuery(html)("*")
        for c in unwanted_classes:
            d.each(lambda i, e: PyQuery(e).remove_class(c))
        return d.outer_html()

    @staticmethod
    def clean_dom(html):
        d = PyQuery(html)
        # select only the h1 and the div with the main content
        # this removes all the unneeded header and footer gumpf
        h1 = d(Transformer.MAIN_HEADING)
        h1 = h1.html(h1.text()).outer_html()
        main_content = d(Transformer.MAIN_CONTENT).outer_html()
        new_html = h1 + main_content
        return new_html

    @staticmethod
    def clean_spans(html_with_spans):
        soup = BeautifulSoup(html_with_spans, 'html.parser')
        for t in soup.select('span'):
            t.unwrap()
        return str(soup)

    # TODO remove this method
    @staticmethod
    def join_multiline_h1(text):
        """
        Joins together h1 tag pairs that go over multiple lines.
        :return: the joined string
        """
        whitespace = re.compile(r'[\n\s]+', re.S)
        h1s = re.compile('(<h1[^>]*>.*?</h1>)', re.S)

        def join_lines(m):
            return re.sub(whitespace, ' ', m.group(1))

        return re.sub(h1s, join_lines, text)

    @staticmethod
    def titlefy(basename):
        """
        Make a page title with correct case from the base filename.
        :param basename: the filename without the extension or leading path.
        :return: a nice page title
        """
        t = basename.replace('-', ' ').title()
        # title() doesn't know acronyms or abbreviations
        t = re.sub(r'\bUi(s?)\b', "UI\\1", t)
        t = re.sub(r'\bRest Api(s?)\b', "REST API\\1", t)
        t = re.sub(r'\bApi(s?)\b', "API\\1", t)
        t = re.sub(r'\bJira\b', "JIRA", t)
        t = re.sub(r'\bOauth\b', "OAuth", t)
        # lower case special cases
        t = re.sub(r'(?<!^)\b(The|A|An|And|For|In|Is|Via|With|To)\b', lambda m: m.group(1).lower(), t)
        return t

    @staticmethod
    def backup_file(f):
        new_name = f + "~"
        index = 1
        while os.path.isfile(new_name):
            new_name = "%s~%d" % (f, index)
            index += 1
        shutil.copyfile(f, new_name)

    @staticmethod
    def html_string_to_markdown(html):
        """
            Converts the given `html` string into github markdown.
        """
        args = ['--atx-headers', '--wrap=none', '--reference-links']
        return pypandoc.convert_text(html, 'markdown_github', format='html', extra_args=args)

    @staticmethod
    def confluence_link_nicer(link):
        """
            Process the given string from Confluence page URLs to the nice url format preferred on dac.
            Sequences of dash (-) are reduced to single dash and all to lower case.
        """
        return re.sub(r'(--+|_)', r'-', link.lower())
