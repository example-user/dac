import logging as log
import os
import re
import shutil
import time
import urllib.parse

import requests


class Spider:

    REGEX_HREF_URL = r'href=[\'"]?([^\'" >]+)'

    """
    HTTP client for spidering and saving exporting html content trees.
    """
    def __init__(self, host, enforce_prefix, enforce_suffix, fetch_sleep=1):
        self.host = host
        self.enforce_prefix = enforce_prefix
        self.enforce_suffix = enforce_suffix
        self.fetch_sleep = fetch_sleep

    @staticmethod
    def download_binary(url, dest):
        """
            Download `url` as binary in streaming to selected path.
        """
        r = requests.get(url, stream=True)
        if r.status_code == 200:
            with open(dest, 'wb') as f:
                r.raw.decode_content = True
                shutil.copyfileobj(r.raw, f)

    def download_images_in(self, content, dest):
        images = re.findall(r'<img[^>]*?src\s*=[\'"]?([^\'" >]+)', content)
        uniq_images = {}
        for u in images:
            if u not in uniq_images:
                uniq_images[u] = 1
        for i in uniq_images:
            if i.count(self.enforce_prefix) > 0:
                folder = os.path.split(urllib.parse.urlparse(i).path)[0]
                img_folder = os.path.join(dest, folder[1:])
                full_url = "%s%s" % (self.host, i)
                img_path = os.path.join(dest, urllib.parse.urlparse(i).path[1:])
                if not os.path.exists(img_path):
                    log.debug('Creating folder: %s', img_folder)
                    os.makedirs(img_folder, exist_ok=True)
                    log.info('Downloading image: %s', i)
                    time.sleep(self.fetch_sleep)
                    self.download_binary(full_url, img_path)

    def download_links(self, pages, dest, download_images):
        if not os.path.exists(dest):
            os.mkdir(dest)
        for page_url in pages:
            log.debug(' '.join((page_url, ' - ', pages[page_url])))
            time.sleep(self.fetch_sleep)
            try:
                content = requests.get(page_url).text
            except requests.exceptions.ConnectionError as e:
                log.error('Error downloading page: %s', e)
                continue
            if download_images:
                self.download_images_in(content, './content')
            else:
                log.debug("skipping images")
            path = os.path.join(dest, pages[page_url])
            log.debug('Writing ' + path)
            with open(path, 'w') as img:
                print(content, file=img)

    def collect_unique_links(self, root):
        unique_pages = {}
        urls = self.extract_valid_links(root)
        for u in urls:
            full_url = "%s%s" % (self.host, u)
            if full_url not in unique_pages.keys():
                unique_pages[full_url] = full_url.split('/')[-1]
                if self.fetch_sleep > 0:
                    time.sleep(self.fetch_sleep)
                log.debug(full_url)
                second_level_urls = self.extract_valid_links(full_url)
                for u2 in second_level_urls:
                    full_url2 = "%s%s" % (self.host, u2)
                    if full_url2 not in unique_pages.keys():
                        unique_pages[full_url2] = full_url2.split('/')[-1]
                        log.debug('\t - %s', full_url2)
                        log.debug(unique_pages[full_url2])
        return unique_pages

    def extract_valid_links_from_html(self, html):
        result = []
        urls = re.findall(self.REGEX_HREF_URL, html)
        for u in urls:
            if u.count(self.enforce_prefix) > 0 and u.count(self.enforce_suffix) > 0:
                result.append(u)
        return result

    def extract_valid_links(self, url):
        page = requests.get(url)
        return self.extract_valid_links_from_html(page.text)
