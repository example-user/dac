import os
from pyquery import PyQuery

import dac

from bs4 import BeautifulSoup

sample_html = """
    <html>
    <head>
    <title>test data</title>
    </head>
    <body>
    <h1 id="title-heading"><span class="junk">big title</span></h1>

    <div id="main-content">
        <li class="first">
            <span><a href="index.html">JIRA Cloud</a></span>
        </li>
        <li>
            <span><a href="JIRA-Cloud-for-Developers_39375886.html">JIRA Cloud for Developers</a></span>
        </li>
        <p>This is some compelling developer-facing content.</p>
    </div>

    <div class="aui-page-panel-inner" id="feedback-block">
        Don't need this stuff!
    </div>
    </body>
    </html>
"""


def test_link_nicer():
    t = dac.Transformer
    nicer = t.confluence_link_nicer
    assert nicer("") == ""
    already_good = "/jiracloud/jira-platform-tutorials-39987044.html"
    assert nicer(already_good) == already_good
    link = "JIRA-Cloud-for-Developers_39375886.html"
    expected = "jira-cloud-for-developers-39375886.html"
    assert nicer(link) == expected


def test_simple_transform_links_in_lines():
    # TODO crappy unit test that writes to disk
    os.makedirs("tmp", exist_ok=True)
    html_file = "tmp/JIRA-Cloud-for-Developers_39375886.html"
    with open(html_file, "w") as mock_file:
        print("nothing to see here", file=mock_file)

    t = dac.Transformer("tmp")
    lines = ''.join(['<a href="JIRA-Cloud-for-Developers_39375886.html">',
                     '<a href="JIRA-Service-Desk-Cloud-development_39981106.html">'])
    transformed = t.transform_links(lines)
    os.remove(html_file)
    assert transformed.count("JIRA-Cloud-for-Developers_39375886.html") == 0
    assert transformed.count("jira-cloud-for-developers-39375886.html") == 1
    # because these files do not exist, we don't want to transform these links
    assert transformed.count("JIRA-Service-Desk-Cloud-development_39981106.html") == 1
    assert transformed.count("jira-service-desk-cloud-development-39981106.html") == 0


def test_clean_dom():
    t = dac.Transformer("tmp")
    clean = t.clean_dom(sample_html)
    assert clean.count("Don't need this stuff!") == 0
    assert clean.count("body") == 0
    assert clean.count("This is some compelling developer-facing content.") == 1
    assert clean.count('<div id="main-content">') == 1
    assert clean.count('<h1 id="title-heading">big title</h1>') == 1
    expected = """<h1 id="title-heading">big title</h1>
    <div id="main-content">
        <li class="first">
            <span><a href="index.html">JIRA Cloud</a></span>
        </li>
        <li>
            <span><a href="JIRA-Cloud-for-Developers_39375886.html">JIRA Cloud for Developers</a></span>
        </li>
        <p>This is some compelling developer-facing content.</p>
    </div>
    """
    assert_html_same(expected, clean)


def test_clean_html():
    t = dac.Transformer("tmp")
    clean = ''.join(t.clean_lines(sample_html.split("\n")))
    assert clean.count("Don't need this stuff!") == 0
    assert clean.count("body") == 0
    assert clean.count("<span") == 0
    assert clean.count("This is some compelling developer-facing content.") == 1


def test_clean_line():
    t = dac.Transformer("tmp")
    cl = t.clean_line
    assert cl("") == ""
    assert cl("hello") == "hello"
    assert cl('<img width="300" height="123" src="foo.jpg">') == '<img width="300" height="123" src="foo.jpg">'
    assert cl('<div class="conf-macro output-inline" id="whatevs">') == '<div id="whatevs">'


def test_titlefy():
    basename = "the-rest-api-is-surely-a-jira-api-for-the-ui-yo-12345"
    # TODO I think we may want sentence case, not title case
    assert dac.Transformer.titlefy(basename) == "The REST API is Surely a JIRA API for the UI Yo 12345"


def test_join_multiline_h1():
    jm = dac.Transformer.join_multiline_h1

    simple = '<h1 id="foo">\nYeah</h1>'
    assert jm(simple) == '<h1 id="foo"> Yeah</h1>'
    multiline = """<h1 id="title-heading" class="pagetitle">
        <span id="title-text">
            JIRA Cloud : Authentication and authorization                           </span>
    </h1>"""
    expected = '<h1 id="title-heading" class="pagetitle"> <span id="title-text"> JIRA Cloud : Authentication and authorization </span> </h1>'
    actual = jm(multiline)
    assert actual.count('\n') == 0
    assert actual == expected




def assert_html_same(expected, actual):
    expected_soup = BeautifulSoup(expected.strip(), 'html.parser').prettify()
    actual_soup = BeautifulSoup(actual.strip(), 'html.parser').prettify()
    assert expected_soup == actual_soup


def test_clean_spans():
    html_with_spans = """
    <div><p>This is legit.</p>
        <p><span class="this doll has voodoo properties"><b>The</b> Im<b>balm</b>er</span>. Hair are our aerials.</p>
        <span class="scrubbers">The carrot</span>.
    </div>
    """

    actual = dac.Transformer.clean_spans(html_with_spans)
    assert actual.count('<span') == 0
    expected = """
        <div><p>This is legit.</p>
            <p><b>The</b> Im<b>balm</b>er. Hair are our aerials.</p>
            The carrot.
        </div>
    """
    assert_html_same(expected, actual)



