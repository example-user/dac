#!/usr/bin/env bash

# This script is not invoked directly, but needs to be copied into a
# script task in Bamboo, in place of Bamboo's native Git clone.
#
# Bamboo does not support Git LFS at the moment (see BAM-17160), so we
# need to do a manual clone. To do this we need an SSH key, which must
# be placed into the plan variable "dac.deploy.key.password"
# ('password` enables hiding of the contents).
#
# NOTE: This key cannot be a deployment key, as Bitbucket LFS doesn't
# support these yet (see BB-16002).
#
# Unfortunately, Bamboo mangles multiline variables, and SSH is
# somewhat sensitive about the key format. To prevent this the key
# should be base64 encoded with no linewrapping:
#
#     base64 -w0 id_rsa
#
# The resulting string is then stored in a temporary file. We use
# GIT_SSH_COMMAND to override the default key during the clone.  We
# also use GIT_LFS_SKIP_SMUDGE in favour of a manual LFS pull to speed
# things up.

echo "+++ Pulling repository key from environment"
idfile=`mktemp`
echo "$bamboo_dac_deploy_key_password" | base64 -d > $idfile

export GIT_SSH_COMMAND="ssh -o IdentityFile=$idfile"

if [ -d .git/ ]; then
    echo "+++ Existing Git LFS repository, pulling"
    export GIT_LFS_SKIP_SMUDGE=1
    git pull -q && git lfs pull
else
    echo "+++ Cloning Git LFS repository"
    # We should be able to do `git lfs clone ..` here, but there's a
    # regression in Git LFS 1.3 that breaks checking out into the
    # current directory. The workaround is to do a non-LFS clone,
    # followed by an LFS pull, which is roughly equivalent to an LFS
    # clone.
    export GIT_LFS_SKIP_SMUDGE=1
    git clone -q git@bitbucket.org:atlassian/dac.git .
    git lfs pull

fi
git checkout "${bamboo.planRepository.branchName}"

echo "+++ Cleaning up"
rm -f $idfile
