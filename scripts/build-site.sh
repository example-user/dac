#!/bin/bash

for i in themes/*; do
    echo "====== Building theme $i ====="
    pushd $i

    echo "++ Building CSS"
    [ ! -d static/css/ ] && mkdir -p static/css/
    sassc src/sass/app.scss > static/css/app.css

    if [ -f package.json ]; then
        echo "++ Building Node/NPM packages"
        npm install && npm run build
    fi

    popd
    echo "++ Done"
done

echo "===== Building site with theme adg3 ====="
hugo --theme=adg3
