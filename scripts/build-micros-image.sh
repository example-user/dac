#!/bin/bash

REPO=docker.atlassian.io/atlassian/dac-static

docker build --tag ${REPO}:latest -f docker/micros-image.dockerfile .

docker push ${REPO}:latest
