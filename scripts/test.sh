#!/usr/bin/env bash

# advice from https://extranet.atlassian.com/display/RELENG/HOWTO+-+Use+a+custom+Python+version+or+Python+modules

if [[ "$1" == "-b" ]]; then

    export PATH="/opt/bamboo-agent/.pyenv/bin:/opt/bamboo-agent/.pyenv/shims:$PATH"

    curl -L https://bitbucket.org/atlassian/pyenv-installer/raw/master/bin/pyenv-installer | bash

    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
    pyenv install -s 3.5.2
    pyenv global 3.5.2
fi

if [[ ! -d ENV ]]; then
    echo creating virtualenv
    virtualenv -p python3 ENV
fi
if [[ -z "$VIRTUAL_ENV" ]]; then
    echo activating virtualenv
    . ENV/bin/activate
fi

echo installing requirements
pip install -q -r ./requirements.txt

mkdir -p test-reports
echo running actual tests
PYTHONPATH=`dirname $0`/../export py.test -v --junit-xml=test-reports/dac-tests.xml export

