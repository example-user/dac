#!/bin/bash

REPO=docker.atlassian.io/atlassian/dac-build

docker run -i --rm \
       -v `pwd`:/data \
       ${REPO} \
       /bin/bash -c "cd /data  && ./scripts/build-site.sh"
