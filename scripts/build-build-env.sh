#!/bin/bash

REPO=docker.atlassian.io/atlassian/dac-build

docker build --tag ${REPO} -f docker/dac-build.dockerfile .

docker push ${REPO}

