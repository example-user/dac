import { combineReducers } from 'redux'
import { REQUEST_AID_USER, RECEIVE_AID_USER } from '../actions'

const initialState = 0;

const counter = (state = initialState, action) => {
  switch (action.type) {
    case 'INCREASE':
      return state + 1
    case 'DECREASE':
      return state - 1
    default:
      return state
  }
}

function user(state = {
    isFetching: false,
    didInvalidate: false,
    user: {}
  }, action) {
  switch (action.type) {
    case REQUEST_AID_USER:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case RECEIVE_AID_USER:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        user: action.user,
        lastUpdated: action.receivedAt
      })
    default:
      return state
  }
}

const rootReducer = combineReducers({
  counter,
  user
})

export default rootReducer
