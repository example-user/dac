// This file bootstraps the entire application.

import React from 'react'
import { render } from 'react-dom'
import App from './containers/App'

render(
  <App/>,
  document.getElementById('react'))

window.React = require('react')
