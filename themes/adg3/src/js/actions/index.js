import fetch from 'isomorphic-fetch'

export const increase = () => {
  return {
    type: 'INCREASE'
  }
};

export const decrease = () => {
  return {
    type: 'DECREASE'
  }
};

export const REQUEST_AID_USER = 'REQUEST_AID_USER'
function requestAIDUser() {
  return {
    type: REQUEST_AID_USER
  }
}

export const RECEIVE_AID_USER = 'RECEIVE_AID_USER'
function receiveAIDUser(json) {
  return {
    type: RECEIVE_AID_USER,
    user: json,
    receivedAt: Date.now()
  }
}

export function fetchAIDUser() {

  // Thunk middleware knows how to handle functions.
  // It passes the dispatch method as an argument to the function,
  // thus making it able to dispatch actions itself.
  return function (dispatch) {

    // First dispatch: the app state is updated to inform
    // that the API call is starting.

    dispatch(requestAIDUser());

    // The function called by the thunk middleware can return a value,
    // that is passed on as the return value of the dispatch method.

    // In this case, we return a promise to wait for.
    // This is not required by thunk middleware, but it is convenient for us.
    return fetch(process.env.AID_SERVER + `/profile/rest/profile/current`, {credentials: 'include', mode: 'cors' })
    //return fetch(`/json-tests/user.json`)
      .then(function(response) {
          return response.json()
      })
      .then(function(json) {
        // We can dispatch many times!
        // Here, we update the app state with the results of the API call.
        dispatch(receiveAIDUser(json))
      });

      // TODO: catch any error in the network call.
  }
}
