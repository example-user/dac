import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { fetchAIDUser } from '../actions'

class AsyncUser extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { dispatch, user } = this.props
    dispatch(fetchAIDUser())
  }

  render() {
    const { user, isFetching, lastUpdated } = this.props
    const avatarUrl = "https://avatar-cdn.dev.internal.atlassian.com" + "/" + user.user.emailHash + "?s=200"
    return (
      <div>
        {isFetching && user.user != undefined &&
          <h2>Loading...</h2>
        }
        {user.user != undefined &&
          <ul style={{ opacity: isFetching ? 0.5 : 1 }}>
            <li>Username: {user.user.username}</li>
            <li>Email: {user.user.email}</li>
            <li>Title: {user.user.title}</li>
            <img src = {avatarUrl} />
          </ul>
        }
        {lastUpdated &&
          <div>
            Last updated at {new Date(lastUpdated).toLocaleTimeString()}.
            {' '}
          </div>
        }
      </div>
    )
  }
}

AsyncUser.propTypes = {
  user: PropTypes.any.isRequired,
  isFetching: PropTypes.bool.isRequired,
  lastUpdated: PropTypes.number,
  dispatch: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { user } = state
  const {
    isFetching,
    lastUpdated
    } = user || {
    isFetching: true,
    user: {}
  }

  return {
    user,
    isFetching,
    lastUpdated
  }
}

export default connect(mapStateToProps)(AsyncUser)
