import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { increase, decrease } from '../actions'
import Button from '../components/Button'

const mapStateToProps = (state, ownProps) => {
  return {
    value: state
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClickIncrease: () => {
      dispatch(increase())
    },
    onClickDecrease: () => {
      dispatch(decrease())
    }
  }
}


const StatelessCounterDashboard = ({ value, onClickIncrease,
                            onClickDecrease }) => {
  const incText = 'Increase!';
  const decText = 'Decrease!';
  return (
    <div>
      <a href="#"  className="aui-button" aria-disabled="true">{value}</a>
      <Button text={incText} onClick={onClickIncrease} />
      <Button text={decText} onClick={onClickDecrease} />
    </div>
  )
}

const CounterDashboard = connect(
  mapStateToProps,
  mapDispatchToProps
)(StatelessCounterDashboard)


export default CounterDashboard
