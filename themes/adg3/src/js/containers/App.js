import React from 'react';
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import { createStore, applyMiddleware } from 'redux'

import { fetchAIDUser } from '../actions'
import rootReducer from '../reducers'
import CounterDashboard from './CounterDashboard'
import AsyncUser from './AsyncUser'

const loggerMiddleware = createLogger()

export default function configureStore(initialState) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  )
}

const store = configureStore()

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <div className="aui-message error">
          <p className="title">
            <span className="aui-icon icon-error"></span>
            <strong>Retrieve Atlassian ID Profile</strong>
          </p>
          <AsyncUser />
        </div>
      </Provider>
    );
  }
};

export default App
