---
title: "Confluence 5.9 EAP release now available"
date: "2015-10-06T15:00:00+07:00"
author: "mjensen"
categories: ["Confluence","EAP"]
---

The Confluence development team is proud to announce that the first Early Access Program (EAP)
 release of Confluence 5.9 is now  available. There are a large number of API changes
 and library changes, so now is the time to begin testing and planning new compatibility
 for your add-on.

## What's the deal with EAPs?
[EAP releases](https://developer.atlassian.com/docs/developer-tools/additional-developer-resources/early-access-programs?utm_source=DAC&utm_medium=blog&utm_campaign=confluence-59-eap-announcement)
are early previews of Confluence during development. We release these previews so
that Atlassian's add-on developers can see new features as they are developed and
test their add-ons for compatibility with new APIs.

The number of EAP releases before the final build varies depending on the nature of the
release. The first publicly available EAP release for Confluence 5.9 is `pluginsfour018`. Developers
should expect that we will only provide a few EAP releases before the final release.

***

## New in 5.9.1-pluginsfour018

To find out what's in this release, check out the [Confluence 5.9.1-pluginsfour018 Release Notes][1].

## Get Started

[Download the EAP][2] release from our site and start testing your add-on for compatibility
with changes to Confluence. Be sure to take a look at our [Preparing for Confluence 5.9][3] guide for details of changes that may affect your add-on. We'll update that page
regularly, so we recommend you check in on it regularly to ensure you're aware of
any changes.

[1]: https://confluence.atlassian.com/display/DOC/Confluence+5.9.1-pluginsfour018+Release+Notes
[2]: http://www.atlassian.com/software/confluence/download-eap
[3]: https://developer.atlassian.com/display/CONFDEV/Preparing+for+Confluence+5.9