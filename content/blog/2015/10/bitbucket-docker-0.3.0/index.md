---
title: "Release: Bitbucket Docker Hub 0.3.0; the private repo release "
date: "2015-10-29T06:00:00+07:00"
author: "ssmith"
categories: ["docker", "bitbucket", "atlassian-connect", "clojure"]
---

<style>
  .float-image {
      display: block; 
      margin: 15px auto 30px auto;
      box-shadow: 10px 10px 15px #888888;
  }
</style>


In case you missed it, last month we launched our
[Bitbucket Docker Hub integration][prev-blog] as part of the
[Docker Hub 2.0 launch][hub2]. We are now pleased to announce the next version
of this add-on is now available. If you already have it installed you'll get it
automatically. If you haven't already installed it see below for instructions on
adding it to your account. Carry on reading for more information on this
release.

[hub2]: http://blog.docker.com/2015/09/docker-hub-2-0/
[prev-blog]: https://developer.atlassian.com/blog/2015/09/docker-bitbucket/


## What's new in this release

<img class="float-image" src="screenshot.png" />

1. *Private Bitbucket Repositories*: We now support private Bitbucket
   repositories as well as public ones. Private Docker Hub repositories are not
   yet supported, but we are looking into adding it. If you are interested in
   this feature please let us know in the comments below.
1. *`has_file` support*: Uses the new Bitbucket Connect `has_file` functionality
   to be better behaved on repositories that don't have Docker Hub integration
   configured.
1. *Misc. internal fixes and improvements*: Including a Compose-based
   integration testing framework, a better release process, Redis caching of
   session, etc. See the [Bitbucket commit log][plugin-repo] for details.

## Installing the add-on

The Docker Hub shows the status of a corresponding Docker Hub image repository
from within your Bitbucket repository. This allows you to keep your Dockerfiles
in Bitbucket and see their build results and pull status directly in the
repository.

Installing it is a 2-step process.

1. Go to your account settings in Bitbucket, select "Find new add-ons", and
   click "Install" on "Docker Hub Integration".
1. As not all repositories necessarily have Dockerfiles the add-on is disabled
   by default. To enable it for a repository add the file
   `.docker-repository.yml` in the root.


By default the add-on will assume that your Bitbucket and Docker Hub
accounts/repositories have the same names; e.g. if your Bitbucket respository
is:

    https://bitbucket.org/ssmith/mydocker

the corresponding Hub repository is:

    https://hub.docker.com/ssmith/mydocker

You can override this default in `.docker-repository.yml` [YAML][YAML] file. If
you add the entry `repository`, the add-on will use this path for the Docker
account/repository. For example:

    repository: tarkasteve/multipy

will then use the Docker Hub repository path of:

    https://hub.docker.com/r/tarkasteve/multipy/

Learn more by checking out the [add-on source repository][plugin-repo].


[YAML]: https://en.wikipedia.org/wiki/YAML
[plugin-repo]: https://bitbucket.org/ssmith/bitbucket-docker-connect
