---
title: "Tip of the Week - Using a macro-enabled keyboard in Confluence."
date: "2015-10-16T16:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","Confluence"]
lede: "Did you know you can update 70 pages in less than a minute using a macro-enabled keyboard?
In this Tip of the Week we'll explain how you can do this."
---
In this weeks tip <a href="https://twitter.com/DeazexS">Stephen Siffers</a> explains
how he uses a macro-enabled keyboard to make changes to 70 Confluence pages in less
than a minute. Keep in mind, we are not talking about the
<a href="https://marketplace.atlassian.com/plugins/app/confluence/highest-rated?category=Macros" target="_blank">macros you love and use</a>
daily within your Confluence pages, but about macro-enabled hardware keyboards!

I'll let Stephen explain it:

> When we do a major page overhaul and we need to update
> 70+ pages with a new setup we have a little trick:
> We don't use blueprints due to the nature of how we use Confluence,
> so I implemented some macro functions I could use with a macro enabled keyboard
> and edited all 70 pages in under 1 min.
>
> The interns love this since its less busy work for them.

After requesting some more information about what kind of macros
he uses he gave us the following answer:

> The macro keys are rather simple once you understand them.
> A macro is a small script that'll run every time you hit the macro key.
> So I set up Chrome to open all the pages I wish to edit.
> From there I activate the macro key which does a CTRL+TAB to switch between tabs,  
> a little bit of key magic which select/pastes/tabs/more pastes.
> Then CTRL+S to save and then the macro loops.
> I have it set to be toggled, but you can set it to time out after a certain amount of runs.

Here is a small screengrab from Stephen:<br>
<br>
<center><iframe width="560" height="315" src="https://www.youtube.com/embed/XQaMNf0fU8s" frameborder="0" allowfullscreen></iframe></center>

Thank you Stephen for this tip!

Another use case might be using such a keyboard to quickly do routine maintenance tasks
in Bamboo, JIRA, or Bitbucket Server.

So how do you start using this kind of keyboard?

Here are some first steps:
* check out the different available macro keyboards on <a href="http://www.amazon.com/s/ref=nb_sb_noss_1?url=search-alias%3Daps&field-keywords=macro+keyboard&rh=i%3Aaps%2Ck%3Amacro+keyboard">Amazon</a>.
* install the right software for your keyboard
* start creating and using macros

***

For those of you who don't feel like buying another keyboard or who would like to do this
"Developer Style" there is of course our fully enabled REST API for Confluence.
This allows you to write something similar to this solution in your favourite language.

Take a look here for a tutorial : <a href="https://developer.atlassian.com/static/connect/docs/latest/guides/confluence-gardener-tutorial.html"> Confluence Gardener Tutorial</a>.

***

Cheers and till next week!
