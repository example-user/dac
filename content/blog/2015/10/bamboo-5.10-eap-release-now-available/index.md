---
title: "Bamboo 5.10.0 EAP release now available"
date: "2015-10-21T16:00:00+07:00"
author: "easenjo"
categories: ["Bamboo","EAP"]
---

The Bamboo development team is proud to announce that the first Early Access Program (EAP)
 release of Bamboo 5.10.0 is now available. There are a large number of API changes
 and library changes, so now is the time to begin testing and planning new compatibility
 for your add-on.

## What's the deal with EAPs?
[EAP releases](https://developer.atlassian.com/docs/developer-tools/additional-developer-resources/early-access-programs)
are early previews of Bamboo during development. We release these previews so
that Atlassian's add-on developers can see new features as they are developed and
test their add-ons for compatibility with new APIs.

The number of EAP releases before the final build varies depending on the nature of the
release. The first publicly available EAP release for Bamboo 5.10.0 is `5.10.0-beta01`. Developers
should expect that we will only provide a few EAP releases before the final release.

***

## New in 5.10.0-beta01

To find out what's in this release, check out the [Bamboo 5.10.0-beta01 Release Notes][1].

## Get Started

[Download the EAP][2] release from our site and start testing your add-on for compatibility
with changes to Confluence. Be sure to take a look at our [Preparing for Bamboo 5.10.0][3] guide for details of changes that may affect your add-on. We'll update that page
regularly, so we recommend you check in on it regularly to ensure you're aware of
any changes.

[1]: https://confluence.atlassian.com/display/BAMBOO/Bamboo+5.10+EAP+release+notes 
[2]: http://www.atlassian.com/software/bamboo/download-eap
[3]: https://developer.atlassian.com/display/BAMBOODEV/Bamboo+5.10+EAP+developer+notes
