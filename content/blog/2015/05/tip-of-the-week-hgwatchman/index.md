---
title: "Tip of the Week: Hgwatchman to wrangle those huge Mercurial repos"
date: "2015-05-18T16:00:00+07:00"
author: "jgarcia"
categories: ["Mercurial", "DVCS","totw"]
---

Last week, we had an amazing 10th anniversary meetup for
[Mercurial](http://mercurial.selenic.com)! [Sean
Farley](http://twitter.com/seanfarley) invited
[Facebook](https://www.facebook.com/facebook)'s [Ryan
McElroy](https://twitter.com/ryanmce) told us about an exciting open source
extension to Mercurial that the team at Facebook has been using for some time
now. As with most commercial software endeavors, the code base at Facebook grows
every day, and as a result specific operations can be time consuming. You can
watch the talk here, or read my summary below.

<div class="video-player">
<iframe width="560" height="315" src="https://www.youtube.com/embed/xsfTwG4Nc58"
frameborder="0" allowfullscreen></iframe>
</div>

## There must be a better way

In response to this business need, Ryan and his crew invented the
[hgwatchman](https://bitbucket.org/facebook/hgwatchman) extension and released
it on Bitbucket to the open-source community. If you have large latency with
your repo operations in Mercurial, give it a spin. Instructions to build the
extension are in the README; Ryan's real-world observations of performance
deltas are contained below.

<blockquote class="twitter-tweet tw-align-center" data-partner="tweetdeck">
<p lang="en" dir="ltr">Hgwatchman isn&#39;t just for status
<a href="https://twitter.com/hashtag/happybdayhg?src=hash">#happybdayhg</a>
<a href="https://twitter.com/atlassiandev">@atlassiandev</a>
<a href="http://t.co/0Q3ijyID86">pic.twitter.com/0Q3ijyID86</a>
</p>&mdash; John Garcia (@bitbucketeer)
<a href="https://twitter.com/bitbucketeer/status/596134423923298304">
May 7, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

## How the magic happens

Behind the scenes, hgwatchman is monitoring the filesystem for changes and
keeping an index of those changes between operations, instead of `stat` on every
file at runtime.

It's trivial to install, yet gives a performance boost to repos with large
filesets. Check out the video above if you'd like know more. Let us know what
you think in the comments!

Watch out for news and info from our
[@AtlassianDev](https://www.twitter.com/atlassiandev) Twitter feed!

Follow me at [@bitbucketeer](https://www.twitter.com/bitbucketeer)!

<div class="signup-container"><a class="aui-button aui-button-primary" href="http://atlascamp.com">Learn more at AtlasCamp</a></div>

<style>
  .video-player {
    display: block;
    margin: 25px auto;
  }

  .video-player {
    width: 560px;
  }
</style>
