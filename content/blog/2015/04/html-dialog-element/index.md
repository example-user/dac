---
title: "The HTML dialog element"
date: "2015-04-15T16:00:00+07:00"
author: "tsmith"
categories: ["HTML"]
---

<style>
.blog img
{
  display: block;
  margin: 0 auto;
  border: 1px #ccc solid;
}
</style>

Did you know that there's a `dialog` element in the proposed HTML 5.1 spec? Did you know that
you can use this in [Blink]-based browsers natively or with a
[dialog polyfill][dialog-polyfill] for other browsers? I didn't know this until just recently.
Here's what I've learned...

The simplest example is just a page that opens and closes a modal dialog.

![Native dialog example in action](default-dialog.gif)

To make that example happen, we need just a couple items in our page. A `dialog` element provides
the content, and we are embedding a `form` with the [method of "dialog"][method-dialog], which
closes the dialog on submit instead of navigating to another page. A button's click handler is
setup to call `[showModal]()` on the dialog element, allowing the dialog to appear when desired.

``` htmlmixed
<!DOCTYPE html>
<html>
<head>
  <title>Dialogs!</title>
</head>
<body>
  <dialog>
    <form method="dialog">
      This is a dialog!
      <input type="submit" value="Close" />
    </form>
  </dialog>
  <h1>Hi there!</h1>
  <p><button>Open dialog</button></p>

  <script>
    var button = document.querySelector('button');
    button.onclick = function() { document.querySelector('dialog').showModal(); };
  </script>
</body>
</html>
```

So that's pretty cool! But that's not all we can do.
We can apply some styling to the backdrop of the `dialog` via the [backdrop][backdrop-element]
pseudo selector. This controls the 'gray out' effect when using a modal dialog.

``` css
dialog::backdrop {
  background-color: red;
}
```

And that CSS in action produces a pretty red overlay to all the content.

![Dialog with red backdrop](custom-backdrop.png)

If you want to do this using the polyfill, you'll need to adjust your selector slightly as the
pseudo element doesn't exist and instead the polyfill creates a new element to take the place of the
`::backdrop`.

```
dialog + .backdrop,
dialog::backdrop {
  background-color: red;
}
```

This is enough to get you started, but there's a couple other things if you're interested...

## `dialog.close()` and `dialog.returnValue`

You can call `dialog.close()` instead of using a form submit to dismiss the dialog. When you call
close you may provide a return value that's exposed as the `returnValue` property on the dialog
object. This might be handy to ask for and return input in a modal, like `[window.prompt]`.

## 'show' vs. 'showModal'

The behaviour of 'show' and 'showModal' differs in display. And 'show' does not produce a
`::backdrop` pseudo element, covering over other content.

### `dialog.show()`

![Dialog.show](dialog-open.png)

### `dialog.showModal()`

![Dialog.show](dialog-openModal.png)

## `dialog.cancel` event

If you open a modal dialog, the escape button will close the dialog. You can listen for this event
and then use `event.preventDefault()` to disable this behaviour.
`dialog.oncancel = function(evt) { evt.preventDefault(); }` will do that for you.

## `dialog.open` attribute

You can make the dialog open on page load by adding the `open` attribute to the `dialog` element.
This opens the dialog as if you used the `show` method on the element. Calling 'show' or 'showModal'
on the dialog also changes this property.

We can see what's going on by registering a `[MutationObserver]`, which listens for changes to the
DOM.

``` javascript
var observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
    console.log(mutation);
  });
});
var dialog = document.querySelector('dialog');
observer.observe(dialog, { attributes: true });
```

The MutationObserver shows that the open value changed, but Chrome does not appear to fill in
`oldValue` in this case. You still get notifications that it's changing &mdash; the important part.
Hitting open and close a couple times shows the following in Chrome.

![MutationObserver results in Chrome](mutation-observer-of-open.png)

## Using dialogs!

There are websites using [dialog elements now][dialog-examples], and you can too!
Now you can replace all those app modal `alert` dialogs with page or tab modal `dialog`s!

Thanks to [@jasonkarns] for bringing this to my attention. Without that, I wouldn't have spent half
a day poking around to bring you all the insight I learned.
Follow me [@TravisTheTechie] on Twitter for more things about the web, LEGOs, and tech.


[caniuse-dialog]: http://caniuse.com/#search=dialog
[dialog-polyfill]: https://github.com/GoogleChrome/dialog-polyfill
[backdrop-element]: https://developer.mozilla.org/en-US/docs/Web/CSS/::backdrop
[@jasonkarns]: https://twitter.com/jasonkarns
[Blink]: http://www.chromium.org/blink
[method-dialog]: https://html.spec.whatwg.org/multipage/forms.html#attr-fs-method-dialog-keyword
[window.prompt]: https://developer.mozilla.org/en-US/docs/Web/API/Window/prompt
[MutationObserver]: https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
[dialog-examples]: https://wiki.whatwg.org/wiki/Dialogs#Real_world_examples_today
[@TravisTheTechie]: https://twitter.com/TravisTheTechie
[showModal]: https://html.spec.whatwg.org/#dom-dialog-showmodal
