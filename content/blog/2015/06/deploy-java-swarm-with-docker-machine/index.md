---
title: "Deploy a Java Swarm with Docker Machine"
date: "2015-06-18T06:00:00+07:00"
author: "npaolucci"
categories: ["docker", "stash", "devops", "java"]
---

In this video you'll see me use the pre-release version of [Docker Machine]
(`0.3.0`) to deploy a 3 node [swarm] on Digital Ocean and use [label
constraints] to deploy our Java based enterprise Git server [Stash] and
[PostgreSQL].

<iframe width="850" height="500" src="https://www.youtube.com/embed/ULBxJq9UcM0" frameborder="0" style="margin: 15px 0 10px 0" allowfullscreen></iframe>

Follow me [@durdn] and [@atlassiandev] or come say hi next week at [DockerCon],
I'll be there with a fun group of Atlassians to nerd out on Docker and the
revolution at hand!

[Docker Machine]: https://github.com/docker/machine
[swarm]: https://github.com/docker/swarm
[label constraints]: https://docs.docker.com/swarm/scheduler/filter/
[PostgreSQL]: http://www.postgresql.org/
[@durdn]: http://twitter.com/durdn
[@atlassiandev]: http://twitter.com/atlassiandev
[DockerCon]: http://www.dockercon.com/
[Stash]: https://www.atlassian.com/software/stash
