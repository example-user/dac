---
title: "We are discontinuing the support for Atlassian IDE Connectors"
date: "2015-06-01T08:00:00+07:00"
author: "bgatz"
categories: ["IDE Connectors"]
---

<style type="text/css">
.lede {
  display: none;
  visibility: hidden;
  font-style: italic;
}
.blog-archive .lede {
  display: block;
  visibility: visible;
  font-style: normal;
}
</style>

<p class="lede">Four years in software industry is like two lifespans in human years. The evolution
of Atlassian products is so fast that looking back at the product releases from four years ago is
like watching the black and white photos of our grand parents when they were young.</p>

Four years ago Jens Schumacher, the Group Product Manager of Development Tools at Atlassian, [wrote
a blog post](http://blogs.atlassian.com/2011/02/atlassian_ide_connector_30/) where he explained our
motivation behind starting the open source [Atlassian IDE
Connector](http://www.atlassian.com/software/ideconnector) project. We had a goal of delivering a
faster and more convenient way to interact with Atlassian applications.

Since we started that effort, a lot has changed. The web and our products have evolved, and as a
consequence, IDE Connectors are duplicating functionality that is already available today in
Atlassian products.

Over the last several years, we have performed a full redesign of all Atlassian products' UI to
match the [Atlassian Design Guidelines](https://design.atlassian.com/latest/). We made the flows
more consistent, and we have tied many of our products much closer together. At the same time we
have invested a huge effort into product APIs, and have created [a whole new add-on
platform](https://developer.atlassian.com/static/connect/docs/latest/index.html) to support all
those cases where we could not provide functionality within the products directly, or users
desired integration with external systems.

There is still a lot of work ahead of us to make our products and respective platforms
stronger. For that reason we have decided to **end the support of IDE connectors**. The
product will remain available as **open source**, and we will continue to host it on Bitbucket.

## What exactly is changing?

Atlassian will not release any new versions of IDE connectors, the current release is the last. We
have also discontinued the customer support and Atlassian Answers effort related to IDE Connectors.

Specifically:

* **IDE Connector for Eclipse**, the last version released from Atlassian on **27 April 2015** is
  [3.2.5](https://confluence.atlassian.com/display/IDEPLUGIN/Atlassian+Connector+for+Eclipse+-+v3.2.5+Release+Notes).
  Its [support project on SAC](https://support.atlassian.com/browse/ECSP) and the [development
  backlog project on EAN](https://ecosystem.atlassian.net/projects/PLE) are now closed.
* **IDE
  Connector for IntelliJ**, the last version released from Atlassian on **20 April 2015** is
  [3.0.16](https://confluence.atlassian.com/display/IDEPLUGIN/Atlassian+Connector+for+IntelliJ+IDEA+-+v3.0.16+Release+Notes).
  Its [support project on SAC](https://support.atlassian.com/browse/IDESP) and the [development
  backlog project on EAN](https://ecosystem.atlassian.net/projects/PL) are now closed.
* **IDE Connector for Visual Studio**, the last version released from Atlassian on **10 April 2015**
  is[1.3.12](https://confluence.atlassian.com/display/IDEPLUGIN/Atlassian+Connector+for+Visual+Studio+-+1.3.12+Release+Notes).
  Its [support project on SAC](https://support.atlassian.com/browse/IDESP) and the [development
  backlog project on EAN](https://ecosystem.atlassian.net/projects/PLVS) are now closed.
* We have updated product listings on [www.atlassian.com](http://www.atlassian.com) and
  [marketplace.atlassian.com](http://marketplace.atlassian.com).

These changes are effective immediately.

## What about the older versions of the Atlassian IDE Connectors?

You will still be able to download these versions of the Connectors, but Atlassian will not maintain
support for them going forward. The good news is that Atlassian IDE Connectors are **open source**,
and we have just made it a whole lot easier for you to contribute by hosting the Atlassian IDE
Connectors on BitBucket. Please feel free to fork the
[VisualStudio](https://bitbucket.org/atlassian/connector-vs),
[Eclipse](https://bitbucket.org/atlassian/connector-eclipse) or
[IDEA](https://bitbucket.org/atlassian/connector-idea) repository and contribute to the project.

We are also making the documentation for the connectors open source. You can access them here:
[http://atlassian-docs.bitbucket.org/](http://atlassian-docs.bitbucket.org/)  

## What if I have questions?

Please feel free to get in touch with us. You can email me directly under: bgatz at atlassian dot
com.
