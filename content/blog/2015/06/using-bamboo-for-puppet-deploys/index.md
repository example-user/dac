---
title: "Tip of the Week: Using Bamboo for Puppet Deploys"
date: "2015-06-22T16:00:00+07:00"
author: "jgarcia"
categories: ["totw","Bamboo"]
---

This week's Tip of the Week is a clever idea for automating Puppet deploys using
Stash and Bamboo. Many thanks to [Peter Van de
Voorde](https://twitter.com/pvdevoor) of [RealDolmen](http://www.realdolmen.com)
for this idea.

Peter writes:

>We store all our puppet scripts and config in Stash. To make sure that
everything we push to Stash is correct we execute a homemade pre-receive hook
that will do a puppet-lint and puppet-validate on all puppet scripts that are in
every push to Stash.

>Now that we are sure of the 'correctness' of our puppet scripts we can use them
from our Bamboo Build Plans. We can now use them as part of the set-up phase of
a plan to set-up the configuration of any server or environment we want by
simply checking them out of Stash and executing them using an Agent.

>We would also like to create a Build Plan in Bamboo that actually executes a
number of puppet scripts on a list of servers (for example to update the Java
Version on all these servers).
