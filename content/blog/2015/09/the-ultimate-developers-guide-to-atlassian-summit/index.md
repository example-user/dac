---
title: "The ultimate developers guide to Atlassian Summit"
date: "2015-09-29T20:00:00+07:00"
author: "lkilpatrick"
categories: ["summit", "atlassian-connect"]
lede: "Atlassian Summit is coming up fast, November 3-6 in San Francisco, CA. This year 
			 we've built a series of special programs just for developers.  This program includes a 
			 full training day dedicated to Atlassian Connect, which covers the essentials needed to
			 build add-ons for our cloud products."
---


If you’re a developer attending [Atlassian Summit](https://summit.atlassian.com/) 
and you'd like to integrate and customize JIRA, Confluence, HipChat, or Bitbucket 
with add-ons - this guide is for you. And if you're not attending, you should check it out!

## Monday, November 2nd : Ecosystem day

Ecosystem day is a full day of workshops, product updates and marketing training 
to take your add-on business to the next level. Marketplace vendors and Experts 
are welcome to <a href="http://www.eventbrite.com/e/atlassian-ecosystem-partners-day-2015-tickets-18370350212">join ecosystem day</a>.


## Tuesday November 3rd : Training day

This year we have an entire day of [Atlassian Connect Training](https://summit15marketplacetraining.eventbrite.com). 
In the morning we'll focus on JIRA and Confluence. In the afternoon we'll cover 
HipChat and Bitbucket. Its only $99 for each morning or afternoon session.

<p align="center" style="margin: 35px 0 35px 0;">
<a href="https://summit15marketplacetraining.eventbrite.com" class="aui-button aui-button-primary" style="font-size: 16px; line-height: 24px;">Register for training</a> 
</p>

Meet our Connect experts and learn how to use Atlassian Connect to bring your add-on, 
new idea, or integration into the cloud with our products.

<div class="aui-group" style="padding-top: 25px;">
	<div class="aui-item" style="font-size: 18px; text-align: center;">
		<span class="aui-avatar aui-avatar-project aui-avatar-xlarge">
		    <span class="aui-avatar-inner">
		        <img src="ralph_whitbeck.png">
		    </span>
		</span>
		<p>Ralph Whitbeck<br/>
		<em>Bitbucket</em></p>
	</div>
	<div class="aui-item" style="font-size: 18px; text-align: center;">
		<span class="aui-avatar aui-avatar-project aui-avatar-xlarge">
		    <span class="aui-avatar-inner">
		        <img src="patrick.jpg">
		    </span>
		</span>
		<p>Patrick Streule<br/>
		<em>HipChat</em></p>
	</div>
	<div class="aui-item" style="font-size: 18px; text-align: center;">
		<span class="aui-avatar aui-avatar-project aui-avatar-xlarge">
		    <span class="aui-avatar-inner">
		        <img src="dallas_tester.png">
		    </span>
		</span>
		<p>Dallas Tester<br/>
		<em>JIRA</em></p>
	</div>
	<div class="aui-item" style="font-size: 18px; text-align: center;">
		<span class="aui-avatar aui-avatar-project aui-avatar-xlarge">
		    <span class="aui-avatar-inner">
		        <img src="matt.jpg">
		    </span>
		</span>
		<p>Matthew Jensen<br/>
		<em>Confluence</em></p>
	</div>
</div>

## November 4th and 5th : Enhance Track

On Wednesday morning, you won't want to miss the keynote from the founders of Atlassian. 
You'll hear all the details of what we've been working on for the last year. Then come 
along to the [Enhance track](https://summit.atlassian.com/sessions?track=Enhance).

On Thursday Connie Kwan will kick off the day showing off new features and what's 
coming for Marketplace. Then hear how **Twilio**, **Oscar Health Insurance** and 
**Camerican International** use add-ons with their Atlassian products, so they work 
faster and smarter. You won't want to miss a session of the Enhance track.

<style>
table.aui th, table.aui td, table.aui a {
	font-size: 18px;
}
</style>
<table class="aui" style="margin-top: 25px;">
	<thead>
		<tr>
			<th nowrap>
				<p>Nov 4th<br/>
				Technical updates about Atlassian Connect</p>
			</th>
			<th nowrap>
				<p>Nov 5th<br/>
				Hear from add-on users</p>
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><a href="https://summit.atlassian.com/sessions?track=Enhance&session=28566">Connecting HipChat to (allthethings)</a></td>
			<td>
				<a href="https://summit.atlassian.com/sessions?session=31069">Just-In-Time Business Operations (Yeah, there's an add-on for that!)</a>
				<ul class="menu">
					<li>Ethan Foulkes, Business Development Manager, Camerican International</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td><a href="https://summit.atlassian.com/sessions?session=28631">Building Kick-ass Tools for Millions of Developers with Bitbucket</a></td>
			<td>
				<a href="https://summit.atlassian.com/sessions?track=Enhance&session=31068">ChatOps: Automating your Company with HipChat, Bitbucket and JIRA Service Desk</a>
				<ul class="menu">
					<li>Sara Wajnberg, Director of Operations Infrastructure, Oscar Insurance</li>
					<li>Mackenzie Kosut, Head of Technical Operations, Oscar Insurance</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td><a href="https://summit.atlassian.com/sessions?track=Enhance&product=Confluence&session=31828">Tailoring Confluence for Team Productivity</a></td>
			<td>
				<a href="https://summit.atlassian.com/sessions?track=Enhance&session=31070">JIRA: Beyond Engineering &amp; Computers; From 2FA to Approvals: Utilizing Atlassian From Your Thumbs</a>
				<ul class="menu">
					<li>Dom DeGuzman, Infrastructure Engineer, Twilio</li>
				</ul>
			</td>
		</tr>
	</tbody>
	</table>

<br>
The Summit Bash at San Francisco's world-famous Exploratorium on Wednesday night 
is one not to miss. A chance to relax and socialize with Atlassians and your peers 
as you chat and explore in one of the coolest venues in the city.

We can't wait to see you at Summit 2015. [Get started with Connect training](https://summit15marketplacetraining.eventbrite.com).