---
title: "Easy data storage with Atlassian Connect Express"
date: "2015-03-11T16:00:00+07:00"
author: "tsmith"
categories: ["Atlassian Connect", "ACE", "Add-ons"]
---

You've been working on a Connect add-on using [Atlassian Connect Express (ACE)][ace]. You're to the point where you
need to store some data in your add-on. You can use the [add-on properties REST API][addon-properties] in some cases.
You can add an entire storage mechanism to handle this but ACE has a settings storage mechanism already. Get this 
&mdash; you can just piggyback on that! It's not hard to do, and it's perfect for things like global configuration
for your add-on. 

## Add-on properties REST API

If you don't want to store any data in your add-on, you can use the add-on properties REST API. This has some 
limitations though: 

* Your add-on is limited to 100 properties
* No property can be larger than 32KB
* Property values must be valid JSON
* Client side requests must be made with a logged-in user
* Using add-on properties with server side code in your add-on means another round trip of latency
 
If those limitations are acceptable to you, you shouldn't even bother with worrying about storing data in ACE. However,
there are plenty of times these limitations don't work for you. 

## Settings storage in ACE

Assuming you're using ACE 1.x, you'll find a couple of methods on `addon.settings` you can use to retrieve and store
data. If you need to store large amounts of data, this might not be the best way to handle it. But for many 
scenarios with limited storage requirements this should work brilliantly. 

Here is a simple example of loading some data and passing it off to a template for rendering.

``` javascript
app.get('/configure', addon.authenticate(), function (req, res) {
  addon.settings.get('config', req.context.clientKey).then(function (data) {
    res.render('configure', data);
  });
});
```

Storing the data with `addon.settings.set(key, value, clientKey)` is similar; it also returns a promise. In this 
example there is an example that would be called with an AJAX post from the `/configure` page above. Since this request 
isn't from the product host, you should use the [ACE token to authenticate with `addon.checkValidToken()`][ace-token]. 
This helps ensure security for this request. 

``` javascript
app.post('/configure', addon.checkValidToken(), function (req, res) {
  var config = req.body;
  addon.settings.set('config', config, req.context.clientKey).then(function() {
    res.json(config);
  });
});
```

## The choice is yours

Now that you have the knowledge to use `addon.settings` in ACE, you have an array of options to choose from. 
[Entity properties][entity-props] in JIRA and [content properties][content-props] in Confluence allow you to store data
inside the product instance tied to content. Add-on properties allow you to store data tied to your add-on. Storage
of configuration data should be no problem for your add-on!

[entity-props]: https://developer.atlassian.com/display/JIRADEV/JIRA+Entity+Properties+Overview
[content-props]: https://developer.atlassian.com/display/CONFDEV/Content+Properties+in+the+REST+API
[ace]: https://bitbucket.org/atlassian/atlassian-connect-express
[promise]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
[ace-token]: https://bitbucket.org/atlassian/atlassian-connect-express#markdown-header-in-ace-10
[addon-properties]: https://developer.atlassian.com/static/connect/docs/concepts/hosted-data-storage.html
