---
title: "Fresh Stashberry Pi for Pi Day"
date: "2015-03-14"
author: "jgarcia"
categories: ["arm", "Tomcat", "Stash"]
series: ["JIRA Startup Guides", "Old Stash Stuff"]
---

In the US offices of Atlassian, we're celebrating Pi Day tomorrow, 3/14/15, by
installing [Atlassian
Stash](https://www.atlassian.com/software/stash) onto a [Raspberry Pi
2](http://www.raspberrypi.org/raspberry-pi-2-on-sale/). Like many engineers with a
background in embedded systems,
I am gobsmacked to hear that the new Raspberry Pi 2 provides a full gig of RAM
 and a proper quad-core processor for USD 35, and I could
hardly wait to try it out with a commercial Java app! With a slim five watt
power requirement and solid state construction, it's an incredible
value. If you are ever called to send DVCS on a rocketship to the moon, you
will be hard pressed to find a more suitable candidate.

![Stashberry Pi.](stashberry pie.png)

It turns out this is actually a straightforward procedure, as the Pi's
Raspbian operating system comes with genuine [Oracle
Java](http://www.raspberrypi.org/oracle-java-on-raspberry-pi/) [JDK
8](http://www.raspberrypi.org/new-raspbian-and-noobs-releases/). This common
Java core is used throughout all of our BTF products, so within reasonable
parameters you would expect to be able to do something similar to this with
everything we publish from JIRA to Bamboo. There are really just two major
caveats; the binary installers won't work because they install IA64 or x86
compatible Java instead of the required armhf, and as the system has just 1GiB
of ram, we need to be careful about the way we allocate our memory. As the
install is pretty simple, I'll refer to the docs where appropriate and merely
call out specific changes.

Don't worry about a keyboard, video, or mouse. For this procedure, you can just
plug it into your router with Ethernet and power it up.
I want to be sure to point out that Atlassian does not
officially support this install scenario, nor recommend it for use in a
production scenario.

1. Download and install [Raspbian](http://www.raspberrypi.org/downloads/)
2. Use [Adafruit's](https://twitter.com/adafruit) excellent
[Occidentalis](https://github.com/adafruit/Adafruit-Pi-Finder#adafruit-
raspberry-pi-finder) to orient your new Pi
3. SSH to your Pi
4. ```sudo apt-get update``` and ```sudo apt-get upgrade```
5. ```sudo raspi-config``` and expand your partition to fill the SD
6. Reboot when prompted
7. Find the .tar.gz "Standalone" version of [Stash for Linux](https://www.
atlassian.com/software/stash/download#allDownloads) and ```wget``` it
8. ```tar xvf atlassian-stash-x.x.x.tar.gz``` to untar, then
```mv``` to a sensible location such as ```/opt```
9. Create a home folder for
Stash and define a ```STASH_HOME``` environment variable
10. Edit ```<stash_install>/bin/setenv.sh``` to present a reasonable amount
of ram:
	```JVM_MAXIMUM_MEMORY="512m"```
11. Ensure the logged-in user controls the ```/opt/<stash>``` and
```/var/<stash>``` folders
12. ```<stash_install>/bin/startup.sh``` to make Stash go.

Voila!!
=======

Here's some performance test results to show that it's not a total waste of
time. If you have a spare Pi 2 laying around and need
a household DVCS server, this is a great way to go about it. For those keeping
score at home, we used 50 users ramped over ten seconds with about 40 reqs each.

![Indicators](indicators.png) ![Response times](responsetimes.png)

Follow me on Twitter for updates about Atlassian software, robotics, Maker art,
and software philosophy at [@bitbucketeer](https://twitter.com/bitbucketeer).
