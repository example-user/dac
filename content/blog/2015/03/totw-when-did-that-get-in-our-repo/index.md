---
title: "Tip of the week: when did that get in our repo?"  
date: "2015-03-23T16:00:00+07:00"
author: "jgarcia"
categories: ["git", "dvcs", "totw"]
---

As our repositories grow large and complicated, it can seem impossible to find
when specific strings of text were introduced to the repository. While the `git
blame` command does great for showing you the most **recent** modification of
the line, how do we find the **earliest**, especially considering that line
numbers shift as commits and merges happen?

From our excellent Technical Account Manager Tim Wong comes this week's tip, the
solution to that question. In his words:

>"A common thing I want to know is, when did version x.y.z of some library enter
>the codebase?  The only way I have found to do this is:"

    #Get the commit that introduces the change:
	git log -S'Regex that matches the string i care about, like <crowd.version>2.6' --pickaxe-regex pom.xml
	 
    #find all the places that uses that commit
    git branch --contains COMMIT_HASH
 
	#eyeball for earliest commit

Note: --pickaxe-regex is an option that allows us to use POSIX regular expression 
matching, instead of the default string matching we would expect from -S. More 
info can be found at [the associated commit](http://git.kaarsemaker.
net/git/commit/d01d8c6782850c18d62676dae3c72ad73be1e52e/) in the Git project.

Let us know in the comments if you've found an easier way!

Follow me on Twitter for updates about Atlassian software, robotics, Maker art,
and software philosophy at [@bitbucketeer](https://twitter.com/bitbucketeer).