---
title: "Stash Plugin Tutorial: Beer O'Clock Repository Hook"
date: "2015-01-08"
author: "tpettersen"
categories: ["git", "stash"]
---

<style>
  .video-player, .screen-caps, .big-image {
    display: block;
    margin: 25px auto;
  }

  .video-player {
    width: 560px;
    text-align: center;
  }

  .screen-caps img {
    width: 32%;
  }
</style>

One of my favorite Stash features is its plugin system. Since Stash is a git 
repository host, 99% of Stash users are software developers. And since 
developers often like to [scratch their own itch][scratch], we've invested 
heavily in extensibility and customization to make plugins both powerful and 
simple to write. In this 30 minute [video tutorial][yt], I'll walk you through 
building, deploying and debugging a simple [repository hook][hooks] plugin, from 
scratch.

A plugin is an executable bundle of code that provides additional functionality 
to your Stash server. It does this by consuming services from the Stash API and
implementing plugin points provided by the Stash [SPI][spi]. There are many plugin 
points to choose from, allowing you to build your own features with custom UIs, 
REST APIs, pull request merge checks and repository hooks, among other things. 
The logic of this plugin is written in Java, but the actual amount of code is 
tiny, so you should be able to follow along even if you have little or no Java 
experience.

Make sure you view the video in HD mode (at least 1080p) so you can read the 
text. 

<div class="video-player">
  <iframe width="560" height="315" src="//www.youtube.com/embed/oU7i7hhiUQ8" 
          frameborder="0" allowfullscreen></iframe>  
  <a href="./transcript.html">View transcript</a>
</div>

If you're already done some Stash plugin development, you might want to skip 
ahead to a specific part of the video to learn more about a particular topic. 
This tutorial covers:

- auto-generating a new Stash plugin ([2:32][2:32]) and different types of 
plugin modules ([8:20][8:20]) using the Atlassian SDK
- flushing messages to the developer's console when they push changes to the 
repository ([12:22][12:22])
- spinning up a personal Stash server in developer mode using ``atlas-debug`` 
([14:52][14:52])
- what an Atlassian "timebomb" license is ([20:49][20:49])
- attaching a debugger to Stash to step through plugin code with breakpoints 
([24:19][24:19])
- hot deploying plugins to Stash using ``atlas-cli`` ([26:21][26:21]) for rapid 
iterative development
- me implementing, then fixing, a nasty bug. If you're game, watch from the 
beginning and see if you can catch it before I do :)

<div class="screen-caps">
  <a href="three.png"><img alt="Hook code" src="three.png"></a>
  <a href="one.png"><img alt="Hooks screen" src="one.png"></a>
  <a href="two.png"><img alt="Pushing" src="two.png"></a>  
</div>

If you enjoyed the video, have questions or want to see a tutorial on a specific 
aspect of plugin development, drop me a line on Twitter (I'm 
[@kannonboy][kannonboy]).

[scratch]: https://gettingreal.37signals.com/ch02_Whats_Your_Problem.php
[spi]: http://en.wikipedia.org/wiki/Service_provider_interface
[yt]: https://www.youtube.com/v/oU7i7hhiUQ8?version=3&vq=hd1080
[2:32]: https://www.youtube.com/watch?v=oU7i7hhiUQ8&t=2m32s
[8:20]: https://www.youtube.com/watch?v=oU7i7hhiUQ8&t=8m20s
[12:22]: https://www.youtube.com/watch?v=oU7i7hhiUQ8&t=12m22s
[14:52]: https://www.youtube.com/watch?v=oU7i7hhiUQ8&t=14m52s
[20:49]: https://www.youtube.com/watch?v=oU7i7hhiUQ8&t=20m49s
[24:19]: https://www.youtube.com/watch?v=oU7i7hhiUQ8&t=24m19s
[26:21]: https://www.youtube.com/watch?v=oU7i7hhiUQ8&t=26m21s
[hooks]: https://confluence.atlassian.com/display/STASH/Using+repository+hooks
[kannonboy]: https://twitter.com/kannonboy
