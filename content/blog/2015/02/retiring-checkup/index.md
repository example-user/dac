---
title: "Where we retire checkup.atlassian.com"
date: "2015-02-12T20:00:00+07:00"
author: "nwade"
categories: ["APIs", "Add-ons", "Developer experience"]
---

### What you need to know

For some time we've provided an API compatiblity-checking service to plugin developers –
[checkup.atlassian.com][checkup]. We built this service some years ago ostensibly so you could more easily check if
any product Java API you are using is or isn't a public supported API. We're retiring this service shortly.

While there have been a handful of developers and vendors using the service in recent months, it's usage levels don't
make sense for us to continue support. As of March 31st 2015, Checkup will be shut down and all requests will be
redirected to this post, and then later to our [API documentation][docs].

<hr style="background-color:#ddd;">

### Some history

Checkup was created to allow developers to validate their add-ons were API compatible between versions of
products. Checkup provided some useful API checks, but every plugin developer still needed to test on multiple versions
since API compatibility isn't the only thing that matters.

We feel that Checkup provides some value but not enough based on it's current
usage (about 100 tests per month) compared to its support needs.

Finally, we're a bit ashamed to admit that only a couple of Atlassian products have been providing new versions to
Checkup in the last year. And that's reducing to one product soon anyway (if we do nothing). This also limits its
effectiveness in a subtle, forgotten in-the-corner way.

This is not the kind of experience we're accustomed to, or aspire to delivering. 

### What to do now

For now, if you want to continue to check on the support for given Java APIs, check out the [docs] repository. It
includes links to the Javadocs for all our products and libraries. 

We will explore other ways to provide automated API checking to add-on and plugin developers in the future, although
there's nothing much we can say about that right now.

While Checkup is still running, you can provide
[feedback about Checkup][feedback] if there's something you'd like to add. 

So long and thanks for all the APIs, Checkup! 

[checkup]: http://checkup.atlassian.com/?utm_source=dac&utm_medium=blog&utm_campaign=retiring-checkup
[docs]: https://docs.atlassian.com/?utm_source=dac&utm_medium=blog&utm_campaign=retiring-checkup
[feedback]: https://ecosystem.atlassian.net/projects/PAPIA
