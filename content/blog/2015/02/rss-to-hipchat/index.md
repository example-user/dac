---
title: "RSS to HipChat integration in under an hour"
date: "2015-02-19T16:00:00+07:00"
author: "tsmith"
categories: ["HipChat", "webhooks"]
---

I got a request from an internal team to get notified when a new post on this blog goes live. It seemed like something
one should be able to do without too much effort. So I set aside about an hour to figure something out and now I want to
share what I came up with to solve this problem. This same method could be used to integrate almost any services easily,
quickly, and without investments in infrastructure.

I don't want to manage an application or infrastructure, so I'm going to use a couple of third party services here.
The data flow is first from an RSS feed into [Zapier][zapier], then use a web hook to push that data into
[webscript.io][webscript] which transforms the data into a message payload that is then pushed into HipChat. Zapier has
a HipChat integration &ndash; but it requires an admin token for the HipChat account. I'm interested in reducing the
scope of access this integration has down to one or a few rooms only. I also want to have the flexibility to transform
the data that using webscript.io can offer

First off I know I need to get data into HipChat, thankfully we have [build your own integrations with HipChat][byo-hc].

> Log into [hipchat.com][hc], and go to **Rooms > Integrations > Find New**. From there, click on “Build Your Own”.

I'll start with naming my integration, in this case _RSS Bot_ works fine. The next page gives me the details I need,
most notably a handily named URL I'll need later, _Send messages to this room by posting to this URL_.

The external piece I need is a place to transform data and push it into HipChat. A [webscript.io][webscript] endpoint
can be made to do just about anything using Lua so I'm going to do that. 

1. Sign up for a free account
1. Click _Create a new script..._ 
1. Choose a subdomain and path for the endpoint. I'll select `rss-to-hipchat.webscript.io/new-rss`.
   ![Configuring new Webscript endpoing](webscriptio-newscript.png)


Now that's for the transformation I want to do. To get each new feed item over to my webscript.io endpoint, Zapier comes
into play. Zapier trigger actions based upon RSS updates.

1. Sign up for a free account with Zapier
1. Click _Make a New Zap_ button to start an integration
1. For the trigger, select "RSS" then "New Item in Feed"
1. For the action, select "Web Hook" then "POST" &mdash; at this point one could just integrate directly with HipChat
   but that requires a admin token rather than a room token and we want to scope the access of this integration to just
   a single room
1. Click _Continue_ to move forward to the configuration screens
1. Put the RSS feed into the URL field
   ![Configuring Zapier's RSS trigger](configure-rss.png)
1. For the web hook section, we start by putting in the URL for the webscript endpoint;
   e.g. `rss-to-hipchat.webscript.io/new-rss`
1. Select json as the payload type
1. Configure the payload by adding 3 fields
   * `url` to Link
   * `title` to Title
   * `date` to Pub Date

   ![Configuring Zapier's Web Hook action](configure-webhook.png)

Configuring Zapier is now done, but before testing the webscript endpoint needs to be written. Going back to
[webscript.io][webscript] and filling out the endpoint, by putting in the Lua code, converts posted payloads to messages
that appear in a given HipChat room.

``` lua
-- parse the POST body from Zapier
local requestBody = json.parse(request.body)

local url = requestBody.url
local title = requestBody.title

-- generate the data to push to HipChat
local data = json.stringify({
		color = "green",
		notify = false,
		message_format = "html",
		message = "New blog: <a href=\'" .. url .. "\'>" .. title .. "</a>"
	})

-- make the request to HipChat
-- replace the URL with one from the first steps so it includes an auth token and the right room id (not 55555)
-- or see https://www.hipchat.com/docs/apiv2/auth if help is needed with tokens
-- or see https://www.hipchat.com/docs/apiv2/method/send_room_notification for information about this API
http.request {
	url = "https://api.hipchat.com/v2/room/55555/notification?auth_token=...",
	method = "POST",
	data = data,
	headers = {
		["Content-Type"] = "application/json"
	}
}

-- we return an http status of 200 (OK) and the data we're posting to HipChat
--   just so we can examine the data during testing.
return 200, data
```

So it will look something like this:

<img src="webscript-endpoint.png" alt="Webscript.io endpoint"
  style="border:1px solid #ccc; display: block; margin: 20px auto 20px auto;" />

With the endpoint built, [httpie] makes it easy to test. Executing this command posts a JSON payload to the endpoint on
webscript and it should post a message to the configured HipChat room. If you get a 500 error, webscript provides logs
below the script entry you can expand to help debug what's going wrong. Make sure the URL of the `http.request` is
updated, including the correct room id and auth token.

``` shell
$ http post rss-to-hipchat.webscript.io/new-rss url=https://developer.atlassian.com/blog/2015/01/work-with-git-and-perforce/ "date=Fri, 13 Feb 2015 16:00:00 GMT" "title=Working with Git and Perforce: integration workflow"
  HTTP/1.1 200 OK
  Access-Control-Allow-Origin: *
  Cache-Control: no-store
  Connection: keep-alive
  Content-Length: 224
  Content-Type: text/plain
  Date: Fri, 13 Feb 2015 18:38:52 GMT
  Server: nginx/1.2.1
  X-Frame-Options: SAMEORIGIN

  {"color": "green", "message": "New blog: <a href='https://developer.atlassian.com/blog/2015/01/work-with-git-and-perforce/'>Working with Git and Perforce: integration workflow</a>", "notify": false, "message_format": "html"}
```

Assuming that works, and the message is visible in the HipChat room, the final step is to use the tester in Zapier to
make sure everything is hooked up right front-to-back.

![Zapier's test options](test-zap.png)

With these tools, I can quite easily take any data source and transform it however I want and send it into HipChat.
I can also notify multiple rooms in one go just by adding additional `http.request` calls to the webscript endpoint.
All of this without building or deploying any infrastructure &ndash; just using existing services hooked together.

[byo-hc]: https://blog.hipchat.com/2015/02/11/build-your-own-integration-with-hipchat/
[hc]: https://www.hipchat.com/
[zapier]: https://zapier.com
[webscript]: https://www.webscript.io/
[httpie]: https://github.com/jakubroztocil/httpie
