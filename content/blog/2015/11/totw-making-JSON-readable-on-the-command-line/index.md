---
title: "Tip of the Week - Making JSON readable on the command-line"
date: "2015-11-24T16:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","bash","Powershell"]
lede: "As a developer you run into a lot of information stored in JSON format.
In this tip of the week we are going to show how you can easily make the JSON data
humanly readable on the command-line."
---
This week's article is focused on those times when you're developing against an API
that's returning JSON data and you need to be able to easily read the JSON data.
Especially if you want to do some quick tests from the command-line this tip can be of
interest to you.

If you're on Linux or Mac, the easiest way to make JSON data readable is by installing python on your machine
and piping the following command behind the command that produces your JSON data:

    python -m json.tool

Here is a quick example of the JSON we get when we call the JIRA REST API without
the above command:

    curl -X GET -H "Content-Type: application/json" https://jira.atlassian.com/rest/api/2/search?jql=key=JRA-45674

![No Layout JSON Data Screenshot](no-layout.png "No Layout JSON Data Screenshot")

Here is the same request but now readable:

    curl -X GET -H "Content-Type: application/json" https://jira.atlassian.com/rest/api/2/search?jql=key=JRA-45674 | python -m json.tool

![Nice Layout JSON Data Screenshot](layout.png "Nice Layout JSON Data Screenshot")

Quite an improvement if you ask me :)
***
If you're on Windows you could do the following in Powershell to see all the fields of the issues our search returns:

    Invoke-RestMethod -Method Get -Uri https://jira.atlassian.com/rest/api/2/search?jql=key=JRA-45674 | Select-Object -Property issues -ExpandProperty issues | Select-Object -Property fields -ExpandProperty fields

![Powershell Results](powershell.png "Powershell REST API results.")

***

If you want to start handling the JSON data right from the command line you can
take a look at <a href="https://stedolan.github.io/jq/">JQ</a>, which is a lightweight
and flexible command-line JSON processor and which will be the subject of another <a href="https://developer.atlassian.com/blog/categories/totw/">Tip of the Week</a>.
