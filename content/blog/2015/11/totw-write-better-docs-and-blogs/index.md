---
title: "Tip of the Week - Seven tips to write more fluently."
date: "2015-11-19T9:00:00+07:00"
author: "pvandevoorde"
categories: ["totw"]
lede: "Writing documentation, blogs, and tutorials is often part of a developer's job.
 But it's something most of us don't feel comfortable doing.
 In his talk <a href=\"https://twitter.com/mojavelinux/\">Dan Allen</a> inspired me with his 7 tips to write more fluently.
 And I now want to share these tips with you. "
---
Last week I went to Devoxx Belgium and I learned something new:
7 tips to write more fluently.
These tips can be applied on documentation, blogs, and even books.
All things we as developers might some day need to write.

So let me please share the tips I learned from <a href="https://twitter.com/mojavelinux/" target="new">Dan Allen</a>:

1. Write in plain text
-----------------------
Write using <a href="http://www.methods.co.nz/asciidoc/" target="new">AsciiDoc</a> or plain-text editor of your choice.
This helps you to free your mind in the closest format to thought, plain text.
This also helps you to keep a clean separation of content and presentation.
So you can focus on content and get a beautiful result.

2. Answer a question
---------------------
Dan talks about how most of us are ok with replying to e-mails.
But when it comes to writing documentation or other long-form writing,
we clam up.

So why not simply write as if you are answering a question?
Start with an outline of questions and answer those questions,
then go back and change those questions to statements in the final version.

The key is to get all the information out of your head and onto the page.
You can organize and shape it later on.

3. Sentence-per-line
---------------------
Simply write one sentence per line like you do when writing code.

Dan mentions the following benefits:
- It feels natural (matches how we write code).
- It keeps changes localized (because edits do not cause reflow).
- It's easier to diff.
- You can easily rearrange or comment out sentences.
- You can add commentary at the sentence level.
- It encourages shorter sentences; edit with a knife!


4. Write in comments
---------------------
Again something we do automatically in our code: writing comments.
Once you write using Sentence-per-line,
you can use line or block comments quite effectively to try out content,
swapping it in and out of place.
Comments give you some place to put ideas, notes,
reminders, and drafts directly adjacent to the source.

5. Power thesaurus
-------------------
Dan loves using a thesaurus.
His favorite is <a href="https://www.powerthesaurus.org/">Power Thesaurus</a>.
The thesaurus is a mediator between your unconscious and conscious.
Your unconscious knows what it wants, but your conscious isn’t getting this information.
The thesaurus is the secret to getting that information across the divide and onto the page.

6. Visualize progress
----------------------
The hardest part about writing is getting started.
Visualizing your progress can be very motivating.
So make sure that you always have an up-to-date preview of your content available.
It will help you keep the big picture throughout the writing process.

7. Couch read
--------------
Couch reading is the practice of reading your content away from your PC.
Make yourself comfortable and read through your document on your phone or tablet.
You'll be surprised about the amount of errors you'll discover.
This works because it shifts your "locus of attention" to reading (and only reading).

***
And here is Dan's entire presentation straight from YouTube:
<br></br>
<center><iframe width="560" height="315" src="https://www.youtube.com/embed/r6RXRi5pBXg" frameborder="0" allowfullscreen></iframe></center>

The full transcript is also available: <a href="http://mojavelinux.github.io/presentation-write-fluently/transcript.html" target="new">7 Ways to Hack Your Brain to Write Fluently: Transcript</a>.
Feel free to share your tips below in the comments or ping me on Twitter: <a href="https://twitter.com/pvdevoor" target="new">@pvdevoor</a>
