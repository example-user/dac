---
title: "Creating REST APIs for HipChat commands"
date: "2015-07-10T15:00:00+07:00"
author: "tsmith"
categories: ["HipChat", "Python"]
---

I exist in a world that uses Python to power some pretty [awesome sites][bitbucket] and internal
tools. From time to time there are REST APIs that I keep on hitting, it would be nice to turn
one into a [custom slash command] for [HipChat]. I'll walk you through how I did that for an API.

You can find the code for [pypples], a small API just does geocoding for a given address &ndash;
backed by Google. It's running on Heroku now. The project is pretty simple...

```
.
|____ LICENSE.txt
|____ Procfile
|____ pypples
| |____ __init__.py
| |____ api
| | |____ __init__.py
| | |____ address_to_geocode.py
| | |____ resources.py
|____ README.md
|____ requirements.txt
|____ runserver.py
```

[`pypples/__init__.py`][__init__.py] starts up a [Flask] app and
[`pypples/api/__init__.py`][api/__init__.py] sets up some defaults for [Flask-RESTful], and lets
[`pypples/api/resources.py`][api/resources.py] create a couple REST endpoints (`/geo`,
and `/hipchat/geo`). I extracted the actual work the API executed into
[`pypples/api/address_to_geocode.py`][api/address_to_geocode.py], so the two endpoints in
`resources.py` could share that code. Not much effort to this API deployed to Heroku
using the [Heroku Toolbelt]:

```bash
heroku create pypples
git push heroku master
heroku ps:scale web=1
```

_And if you're new to Python & Heroku, check out Heroku's [Getting Started with Python] article._

So now I have two endpoints at https://pypples.herokuapp.com/geo and
https://pypples.herokuapp.com/hipchat/geo. Because slash commands in HipChat post data to the
endpoint, the second endpoint (`/hipchat/geo`) accepts HTTP POSTs.

Now I want to hook this up to HipChat, it's pretty simple. Go to [your rooms], login, and find a
room that you want to add functionality to. Once there, find the "Integrations" option in the left
navigation.

![Select BYO Integrations](select-byo.png)

Find and click on "Build your own integration" and then name your integration.
The name isn't functionally important, it just helps you find the configuration later.

![Name your integration](enter-byo-name.png)

Hit next once you name your integration and scroll on down to create a new slash command. We need
the command and a URL to post data to.

![Configure the slash command](configure-byo-endpoint.png)

After this effort, you should be able to hit your slash command in your HipChat room!

![slash commands at work](geo-lookup.png)

So feel free to use [pypples] as a template for building APIs you might share between normal
REST consumers and HipChat. Good luck doing the same!

[bitbucket]: https://bitbucket.org
[custom slash command]: https://help.hipchat.com/knowledgebase/articles/64451-work-faster-with-slash-commands
[HipChat]: https://www.hipchat.com/
[pypples]: https://bitbucket.org/travisthetechie/pypples
[Flask]: http://flask.pocoo.org/
[Flask-RESTful]: https://flask-restful.readthedocs.org/en/0.3.3/
[your rooms]: https://atlassian.hipchat.com/rooms?t=mine
[__init__.py]: https://bitbucket.org/travisthetechie/pypples/src/32f79e384133b36ae6883604ce8bfd66e00a5bea/pypples/__init__.py
[api/__init__.py]: https://bitbucket.org/travisthetechie/pypples/src/32f79e384133b36ae6883604ce8bfd66e00a5bea/pypples/api/__init__.py
[api/resources.py]: https://bitbucket.org/travisthetechie/pypples/src/32f79e384133b36ae6883604ce8bfd66e00a5bea/pypples/api/resources.py
[api/address_to_geocode.py]: https://bitbucket.org/travisthetechie/pypples/src/32f79e384133b36ae6883604ce8bfd66e00a5bea/pypples/api/address_to_geocode.py
[Heroku Toolbelt]: https://toolbelt.heroku.com/
[Getting Started with Python]: https://devcenter.heroku.com/articles/getting-started-with-python
