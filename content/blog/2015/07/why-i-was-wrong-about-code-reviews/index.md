---
title: "Why I was wrong about code reviews"
date: "2015-07-21T06:00:00+07:00"
author: "shaffenden"
categories: ["pull requests", "code reviews"]
---
I submitted my first code to JIRA Software which turned out to be a really positive experience, something which genuninely surprised me; in fact my conclusions left me feeling that I'd been unfairly harsh on the whole code review process and that I needed to change my perspective. Perhaps you do too?

If you've ever had to go through a process of code review I'm sure you'll be familiar with the time honoured tradition of raising a pull request and having your hard written code critiqued. This is something that I've traditionally disliked. It's slow, it's distracting, and I've never been a fan of people telling me I'm wrong. However, being new to the team I thought it best to do things the right way and I found myself relying heavily on the code review process. It wasn't just a rubber stamp on my work, but a proces that helped me identify inconsistencies in my code style and if I'm honest to validate that I was still capable of writing Java.

##Where did this come from?
There's a UK hiphop artist, [Scroobius Pip](http://www.scroobiuspip.co.uk/ "Scroobius Pip") and you'd be forgiven for not being familiar with him, but in the context of this blog post he's important for penning the following lyrics:

> If your only goal's to be as good as Scroobius Pip  
> Then as soon as you achieve that your standards have slipped  
> If your goal is always to improve on yourself  
> Then the quest is never over no matter how big your wealth  

You see, it's important that we all strive to be better at everything that we do. That doesn't mean comparing ourselves to others, but rather taking the time to look at ourselves at to compare the way we do things now against the way we did things yesterday and the way we want to do things tomorrow. Improving our own skills and techniques is just good practice and it makes everyday a challenge.

##How's your code review technique?
Code reviews are an integral part of development culture at Atlassian, but it's easy to treat them with a certain amount of disdain. How often have you found yourself on the receiving end of a code review and only giving it a cursory glance before clicking the approve button or waiting util someone else completes a code review before submitting your own approval without checking the changes? And when you create a code review do you take the time to provide context around your changes or setup an instance to allow your reviewers to validate your changes easily? Have you ever had a code review where you've seen the feedback you received as a criticism of your ability rather than an opportunity to hone your skills? Now obviously, these are some fairly sweeping generalisations after all, every code review is unique but it'd be surprising if we hadn't all had experiences similar to those listed at one time or another.

The thing is, different people will identify with the role a code review plays in the different ways. A method of identifying errors in our code, a form of testing, a rubber stamping process, and human linting are all perceptions that might be conjured up when talking about this task. In order to make the most out of each code review it's worth considering the benefits to our own developmet, or to put it more succinctly: How does this review to help me grow as a developer?

##How does a review help me grow as a developer?
The crux of the matter is that the code review process is a great opportunity to take stock of our work. It might be finding better, more effective ways of implementing solutions or practicing the valuable art of providing concise, easy to understand feedback. Code reviews could also help in improving our depth of knowledge in a specific product, library, or language; they may even just increase our ability to read, understand, and reason about other peoples code. There's always something new to learn in even the most mundane of code reviews. Next time you're starting a code review — whether it be as a reviewer or as someone creating a review — start by asking yourself how it can help you to grow as a developer. Hopefully it will lead to a more fruitful experience for you and everyone else involved and, as we all grow the quality of the things we produce will grow too.
