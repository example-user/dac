---
title: "Let's build a Bitbucket add-on in Clojure! - Part 3: Creating our API"
date: "2016-01-08T06:00:00+07:00"
author: "ssmith"
description: "Part 3 of the developing Atlassian Connect Bitbucket integration in Clojure. This installment covers creating REST APIs."
categories: ["bitbucket", "atlassian-connect", "clojure", "bbclojureseries"]
---

<style>
  .float-image {
      display: block;
      margin: 15px auto 30px auto;
  }
  .shadow-image {
      display: block;
      margin: 15px auto 30px auto;
      box-shadow: 10px 10px 15px 5px #888888;
  }
</style>


In [part 2 of this series][part2] we built upon the foundations we created in
[part 1][part1] to generate a [Connect descriptor][descriptor-doc]. That
descriptor specifies, among other things, the API that Bitbucket should call
on key events in our repository and add-on lifecycle. In this installment we're
going to look at how to specify and serve this API, and how to convert JSON sent
to us by Bitbucket into Clojure data-structures.

# Serving our API

If we take a closer look at [our descriptor][hello-connect-descriptor] we can
see we define the followinhcg API:

* `/installed`: Called on installation to an account and provides a shared key.
* `/uninstalled`: Called on uninstall from an account.
* `/webhook`: Called on repository events such as pushes.
* `/connect-example`: Called to be inserted into a webpanel in the repository.

We'll cover the webpanel component later on; for now we'll look at the REST
endpoints. The most important of these is `/installed`, which is called by
Bitbucket when a user adds the repository to an account. This provides the
add-on with some key information the it needs to store for future use,
including a shared secret to verify future calls and authorise calls to
the Bitbucket API, a unique per-installation key, and information about the
installing user.

Obviously we need to store some of this information across restarts of our
add-on, so we need some storage. In a full production environment we'd probably
use a database of some sort (for example the Docker Hub add-on
[uses Amazon's DynamoDB][storage.clj]); however for this example we can just
abuse Clojure's ability to dump and read runtime data in its [EDN] format. Let's
create a new `storage.clj` file and add some save/load operations:

```` clojure
(ns hello-connect.storage
  (:require [clojure.string :as string]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [environ.core :refer [env]]))

(defonce addon-ctx (atom nil))
(defonce context-file (str (env :data-dir "/tmp") "/addon-context"))


(defn load-addon-context []
  ;; Load up shared key if we have one
  (when (.exists (io/as-file context-file))
    (reset! addon-ctx (edn/read-string (slurp context-file)))
    (log/info "Loaded addon context of" @addon-ctx)))

(defn save-addon-context [ctx]
  (reset! addon-ctx ctx)
  (io/delete-file context-file  {:silently true})
  (spit context-file ctx))

(defn delete-addon-context []
  (io/delete-file context-file {:silently true})
  (reset! addon-ctx nil))

(defn shared-secret []
  (@addon-ctx "sharedSecret"))

(defn client-key []
  (@addon-ctx "clientKey"))
````

(*Note*: This is a very simplistic implementation and should not be used in a
real add-on. In particular it's not multi-tenanted as it doesn't separate storage
per installation via the `clientKey` field. But it will do for simple
illustration purposes.)

Now we have something to do with the information sent to us on installation
let's handle the API call. Bitbucket sends the installation context as JSON in
the body of a POST to the endpoint; we'd prefer that the information was
converted into Clojure's internal data structures. There are
[libraries][clojure-json] to do this, but as it's such a common operation [Ring]
provides a [handy wrapper][ring-json] that will do this for us. To enable this,
just import `wrap-json-body` from `ring.middleware.json` and add it to our
routing stack:

```` clojure
(def app
  ;; Disable anti-forgery as it interferes with Connect POSTs
  (let [connect-defaults (-> site-defaults
                             (assoc-in [:security :anti-forgery] false)
                             (assoc-in [:security :frame-options] false)) ]

    (-> app-routes
        (wrap-defaults connect-defaults)
        (wrap-json-body))))
````

This will check the `Content-Type` of incoming requests and parse any JSON body
into Clojure.

## Installation API

Now we can add the API endpoint to the routes; first add the following to
`handler.clj`:

```` clojure
(defn process-installed [params body]
  (log/info "Received /installed")
  (storage/save-addon-context body)

  {:status 204})
````

This will save the installation context and return an empty OK status. Then we
add the endpoint to the `defroutes` section:

```` clojure
(POST "/installed" {params :query-params body :body}
      (process-installed params body))
````

We should also process `/uninstalled` calls. While not a major issue in our toy
implementation we generally would like to clear unneeded data out of our
database, and it is generally good practice to not store user information
unnecessarily. However we don't want to allow just anybody to make an uninstall
call; in fact, we'd like all our API to be authenticated as coming from
Bitbucket from now on. Luckily that's what the installation information above is
for, in particular the `sharedSecret` entry in the context.

Obviously we'd like this context to be loaded on future runs of our add-on, so
let's load it on startup in `handler.clj`:

```` clojure
(defn init []
  (log/info "Initialising application")
    (storage/load-addon-context))
````

## Installation data

The method Atlassian Connect uses to pass authentication to an add-on is [JWT],
a web-standard for encoding authorisation claims. There are
[already Clojure JWT libraries available][clj-jwt], but Atlassian's Connect
[defines an extension][JWT-qsh] to the standard to add additional verification
of query parameters. However as part of writing the
[Docker Hub Bitbucket add-on][bb-docker-connect] I produced
[a library][clj-connect] to implement this extension, along with some helper
operations. This allows to define a simple Ring-style wrapper that will
authenticate a request and either deny access or forward onto another handler:

```` clojure
(defn wrap-jwt-auth [request handler]
  (if (not (jwt/verify-jwt request (storage/shared-secret)))
    {:status 401}
    (handler request)))
````

## Uninstalling

Now we can create a handler to do the uninstallation (which with our naive
storage implementation is just a delete):

```` clojure
(defn process-uninstalled [request]
  (log/info "Received /uninstalled")
  (storage/delete-addon-context)
  {:status 204})
````

Then we add a route for `/uninstalled` that calls the uninstaller wrapped in
authentication:

```` clojure
(POST "/uninstalled" request
      (wrap-jwt-auth request process-uninstalled))
````

We'll use this method for the rest of our API.

## Webhooks

The last REST endpoint we need to create is `/webhook`. The webhook is called
for any key events in the repository, for example on a `push` event. We're not
going to actually do anything with this information for our plugin, but it's
useful to see what we receive from Bitbucket. So we'll create a function to
pretty-print the received data:

```` clojure
(defn process-webhook [request]
  (log/info "Received /webhook of:\n" (clojure.pprint/pprint (request :body)))
  {:status 204})
````

And again we just define and authenticate the endpoint:

```` clojure
(POST "/webhook" request
      (wrap-jwt-auth request process-webhook))
````

## The code

The code for this part of the tutorial series is available in
[this tag][part3-code] in the
[accompanying Bitbucket repository][hello-connect]. There will also code
appearing there for the later parts as I work on them if you want to skip ahead.

# Next time

Now we've handled the Bitbucket to add-on aspects of the API we need to
address the more complex issue of retrieving information from Bitbucket and
creating user-visible content from it. In the next part we'll look at how to do
that using both the Bitbucket REST API and a pure-Javascript implementation.
We'll use both of these techniques to display some key information in an
embedded component on the Bitbucket repository page. Tune in next-time for more
Clojure goodness!


[bb-connect-announce]: https://blog.bitbucket.org/2015/06/10/atlassian-connect-for-bitbucket-a-new-way-to-extend-your-workflow-in-the-cloud/
[connect-intro]: https://developer.atlassian.com/bitbucket/guides/introduction.html
[JWT]: https://en.wikipedia.org/wiki/JSON_Web_Token
[JWT-qsh]: https://developer.atlassian.com/bitbucket/concepts/understanding-jwt.html
[clj-connect]: https://bitbucket.org/ssmith/clj-connect
[Leiningen]: http://leiningen.org/
[Boot]: http://boot-clj.com/
[Maven]: https://maven.apache.org/
[JDK]: http://www.oracle.com/technetwork/java/javase/downloads/index.html
[Luminus]: http://www.luminusweb.net/
[composable]: https://en.wikipedia.org/wiki/Composability
[lein ancient]: https://github.com/xsc/lein-ancient
[Compojure]: https://github.com/weavejester/compojure
[Ring]: https://github.com/ring-clojure/ring
[Swagger]: http://swagger.io
[12 Factor]: http://12factor.net/
[Jetty]: http://www.eclipse.org/jetty/
[Immutant]: http://immutant.org/
[Undertow]: http://undertow.io/
[s-expressions]: https://en.wikipedia.org/wiki/S-expression
[Handlebars]: http://handlebarsjs.com/
[ERB]: http://apidock.com/ruby/ERB
[Selmer]: https://github.com/yogthos/Selmer
[Django]: https://www.djangoproject.com/
[ngrok]: https://ngrok.com/
[bb-start]: https://developer.atlassian.com/bitbucket/guides/getting-started.html
[lein-profiles]: https://github.com/technomancy/leiningen/blob/master/doc/PROFILES.md
[storage.clj]: https://bitbucket.org/ssmith/bitbucket-docker-connect/src/HEAD/src/clojure/docker_connect/storage.clj?at=master
[EDN]: https://github.com/edn-format/edn
[clojure-json]: https://github.com/clojure/data.json
[ring-json]: https://github.com/ring-clojure/ring-json
[clj-jwt]: https://github.com/liquidz/clj-jwt
[bb-docker-connect]: https://bitbucket.org/ssmith/bitbucket-docker-connect
[Bitbucket]: https://bitbucket.org/
[Clojure]: http://clojure.org/
[docker-hub-announce]: https://developer.atlassian.com/blog/2015/09/docker-bitbucket/
[tpettersen]: https://developer.atlassian.com/blog/authors/tpettersen/
[bb-node-example]: https://developer.atlassian.com/bitbucket/guides/getting-started.html
[Cider]: https://github.com/clojure-emacs/cider
[Fireplace]: https://github.com/tpope/vim-fireplace
[La Clojure]: https://plugins.jetbrains.com/plugin/4050
[Cursive]: https://cursiveclojure.com/
[descriptor-doc]: https://developer.atlassian.com/bitbucket/descriptor/
[HTTP/2]: https://http2.github.io/
[xdm]: https://en.wikipedia.org/wiki/Web_Messaging
[ClojureScript]: https://github.com/clojure/clojurescript
[core.async]: https://github.com/clojure/core.async
[threading]: https://clojuredocs.org/clojure.core/-%3E
[uberjar]: https://github.com/technomancy/leiningen/blob/master/doc/TUTORIAL.md#uberjar
[AOT]: http://clojure.org/compilation
[curl]: http://curl.haxx.se/
[bitbucket-js]: https://developer.atlassian.com/bitbucket/concepts/javascript-api.html
[closure]: https://en.wikipedia.org/wiki/Google_Closure_Tools
[cljs-rationale]: https://github.com/clojure/clojurescript/wiki/Rationale
[clj-peristent]: https://en.wikipedia.org/wiki/Persistent_data_structure
[cljsbuild]: https://github.com/emezeske/lein-cljsbuild
[Figwheel]: https://github.com/bhauman/lein-figwheel
[hello-connect]: https://bitbucket.org/ssmith/hello-connect/
[hello-connect-descriptor]: https://bitbucket.org/ssmith/hello-connect/src/4810770708e659b6c20dac0e021740f56238dc06/resources/views/atlassian-connect.json.selmer?at=master&fileviewer=file-view-default
[part1]: /blog/2015/12/clojure-connect-part-1/
[part2]: /blog/2015/12/clojure-connect-part-2/
[part3-code]: https://bitbucket.org/ssmith/hello-connect/commits/tag/part-3
