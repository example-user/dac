---
title: "Tip of the Week - How to move a full Git repository."
date: "2016-01-22T9:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","Git"]
lede: "This week I'll discuss how you can move a full Git repository, including branches, tags, and history from one repository to another."
---
This week I'll show you how you can move a full Git repository from one remote server to another.
The steps I'm using even allow you to choose which branches and tags to include.

Let’s call the original repository ORI and the new one NEW, here are the steps I took to copy everything from ORI  to NEW:

1. Create a local repository in the **temp-dir** directory using:
        git clone <url to ORI repo> temp-dir
![Git clone original repository into local temp-dir](clone-ori-repo.png "Clone original repository locally.")
2. Go into the **temp-dir** directory.
3. To see a list of the different branches in ORI do:
        git branch -a
![See all the Branches](git-branch-a.png "Show both local and remote branches.")
4. Checkout all the branches that you want to copy from ORI to NEW using:
        git checkout branch-name
![Checkout all branches](checkout-branches.png "Checkout all the branches you want to keep.")
5. Now fetch all the tags from ORI using:
        git fetch –-tags
![Fetch all the tags](git-fetch-tags.png "Fetch all the tags from the repository.")
6. Before doing the next step make sure to check your local tags and branches using the following commands:
        git tag
       git branch -a
![Git tag and Git branch to check if we have everything we need.](git-tag-and-git-branch-a.png "Check if you got all you needed from your repository.")
7. Now clear the link to the ORI repository with the following command:
        git remote rm origin
8. Now link your local repository to your newly created NEW repository using the following command:
        git remote add origin <url to NEW repo>
9. Now push all your branches and tags with these commands:
        git push origin –-all
       git push –-tags
![The End Result of our 10 steps.](end-result.png "The end result.")
10. You now have a full copy from your ORI repo.

## Extra:

If you want to simply copy the entire repository you can use

    git clone --mirror <url to ORI repo> temp-dir

to replace step 1 to 5.

***

Feel free to provide me with feedback by mentioning me: [@pvdevoor](http://twitter.com/pvdevoor "Peter Van de Voorde") or [@atlassiandev](http://twitter.com/atlassiandev "Atlassian Dev Twitter Account") on twitter or in the comments!
