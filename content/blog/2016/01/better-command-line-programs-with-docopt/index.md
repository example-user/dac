---
title: "Build better command-line programs with docopt"
date: "2016-01-27T09:30:00+07:00"
author: "ibuchanan"
categories: ["apis", "scripting", "tools"]
lede:  "Do you write a lot of command-line tools?
        I've written them in C, Java, C#, Ruby,
        and most recently Python.
        I looked at the standard `argparse` library in Python.
        But here I was learning yet-another-argument-parser-library.
        After a little research, I discovered docopt works in many languages.
        Here's why docopt has become my favorite tool for argument parsing."
description:   "Ian Buchanan describes
                how you can get started using docopt,
                the command-line description language
                for creating beautiful command-line interfaces
                with any programming language."
---

Just before Thanksgiving last year,
my colleague [Tim Pettersen][tim] wrote an article
about [building command-line tools with Node.js][nodecli].
While I was reviewing the article,
I asked Tim why he likes [`commander`][commander] for parsing command-line arguments.
As with many programming choices,
it came down to what he had used before.
I'm sure most programmers have been down this road before,
especially for command-line parsing.
What works best is something you know.
That's usually fine for me.
But, in the world of command-line interfaces,
I think there is now just _one right way_.
It's [docopt][docopt].

Writing command-line tools is something I've done many times.
I've written them in C, Java, C#, Ruby, and Python.
My latest is a Python program that manipulates Bitbucket [Snippets][snippet].
When I started about a year ago,
there was an itch I just couldn't scratch.
I looked at the standard `argparse` library in Python.
The code seemed easy enough.
It automatically generates command-line help,
which helps keep documentation up-to-date with the code.
But here I was learning yet-another-argument-parser-library.
I wanted something that would keep me consistent between languages,
not just within Python.

For web applications,
I have been following the growing trend of [API Description Languages][apidls],
such as [Swagger][swagger], [RAML][raml], [I/O Docs][iodocs], and [API Blueprints][apiblueprints].
For some, these tools are reminiscent of heavyweight technologies like SOAP and WSDL.
For me, these solve the important need to keep consistency between code and documentation.
They are especially helpful because web APIs can be consumed by any language.
And so it was that I found myself Googling for
"command-line interface description language".
Fortunately for me, I wasn't the first with the idea.
At [PyCon UK 2012][pyconuk2012-docopt],
Vladimir Keleshev had presented his Python implementation of docopt.
Now, there are parser implementations in every language I had ever used,
and for every one I might in the future.
A docopt description is simple to write.
In addition to Vladimir's video,
there are ample [documentation][docopt] and [examples][examples] to learn from.
There's even [a nifty docopt web tool][web-docopt]
to try out descriptions in a browser.
Here's the description language for my Snippet CLI tool:

``` docopt
A command-line interface for creating, inspecting, and editing
Bitbucket Snippets.

usage:
    snippet list [--owner|--contributor|--member]
    snippet create [--title=<title>] [--public|--private] [FILES ...]
    snippet modify <id> [--title=<title>] [--public|--private] [FILES ...]
    snippet delete <id>
    snippet info <id>
    snippet files <id>
    snippet content <id> <filename>
    snippet watchers <id>
    snippet comments <id>
    snippet commits <id>
```

This description is exactly what the program emits with an implicit `--help` or `-h`,
or whenever the provided command-line arguments don't match the syntax.
Even though it serves as documentation,
it is also a full description of the grammar.
Binding the description language to my code is also simple.
In Python, I can just place the above as comments in my `main.py`.
(In other languages, it is often just a string variable.)
Python lets me access the code comments with [the special doc string symbol `__doc__`][docstring],
which I pass it into `docopt()` like this:

``` python
def main():
    arguments = docopt(__doc__)
```

When my program runs,
`arguments` will be populated with a dictionary of parsed values.
Here's a JSON representation for the arguments `create --title=Example --public example.py`:

``` json
{
  "--contributor": false,
  "--member": false,
  "--owner": false,
  "--private": false,
  "--public": true,
  "--title": "Example",
  "<filename>": null,
  "<id>": null,
  "FILES": [
    "example.py"
  ],
  "comments": false,
  "commits": false,
  "content": false,
  "create": true,
  "delete": false,
  "files": false,
  "info": false,
  "list": false,
  "modify": false,
  "watchers": false
}
```

From here, my program is responsible for dispatching into the right code.
This will vary by language,
but you can see how I dispatched in [my `main.py` method for Snippet][snippet-main].

While my example shows how easy it is for a simple case,
I have noticed some much more complex examples.
One nice thing is the subcommands can be distributed in separate files.
In the Python docopt repository,
there is [a sample using the Git command structure][docopt-git].
In a real-world example,
the developers at ActiveState wrote [a tool with 60 subcommands][kato].

I realize that some developers might object to docopt
because their favorite command-line framework does much more than just parse arguments.
For example, even the standard Python `argparse` will automatically dispatch to Python methods.
(For what it's worth, Keleshev has written [an additional Python library to perform dispatching][docopt-dispatch].)
More extensive libraries like Python's [`clint`][clint] help format text tables,
write color output,
and collect user input.
But I hope for the best of both worlds.
Ideally, docopt might be a plugin to a broader CLI framework.

If you found this useful,
or have your own tips about command-line tools,
tweet me at [@devpartisan][devpartisan] or my team at [@atlassiandev][atlassiandev].

[tim]: https://twitter.com/kannonboy "Tim Pettersen is @kannonboy on Twitter."
[nodecli]: https://developer.atlassian.com/blog/2015/11/scripting-with-node/ "Tim Pettersen (17 November 2015). Building command line tools with Node.js."
[commander]: https://www.npmjs.com/package/commander "Commander is node.js command-line interfaces made easy"
[docopt]: http://docopt.org/ "Docopt is a command-line interface description language."
[snippet]: https://bitbucket.org/atlassian/snippet "Snippet is a command-line interface for creating, inspecting, and editing Bitbucket Snippets."
[apidls]: https://www.youtube.com/watch?v=vu8_QLkW1mg "Laura Heritage (15 August 2014). API Description Languages: Which One Is Right For Me?"
[swagger]: http://swagger.io/ "Swagger declares itself the world's most popular framework for APIs."
[raml]: http://raml.org/ "RESTful API Modeling Language (RAML) declares itself the simplest way to design APIs."
[iodocs]: http://www.mashery.com/api/io-docs "Mashery I/O Docs lets developers execute calls directly from API documentation."
[apiblueprints]: https://apiblueprint.org/ "API Blueprint declares itself a powerful high-level API description language for web APIs."
[pyconuk2012-docopt]: https://youtu.be/pXhcPJK5cMc "Vladimir Keleshev (3 October 2012). PyCon UK 2012: Create beautiful command-line interfaces with Python."
[examples]: https://github.com/docopt/docopt/tree/master/examples "Docopt Examples in Python."
[docstring]: http://epydoc.sourceforge.net/manual-docstring.html "Python documentation strings (or docstrings) associate documentation with Python code. Docstrings can be accessed from the interpreter and from Python programs using the __doc__ attribute."
[web-docopt]: http://try.docopt.org/ "Try docopt."
[snippet-main]: https://bitbucket.org/atlassian/snippet/src/master/snippet/main.py "Main processes the docopt dictionary structure and dispatches to methods in the command module."
[docopt-git]: https://github.com/docopt/docopt/tree/master/examples/git "The Git command structure provides demonstrates how complex parsers can be segmented into subcommands."
[kato]: https://www.activestate.com/blog/2014/04/18-months-docopt-polyglot-cli-option-parser "Phil Whelan (3 April 2014). 18 Months of docopt - A Polyglot CLI Option Parser."
[docopt-dispatch]: https://github.com/keleshev/docopt-dispatch "Docopt-dispatch is a Python library for dispatching to appropriately annotated functions."
[clint]: https://github.com/kennethreitz/clint "Clint is a module filled with a set of awesome tools for developing command-line applications."
[devpartisan]: https://twitter.com/devpartisan "Ian Buchanan is @devpartisan on Twitter."
[atlassiandev]: https://www.twitter.com/atlassiandev "Atlassian's Developer Advocates for Developer Tools are @atlassiandev on Twitter."
