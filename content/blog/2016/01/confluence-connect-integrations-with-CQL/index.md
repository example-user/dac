---
title: "These are the content properties you're looking for: Confluence Connect integrations and CQL"
date: "2016-01-18T16:00:00+07:00"
author: "kwestwalker"
categories: ["Atlassian Connect", "Confluence", "add-ons"]
lede: "The Confluence platform team has been building more ways for your Connect add-ons to integrate with and extend search in Confluence. Initially announced at AtlasCamp 2015, we've shipped some awesome new features that allow you to get the most out of content properties."
---

The Confluence platform team has been building more ways for your Atlassian Connect add-ons to integrate with and extend search 
in Confluence. We've added Connect support for [CQL] query aliases and the ability for your add-ons to integrate with 
search filtering. Initially announced at AtlasCamp 2015, these awesome new features allow you to get the most out of content properties.

## Let's recap. What are content properties?

[Content properties] are a way to store key-value pairs against a piece of content in Confluence. They allow your 
Connect add-ons to store associated JSON directly in Confluence using the REST API, removing the need for a backend 
server for static add-ons. Where they get really powerful is when they're used in conjunction with [CQL], allowing you 
to filter your search results using the data you previously saved on the page. 

## Cool! What are the new features?

### Aliases

One of the best things about content properties is the CQL integration. Defining your own search indexes allows you 
to hook into Confluence search and grab content based on the information you've previously saved as a 
content property. For example, you might have saved a calculated ranking in every Confluence page, and you want to 
pull up pieces of content based on that. This is what that CQL query might have looked like before:

<br>
<div style="text-align: center;">
    <img style="width: 70%; height: 70%; display: inline-block" src="cql-before.png">
</div>

Now, with aliases, that can be simplified down to the query below, just by adding a single `alias` field to the 
definition of your content Property.

<br>
<div style="text-align: center;">
    <img style="width: 70%; height: 70%; display: inline-block" src="cql-after.png">
</div>

### How do I use them?

In the definition of your content Property in your `atlassian-connect.json`, use the `alias` field to define a simple 
name to refer to that property by. It should look something like this:

``` javascript
    { 
        "key": "content-properties-rank", 
        "name": {
            "value": "Rank"
        }, 
        "keyConfigurations": [ 
            { 
                "propertyKey": "myAddonRank", 
                "extractions": [ 
                    { 
                        "objectName": "value",
                        "type": "number", 
                        "alias": "rank" 
                    }
                ] 
            }
        ]
    }
```

For more context on how these work, head over to the Atlassian Connect documentation on [aliases]. 

### User interface support

To extend the support for filtering content, we need to integrate it into lots of different 
places in Confluence. We want to be able to filter content not just as developers querying the REST API, but also 
provide that capability to end users. With [UI support], you can add powerful search filters based on your content properties 
to any existing macros that aggregate and display content, e.g. the [content by label macro]. For example, say you had saved a 'category' 
property for each piece of content on your Confluence instance, and wanted to display content filtered by this property:

<br>
<div style="text-align: center;">
    <img style="width: 90%; height: 90%; display: inline-block" src="enhanced-macros.png">
</div>

In addition to enhanced macros, your filters will also show up in the search screen, allowing users to refine their 
search queries.

<br>
<div style="text-align: center;">
    <img style="width: 90%; height: 90%; display: inline-block" src="search-screen.png">
</div>

### How do I use it?

Defining the `uiSupport` object in your `atlassian-connect.json` provides the information needed for Confluence 
to include it in all the CQL UI filtering locations available, such as enhanced macros and the search screen. The 
`defaultOperator` field decides what will be used as a comparator in the UI, while the [`dataUri`] defines an endpoint 
in your add-on which returns a list of valid values for this content property. 

``` javascript
    "confluenceContentProperties": [ 
        { 
            "key": "content-properties-category", 
            "name": { 
                "value": "Category"
            }, 
            "keyConfigurations": [ 
                { 
                    "propertyKey": "myAddonCategory", 
                    "extractions": [ 
                        { 
                            "objectName": "value", 
                            "type": "string", 
                            "alias": "category", 
                            "uiSupport": { 
                                "defaultOperator": "=", 
                                "dataUri": "/rest/semantix/category", 
                                "valueType": "string", 
                                "name": { 
                                    "value": "Category" 
                                } 
                            } 
                        } 
                    ] 
                } 
            ] 
        }
    ]
```
 

### One more thing... the content property toolbar 

In addition to building these new features, we've also put together a developer add-on to improve the process of 
working with content properties while working on your Connect add-on. This allows you to quickly see the state of 
content properties set in the current page, without having to do a cURL call to the REST API each time you want to 
check them. You can grab the content property toolbar from [Bitbucket]. 

<div style="text-align: center;">
    <img style="width: 50%; height: 50%; display: inline-block" src="content-property-toolbar.png">
</div>

You can also get more information on these new features and how they fit into the Confluence Platform by checking out 
[Ben Mackie and Matthew Jensen's talk] from AtlasCamp 2015. Head over to the [content property documentation] to 
get started!


  [Content properties]: https://developer.atlassian.com/confdev/confluence-rest-api/content-properties-in-the-rest-api
  [content property documentation]: https://developer.atlassian.com/static/connect/docs/latest/modules/confluence/content-property.html
  [CQL]: https://developer.atlassian.com/blog/2015/02/confluence-5-7-apis/
  [`dataUri`]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/user-interface-support.html#dataUri
  [UI support]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/user-interface-support.html
  [Bitbucket]: https://bitbucket.org/kapww/content-property-toolbar
  [Ben Mackie and Matthew Jensen's talk]: https://www.atlassian.com/atlascamp/2015/archives/build-amazing-add-ons/confluence-making-your-life-easier
  [aliases]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/content-property-index-extraction-configuration.html#alias
  [content by label macro]: https://confluence.atlassian.com/doc/content-by-label-macro-145566.html