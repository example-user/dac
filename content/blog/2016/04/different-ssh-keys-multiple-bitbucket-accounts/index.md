---
title: "Tip of the Week: Using different SSH keys for multiple Bitbucket accounts"
date: "2016-04-13T12:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","Git","Bitbucket"]
lede: "This week I'll show you how to let Git automatically select the correct SSH key
when you are using multiple Bitbucket accounts."
description: "Tip on how to let Git auto select the correct SSH key when using multiple Bitbucket accounts."
---
Using multiple accounts with remote repositories can be a SSH key hell.
In this post I'll explain how you can make Git select the correct SSH key for the project you are currently working on.

# Create a key for each of your accounts

To generate a new key pair simply run this command in the ~/.ssh/ folder:

     ssh-keygen -t rsa -C "user1" -f "user1"

The -C option is a comment to help identify the key.
The -f option specifies the file name.

Repeat the above for each Bitbucket account you want to use.

# Add the public key to the correct Bitbucket account

To add a public key to a Bitbucket account, you need to go to the Bitbucket Settings Screen.
Select *SSH Keys* in the left side menu and click *Add key*.

For more detailed information check out the Bitbucket documentation:
- [Add an SSH key to a Bitbucket Cloud account](https://confluence.atlassian.com/bitbucket/add-an-ssh-key-to-an-account-302811853.html?utm_source=dac&utm_medium=blog&utm_campaign=totw).
- [Add an SSH key to a Bitbucket Server account](https://confluence.atlassian.com/bitbucketserver/ssh-user-keys-for-personal-use-776639793.html?utm_source=dac&utm_medium=blog&utm_campaign=totw).

# Configure SSH

In ~/.ssh/ create a file called config with contents based on this:

     #user1 account
     Host bitbucket.com-user1
         HostName bitbucket.com
         User git
         IdentityFile ~/.ssh/user1
         IdentitiesOnly yes

     #user2 account
     Host bitbucket.com-user2
         HostName bitbucket.com
         User git
         IdentityFile ~/.ssh/user2
         IdentitiesOnly yes

Replace user1 or user2 with your Bitbucket usernames

# Getting your keys on a keyring

This is only useful if you use a passphrase to protect your key.
Otherwise you can skip this.

Depending on your operating system you’ll need to find out how best to do this.

Linux users can use [GnomeKeyring](https://wiki.gnome.org/action/show/Projects/GnomeKeyring?action=show&redirect=GnomeKeyring).

Mac users can use the following command to permanently add keys to the Mac SSH Agent:

     ssh-add -K ~/.ssh/user1.rsa

Windows users can take a look here for more info: [Git On Windows](http://guides.beanstalkapp.com/version-control/git-on-windows.html)

You can check the keys on your keyring with:

     ssh-add -L

# Configure your Git repo

If you don't have a local copy of your repo, you have to run the following command to clone a Bitbucket repository:

     git clone git@bitbucket.com-user1:user1/your-repo-name.git    

If you already have a local copy, you'll need to update the origin:

     git remote set-url origin git@bitbucket.com-user1:user1/your-repo-name.git

Now go to the local Git repo that you want to configure and enter:

    git config user.name "user1"
    git config user.email "user1@example.com"

Where user1 matches the values you used earlier in your ssh config.     

You can now git push as normal and the correct key will automatically be used.

I hope you found this tip useful! Check out our earlier [Tips of the Week](https://developer.atlassian.com/blog/categories/totw/) or tweet your own tip suggestions to [@pvdevoor](http://twitter.com/pvdevoor)."

***

This post is based on the following sources:
- [Automatically use correct SSH key for remote Git repo](http://www.keybits.net/2013/10/automatically-use-correct-ssh-key-for-remote-git-repo/)
 by  [Tom Atkins](https://twitter.com/keybits)
- http://unix.stackexchange.com/a/140077 by [slm](http://unix.stackexchange.com/users/7453/slm)
- https://gist.github.com/jexchan/2351996#gistcomment-1623390 by [Adam M Dutko](https://gist.github.com/StylusEater)
