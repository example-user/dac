---
title: "Atlassian update for Git and Mercurial vulnerability"
date: "2014-12-19"
author: "nwade"
categories: ["git"]
---

The maintainers of the Git and Mercurial open source projects have identified a
vulnerability in the Git and Mercurial clients for Macintosh and Windows
operating systems that could allow critical files to be overwritten with
unwanted files, including executables.

Because this is a client-side vulnerability, Bitbucket and Stash themselves are
not affected; however, we recommend that all client users of Git and Mercurial,
including FishEye, Crucible, and SourceTree users, update their Git client...see
all [the details over
here](http://blogs.atlassian.com/2014/12/atlassian-update-git-mercurial-vulnerability/).
