---
title: JIRA Service Desk Modules Web Item Target 40004227
aliases:
    - /jiracloud/jira-service-desk-modules-web-item-target-40004227.html
---
# JIRA Cloud : JIRA Service Desk modules - Web item target

Defines the way a web item link is opened in the browser, such as in a page or modal dialog.

# Example

``` false;
{
  ...,
  "target": {
    "type": "dialog"
  }
}
```

# Properties

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><code>type</code></td>
<td><ul>
<li><strong>Type:</strong> <code>string</code></li>
<li><strong>Default:</strong> <code>page</code></li>
<li><strong>Allowed values:</strong>
<ul>
<li><code>page</code></li>
<li><code>dialog</code></li>
</ul></li>
<li><strong>Description:</strong> Defines how the web-item content should be loaded by the page. By default, the web-item is loaded in the same page. The target can be set to open the web-item url as a modal dialog</li>
</ul></td>
</tr>
</tbody>
</table>



