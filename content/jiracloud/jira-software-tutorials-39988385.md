---
title: JIRA Software Tutorials 39988385
aliases:
    - /jiracloud/jira-software-tutorials-39988385.html
---
# JIRA Cloud : JIRA Software tutorials

The following pages will help you learn how to develop for JIRA Software. Guides provide examples and best practices on JIRA Service Desk concepts or processes. Tutorials provide a more hands-on approach to learning, by getting you to build something with step-by-step procedures.

Check them out below:

-   [Tutorial - Adding a board configuration page] — This tutorial will show you how to build a board configuration page in JIRA Software, by using a JIRA Software module in your Connect add-on.
-   [Tutorial - Adding a dropdown to an agile board] — This tutorial will show you how to add a new dropdown to a board in JIRA Software, by using a JIRA Software module in your Connect add-on.

 

  [Tutorial - Adding a board configuration page]: /jiracloud/tutorial-adding-a-board-configuration-page-39990300.html
  [Tutorial - Adding a dropdown to an agile board]: /jiracloud/tutorial-adding-a-dropdown-to-an-agile-board-39990287.html

