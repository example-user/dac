---
title: Tutorial Adding a Board Configuration Page 39990300
aliases:
    - /jiracloud/tutorial-adding-a-board-configuration-page-39990300.html
---
# JIRA Cloud : Tutorial - Adding a board configuration page

This tutorial will show you how to build a board configuration page in JIRA Software, by using a JIRA Software module in your Connect add-on. We won't go into the details of adding settings to the page, but this tutorial will provide a good starting point if you want your add-on to have separate configurations per board.

To create this board configuration page, you'll be creating a new web panel located in `jira.agile.board.configuration`, then adding code to render the web panel. 

About these Instructions

 You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Sublime Text editor on Mac OS X. If you are using a different IDE and/or OS, you should use the equivalent operations for your specific environment.

This tutorial was last tested with JIRA Software 7.1.0-OD-05.

 **On this page:**

****
-   [Before you begin]
-   [Instructions]
-   [Tips]

## Before you begin

-   If you haven't built a Connect add-on before, read the [Getting started] tutorial. You'll learn the fundamentals of Connect development and build a basic Connect add-on that you can use with this tutorial.

## Instructions

#### Step 1. Add a web panel with board context parameters

To create a board configuration page, you'll need to create a web panel with the label and location set to `jira.agile.board.tools`. 

1.  Edit the **atlassian-connect.json** file in your add-on project.

2.  Add the following code to the `modules` context:

    ``` brush:
    "webPanels": [
          {
            "key": "my-configuration-page",
            "url": "configuration?id={board.id}&type={board.type}",
            "name": {
              "value": "My Configuration Page"
            },
            "location": "jira.agile.board.configuration",
            "weight": 1
          }
        ]
    ```

3.  Save your changes.

#### Step 2. Add code to render the web panel

The server-side code required to render the web panel is shown below. This code is for <a href="https://bitbucket.org/atlassian/atlassian-connect-express" class="external-link">atlassian-connect-express</a>, the node.js framework (with Express), but you can use a different technology stack if you wish.

1.  Navigate to the **routes** directory in your add-on project and edit the **index.js** file.
2.  Add the following code to the file and save it:

    ``` brush:
    app.get('/configuration', function(req,res){
        res.render("configuration", {id : req.query['id'], type : req.query['type'] });
    });
    ```

    This adds the 'configuration' route to your app.

3.  Navigate to the **views** directory in your add-on project and create a new **configuration.hbs** file. 
4.  Add the following code to the **configuration.hbs** file and save it:

    ``` brush:
    <!DOCTYPE html>
    <html>
      <head>
        <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.9.17/css/aui.min.css" media="all">
        <script src="https://ac-getting-started.atlassian.net/atlassian-connect/all.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="//aui-cdn.atlassian.com/aui-adg/5.9.17/js/aui.min.js"></script>
      </head>
      <body class="aui-page-focused aui-page-focused-xlarge">
        <div id="page">
          <section id="content">
            <header class="aui-page-header">
              <div class="aui-page-header-inner">
                <div class="aui-page-header-main">
                  <h2>Example board configuration page</h2>
                  <p id="custom-message">This is an example configuration page for board ID: {{id}} with board type: {{type}}.</p>
                </div>
              </div>
            </header>
          </section>
        </div>
      </body>
    </html>
    ```

    *Notice that we've used two [context parameters]* *used in the template above: `id` (for board Id) and `type` (for board type).*

#### Step 3. Check that it all works

1.  Start up and deploy your add-on (`npm start`) to your JIRA Cloud instance, if it's not running already. 
2.  Navigate to the board configuration for any JIRA Software project. 
3.  Click the **Example board configuration** link. You should see a page like this:
    <img src="/jiracloud/attachments/39990300/39990926.png" width="850" height="431" /> 

 

<img src="/jiracloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" />  **Congratulations!** You've just built a board configuration page for your JIRA Software Connect add-on.

## Tips

-   If you are looking for somewhere to store configuration information, make use of the [board properties].
-   Your add-on can define custom [project] or [global] permissions. This may be useful if you want to limit access to your add-on's board configuration page.

  [Before you begin]: #Tutorial-Addingaboardconfigurationpage-Beforeyoubegin
  [Instructions]: #Tutorial-Addingaboardconfigurationpage-Instructions
  [Tips]: #Tutorial-Addingaboardconfigurationpage-Tips
  [Getting started]: /jiracloud/getting-started-39988011.html
  [context parameters]: /jiracloud/jira-software-context-parameters-39990789.html
  [board properties]: 
  [project]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/project-permission.html
  [global]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/global-permission.html

