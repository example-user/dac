---
title: Latest Updates 39988013
aliases:
    - /jiracloud/latest-updates-39988013.html
---
# JIRA Cloud : Latest updates

We deploy updates to JIRA Cloud frequently. As a JIRA developer, it's important that you are aware of the changes. The resources below will help you keep track of what's happening:

[<img src="/jiracloud/attachments/39988013/39988120.png" class="confluence-thumbnail" height="150" />]

**[JIRA news][<img src="/jiracloud/attachments/39988013/39988120.png" class="confluence-thumbnail" height="150" />] **
*(Atlassian developer's blog)*

Major changes that affect JIRA Cloud developers are announced in this blog, e.g. deprecating APIs. You'll also find handy tips and articles related to JIRA development.

<a href="https://confluence.atlassian.com/display/Cloud/What%27s+New" class="external-link"><img src="/jiracloud/attachments/39988013/39988121.png" class="confluence-thumbnail" height="150" /></a>

**<a href="https://confluence.atlassian.com/display/Cloud/What%27s+New" class="external-link">What's new page</a> **
*(Atlassian Cloud documentation)*

This is the changelog for all Cloud applications, including changes that affect JIRA Cloud developers, e.g. the addition of new JIRA Software modules.

  [<img src="/jiracloud/attachments/39988013/39988120.png" class="confluence-thumbnail" height="150" />]: https://developer.atlassian.com/blog/categories/jira/

