---
title: Integrate with the JIRA UI Using Atlassian Connect 39990073
aliases:
    - /jiracloud/integrate-with-the-jira-ui-using-atlassian-connect-39990073.html
---
# JIRA Cloud : Integrate with the JIRA UI using Atlassian Connect

The true power of Atlassian Connect is the ability to build rich integrations into the JIRA UI that feel like they are **part** of JIRA. This allows Atlassian Connect add-ons to be much more than just an integration – they can become full-fledged extensions of the product that unlock entirely new use cases.

Modules like pages, web panels, and dialogs allow you to display web content inside of JIRA via an iframe, while web sections and web items allow you to open and navigate to this content. To make these interactions feel native, there are three key features in Atlassian Connect that you can use in your add-on.

## JavaScript API

Atlassian Connect provides a [JavaScript API] to allow your add-on iframe to interact with the content around it on the page in JIRA. The Atlassian Connect JavaScript API is provided by JIRA – simply include the `all.js` file in the scripts in each of your pages, there's no need to serve it yourself.

The JS API includes methods for:

-   [Messaging between multiple iframes]
-   [Automatic resizing for your content]
-   [Advanced JIRA UI actions like opening date pickers and the create issue dialog]
-   [Making XHR requests to JIRA REST resources without requiring CORS]
-   and much more!

<img src="/jiracloud/attachments/39990073/39990102.png" class="confluence-thumbnail" width="40" />

[Full JavaScript API documentation][JavaScript API]

## Conditions

More often than not, you will not want to load your modules on every page, for every user, every time. You can use **conditions** in your add-on descriptor to determine whether JIRA should load your add-on's UI modules. For example, you may only want to load a certain panel if the current user is an administrator, or if a certain property on an issue has been configured. Setting up conditions is simple: you define which conditions are applicable when you declare each module in your add-on descriptor, and JIRA will evaluate them in-process when the page is loaded.

-   [Learn more about conditions]
-   [Ful list of conditions]

### Conditions example

``` brush:
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "user_is_logged_in"
                    }
                ]
            }
        ]
    }
}
```

## Context parameters

Frequently, the content you show in a panel will vary depending on the context around it (like the current issue, board, user, or project). When JIRA makes a request to your add-on to load an iframe, it can send additional context parameters that can tell your add-on how to respond to the request and which content to load. There are some standard context parameters (like user language and timezone) as well as JIRA specific parameters. Follow the links on the right to learn more.

-   [Learn more about context parameters]
-   [List of general context parameters]

### JIRA-specific context parameters

JIRA supports these context variables.

-   `issue.id`, `issue.key`, `issuetype.id`
-   `project.id`, `project.key`
-   `version.id`
-   `component.id`
-   `profileUser.name`, `profileUser.key` (available for user profile pages)
-   `dashboardItem.id`, `dashboardItem.key`, `dashboardItem.viewType`, `dashboard.id` (available for dashboard items)

JIRA issue pages only expose issue and project data. Similarly, version and component information is available only in project administration pages.

#### []JIRA Software and JIRA Service Desk context parameters

JIRA Software and JIRA Service supports additional context variables, specific to each application. See the pages below for more information:

-   [JIRA Software context parameters]
-   [JIRA Service Desk context parameters]

 

<a href="https://developer.atlassian.com/jiracloud/authentication-and-authorization-39990700.html" class="aui-button aui-button-primary">Next: Authentication and authorization</a>

  [JavaScript API]: https://developer.atlassian.com/static/connect/docs/latest/concepts/javascript-api.html
  [Messaging between multiple iframes]: https://developer.atlassian.com/static/connect/docs/latest/javascript/module-Events.html
  [Automatic resizing for your content]: https://developer.atlassian.com/static/connect/docs/latest/javascript/module-AP.html
  [Advanced JIRA UI actions like opening date pickers and the create issue dialog]: https://developer.atlassian.com/static/connect/docs/latest/javascript/module-jira.html
  [Making XHR requests to JIRA REST resources without requiring CORS]: https://developer.atlassian.com/static/connect/docs/latest/javascript/module-request.html
  [Learn more about conditions]: https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html#jira-conditions
  [Ful list of conditions]: https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html#product-specific-conditions
  [Learn more about context parameters]: https://developer.atlassian.com/static/connect/docs/latest/concepts/context-parameters.html
  [List of general context parameters]: https://developer.atlassian.com/static/connect/docs/latest/concepts/context-parameters.html#standard-parameters
  []: 
  [JIRA Software context parameters]: /jiracloud/jira-software-context-parameters-39990789.html
  [JIRA Service Desk context parameters]: /jiracloud/jira-service-desk-context-parameters-39988330.html

