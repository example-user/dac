---
title: JIRA Platform Modules Project Configuration 39988366
aliases:
    - /jiracloud/jira-platform-modules-project-configuration-39988366.html
---
# JIRA Cloud : JIRA platform modules - Project configuration

This pages lists the JIRA platform modules for the JIRA administration console. These can be used to inject menu sections and items in the Add-ons menu.

**On this page:**

-   [Project configuration menu]
-   [Project summary area]

## Project configuration menu

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Adds sections and items to the menu of the project configuration screen</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>atl.jira.proj.config</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-654511220" class="expand-container">
<div id="expander-control-654511220" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39988366/39988363.png" width="850" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-667562793" class="expand-container">
<div id="expander-control-667562793" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code>...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example_menu_section&quot;,
            &quot;location&quot;: &quot;atl.jira.proj.config&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example section link&quot;,</code></pre>
<pre><code>            &quot;location&quot;: &quot;example_menu_section&quot;,</code></pre>
<pre><code>            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            }</code></pre>
<pre><code>        }
    ],
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Project summary area

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Adds panels to the project summary area</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webPanel</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>webpanels.admin.summary.left-panels</code> and <code>webpanels.admin.summary.right-panels</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-1487845249" class="expand-container">
<div id="expander-control-1487845249" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39988366/39988364.png" width="850" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-42058403" class="expand-container">
<div id="expander-control-42058403" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code>...
&quot;modules&quot;: {
    &quot;webPanels&quot;: [
        {
            &quot;key&quot;: &quot;example_panel&quot;,
            &quot;location&quot;: &quot;webpanels.admin.summary.left-panels&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example panel&quot;
            }
        }
    ],    </code></pre>
<pre><code>}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

  [Project configuration menu]: #JIRAplatformmodules-Projectconfiguration-Projectconfigurationmenu
  [Project summary area]: #JIRAplatformmodules-Projectconfiguration-Projectsummaryarea

