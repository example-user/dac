---
title: Guide Using the Issue Field Module 40501522
aliases:
    - /jiracloud/guide-using-the-issue-field-module-40501522.html
---
# JIRA Cloud : Guide - Using the Issue Field module

The issue field module is used to associate remote entities with issues. The field will be searchable in both the <a href="https://confluence.atlassian.com/display/JIRASOFTWARECLOUD/Basic+searching" class="external-link">basic search</a> and <a href="https://confluence.atlassian.com/display/JIRASOFTWARECLOUD/Advanced+searching" class="external-link">advanced search</a>; and can be updated via the REST API. For example, JIRA Software has the "sprint" entity that is added by the JIRA Agile add-on. The "sprint" field for this entity can be searched for using JQL, viewed and updated on issues, and updated via the REST API, even though the sprint data itself is stored in the JIRA Agile add-on.

 This guide will show you how to use the JIRA [issue field module]. We'll take you on a guided tour of an example Connect add-on, **jira-issue-field-demo**, that uses the issue field module and explain the key concepts.  You won't be building an add-on yourself in this guide, but you can browse, download, and even run the source code for the example add-on:

Show me the code!

The code for the example add-on described in this guide is here: <a href="https://bitbucket.org/atlassianlabs/jira-issue-field-demo" class="uri" class="external-link">https://bitbucket.org/atlassianlabs/jira-issue-field-demo</a>. Feel free to check it out first, if you prefer to figure things out from the source.

Otherwise, the guide below will explore the key parts of the add-on and explain what's going on.

The **jira-issue-field-demo** add-on that we will be looking at, uses the issue field module to associate a remote "Teams" entity with JIRA issues. Watch the video below to see the add-on in action:

**On this page:**

-   [Before you begin]
-   [The "Teams" issue field — Defining an issue field]
-   [The "Teams" page — Adding and removing teams]
-   [The "Teams in project" page — Enabling/disabling an option for a specific project]
-   [The issue screen — Editing the "Team" field]
-   [JQL search — Searching for an issue by "team" and "team category"]
-   [Next steps]

## Before you begin

-   This tutorial assumes that you are familiar with <a href="https://connect.atlassian.com/" class="external-link">Atlassian Connect</a> and [JIRA Cloud development]. If you haven't created a Connect add-on for JIRA  before, we recommend that you read our [Getting started guide] first.

## The "Teams" issue field — Defining an issue field

As you would have seen in the video, the **jira-issue-field-demo** adds a "Teams" entity to JIRA. Despite the fact that the "Teams" entity is provided by the add-on, the "Team" field behaves like any other issue field in JIRA — the "Team" field options can be changed as needed, the field itself can be edited in an issue, and issues can be searched by the field and field properties. The key to this is the **jiraIssueFields** issue field module, which relates the "Teams" entity to issues in JIRA.

In this section, we'll show you how an issue field is defined.

#### Concepts: Defining a jiraIssueFields module

An issue field is implemented in an add-on by declaring it in the add-on descriptor:

``` false;
"jiraIssueFields": [
    {
        "key" : "team-issue-field",
        "name" : {
            "value" : "Team"
        },
        "description" : {
            "value" : "Team tied to issue"
        },
        "type": "single_select",
        "extractions": [{
            "path": "category",
            "type": "string",
            "name": "category"
        }]
    }
]
```

Let's have a look at the properties of this module:

-   The **key** is the unique identifier for the module. You can define this to be any value you want, provided that it uses alphanumeric characters and dashes. However, it's important to note that this key will be used when accessing the field via the REST API.
-   The **name** and **description** can also be set to any value that you want. These values will be shown to the user when the field is displayed on the view, create, and edit issues screens in the JIRA UI.
-   The **type** can be 'text' or 'single\_select'.
-   *(optional)* The **extractions** are used for single-select fields only. They allow issues to be searched by the property of an option, using JQL. We'll explain this later in this guide.

You can see this module definition in the <a href="https://bitbucket.org/atlassianlabs/jira-issue-field-demo/src/e12c6e05b05ee2f8228b53763ce6db9e2785b9e9/views/atlassian-connect.json?at=master" class="external-link">atlassian.json file</a> of the **jira-issue-field-demo** add-on.

## The "Teams" page — Adding and removing teams

One of the first screens you'll notice in the video is the 'Teams' page, where you add and remove teams. This is a [generalPages module] that renders a basic HTML page. You can see the definition for this in the **<a href="https://bitbucket.org/atlassianlabs/jira-issue-field-demo/src/e12c6e05b05ee2f8228b53763ce6db9e2785b9e9/atlassian-connect.json?at=master" class="external-link">atlassian.json</a>** file and the HTML in the **<a href="https://bitbucket.org/atlassianlabs/jira-issue-field-demo/src/e12c6e05b05ee2f8228b53763ce6db9e2785b9e9/views/teams.hbs?at=master" class="external-link">teams.hbs</a>** file.

<img src="/jiracloud/attachments/40501522/40501512.png" width="500" />

When you add/remove teams on this page, you are actually adding and removing teams from a directory of teams in the add-on's database. Behind the scenes is where it gets interesting. The directory of teams is stored in the add-on database, but this list of teams is synchronised with equivalent data for the issue field in JIRA (when you edit or remove a team). 

This is all done in the <a href="https://bitbucket.org/atlassianlabs/jira-issue-field-demo/src/e12c6e05b05ee2f8228b53763ce6db9e2785b9e9/routes/sync.js?at=master" class="external-link"><strong>sync.js</strong> file</a> of the **jira-issue-field-demo** add-on. If you browse the file, you'll see that it pulls the team data from the add-on database, then updates the issue field data in JIRA via the REST API.

#### Concept: Updating issue field data via the REST API

JIRA provides a CRUD REST API to administer options for single select issue fields, so that you can keep your add-on data in sync with JIRA. This is done via the **option** resource:

`api/2/field/{fieldKey}/option`

This resource has methods to create, retrieve, update, and delete options. Let's take a quick look at the create method. Once you understand how the create methods works, the other methods will be easy to use. 

The **Create option** method requires an option payload. The body of the request looks like this:

``` false;
{
    "value" : "Dev Team 1",
    "scope" : {
        "projects" : ["1000","1001"]
    },
    "properties" : {
        "category" : "Developers"
    }
}
```

The fields in this option payload are:

-   The **value** is the value shown to users in the UI.
-   *(optional)* The **scope** defines the projects that the option will be shown in. This will be explained in the next section.
-   *(optional)* The **properties** are any additional properties that define the option. 

That's it! Just call the method like any other JIRA REST method. 

## The "Teams in project" page — Enabling/disabling an option for a specific project

The 'Teams in Project' screen is where you enable and disable teams for a specific project. This is a [jiraProjectAdminTabPanels module] that renders a basic HTML page. You can see the definition for this in the **<a href="https://bitbucket.org/atlassianlabs/jira-issue-field-demo/src/e12c6e05b05ee2f8228b53763ce6db9e2785b9e9/atlassian-connect.json?at=master" class="external-link">atlassian.json</a>** file and the HTML in the **<a href="https://bitbucket.org/atlassianlabs/jira-issue-field-demo/src/e12c6e05b05ee2f8228b53763ce6db9e2785b9e9/views/teams-project.hbs?at=master" class="external-link">teams-project.hbs</a>** file.

Enabling an option means that it is available to be selected in the 'Team' dropdown on issues in the project, and vice versa for disabling an option. What this is actually doing in the code is changing the scope of the option. Enabling an option adds or updates an option with the appropriate scope, via the REST API. Disabling an option does the opposite. This all done in the <a href="https://bitbucket.org/atlassianlabs/jira-issue-field-demo/src/e12c6e05b05ee2f8228b53763ce6db9e2785b9e9/routes/teams-project.js?at=master" class="external-link"><strong>teams-project.js</strong> file</a> of the add-on.

<img src="/jiracloud/attachments/40501522/40501513.png" width="500" />

#### Concept: Changing the scope for options of a single-select issue field

In the previous section, you would have seen that the payload for an option has a **scope** property:

``` false;
"scope" : {
   "projects" : ["1000","1001"]
```

Changing the **scope** is as simple as adding or deleting the relevant projectIds from the property. If you want an option to be shown in all projects, then you can omit the scope property altogether.

## The issue screen — Editing the "Team" field

The video demonstrates how the "Team" issue field can be seen on issue screens. It can also be edited via the create issue dialog, the issue edit dialog, and the quick edit on the view issue screen. There is no additional coding required for this. 

However, be aware that the *issue field is not added to any screens* when the add-on is installed. If a JIRA administrator wants to show the field on certain screens *(i.e. create, edit, view)* for any projects, then they will need to manually configure the appropriate project schemes. To work around this, you can configure your add-on to display a page when it has finished installing, that tells the JIRA administrator that additional configuration is required to show the fields.

<img src="/jiracloud/attachments/40501522/40501515.png" width="400" />

## JQL search — Searching for an issue by "team" and "team category"

The video demonstrates two different JQL searches related to issue fields: searching by the issue field name (i.e. 'Team') and searching by a property of the issue field (i.e. 'Team.category').

<img src="/jiracloud/attachments/40501522/40501517.png" width="400" />

#### Concepts: Extracting option properties for searching

You don't need to do any additional coding to enable JQL searching using an issue field. It's automatically enabled when you implement the issue field. However, option properties are not automatically made available as search parameters. You need to "extract" the option property to make it searchable. This is done by defining the property as an **extraction** in the **jiraIssueFields** module definition (in the **atlassian.json** add-on descriptor):

``` false;
"extractions": [{
            "path": "category",
            "type": "string",
            "name": "category"
}]
```

The example extraction above looks for the "category" property of an option and makes it available for search as the "category" search parameter. It also validates the parameter as a string, when a search is run that uses it. In the **jira-issue-field-demo** add-on, you can see the extraction defined for the 'category' field in the <a href="https://bitbucket.org/atlassianlabs/jira-issue-field-demo/src/e12c6e05b05ee2f8228b53763ce6db9e2785b9e9/atlassian-connect.json?at=master" class="external-link"><strong>atlassian.json</strong> file</a>.

<img src="/jiracloud/https://extranet.atlassian.com/s/en_GB/6441/4e24aeea81c3997bbbbf19f992ed6274cb23026c/_/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick confluence-external-resource" /> **Congratulations!** You now know how to implement an issue field in a Connect add-on. Have a chocolate!

## Next steps

**Stuck?** Get help here: [Support].

Want to learn more about JIRA Cloud development? Check out our other tutorials for the JIRA platform:

-   [JIRA platform tutorials]

You can also find out about other JIRA platform modules here:

-   [JIRA platform modules]

  [issue field module]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/issue-field.html
  [Before you begin]: #Guide-UsingtheIssueFieldmodule-Beforeyoubegin
  [The "Teams" issue field — Defining an issue field]: #Guide-UsingtheIssueFieldmodule-The%22Teams%22issuefield—Defininganissuefield
  [The "Teams" page — Adding and removing teams]: #Guide-UsingtheIssueFieldmodule-The%22Teams%22page—Addingandremovingteams
  [The "Teams in project" page — Enabling/disabling an option for a specific project]: #Guide-UsingtheIssueFieldmodule-The%22Teamsinproject%22page—Enabling/disablinganoptionforaspecificproject
  [The issue screen — Editing the "Team" field]: #Guide-UsingtheIssueFieldmodule-Theissuescreen—Editingthe%22Team%22field
  [JQL search — Searching for an issue by "team" and "team category"]: #Guide-UsingtheIssueFieldmodule-JQLsearch—Searchingforanissueby%22team%22and%22teamcategory%22
  [Next steps]: #Guide-UsingtheIssueFieldmodule-Nextsteps
  [JIRA Cloud development]: https://developer.atlassian.com/display/jiracloud
  [Getting started guide]: /jiracloud/getting-started-39988011.html
  [generalPages module]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/page.html
  [jiraProjectAdminTabPanels module]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/project-admin-tab-panel.html
  [Support]: /jiracloud/support-39988134.html
  [JIRA platform tutorials]: /jiracloud/jira-platform-tutorials-39987044.html
  [JIRA platform modules]: /jiracloud/jira-platform-modules-39987040.html

