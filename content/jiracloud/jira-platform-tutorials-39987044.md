---
title: JIRA Platform Tutorials 39987044
aliases:
    - /jiracloud/jira-platform-tutorials-39987044.html
---
# JIRA Cloud : JIRA platform tutorials

The following pages will help you learn how to develop for the JIRA platform. Guides provide examples and best practices on JIRA concepts or processes. Tutorials provide a more hands-on approach to learning, by getting you to build something with step-by-step procedures.

Check them out below:

-   [Guide - Using the Issue Field module] — This guide will show you how to use the JIRA issue field module https://developer.atlassian.com/static/connect/docs/latest/modules/jira/issue-field.html. We'll take you on a guided tour of an example Connect add-on, jira-issue-field-demo, that uses the issue field module and explain the key concepts. 

  [Guide - Using the Issue Field module]: /jiracloud/guide-using-the-issue-field-module-40501522.html

