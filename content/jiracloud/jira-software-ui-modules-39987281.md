---
title: JIRA Software UI Modules 39987281
aliases:
    - /jiracloud/jira-software-ui-modules-39987281.html
---
# JIRA Cloud : JIRA Software UI modules

JIRA modules allow add-ons to extend the functionality of the JIRA platform or a JIRA application. JIRA modules are commonly used to extend the user interface by adding links, panels, etc. However, some JIRA platform modules can also be used to extend other parts of JIRA, like permissions and workflows.

The JIRA platform and JIRA Service Desk also have their own modules. For more information, see the following pages:

-   [JIRA platform modules]
-   [JIRA Service Desk UI modules]

## Using JIRA Software UI modules

You can use a JIRA Software UI module by declaring it in your add-on descriptor (under `modules`), with the appropriate properties. For example, the following code adds the `webPanel` module at the `jira.agile.board.configuration` location in your add-on, which creates a new board configuration page in JIRA Software.

**atlassian-connect.json**

``` 22;
...
"modules": {       
    "webPanels": [
        {
            "key": "my-configuration-page",
            "url": "configuration?id={board.id}&type={board.type}",
            "location": "jira.agile.board.configuration",
            "name": {
                "value": "My board configuration page"
            },
            "weight": 1
        }
    ]
}
...
```

## JIRA Software UI modules

-   [JIRA Software modules - Boards]

  [JIRA platform modules]: /jiracloud/jira-platform-modules-39987040.html
  [JIRA Service Desk UI modules]: /jiracloud/jira-service-desk-ui-modules-39988267.html
  [JIRA Software modules - Boards]: /jiracloud/jira-software-modules-boards-39990330.html

