---
title: JIRA Platform Modules Administration Console 39988333
aliases:
    - /jiracloud/jira-platform-modules-administration-console-39988333.html
---
# JIRA Cloud : JIRA platform modules - Administration console

This pages lists the JIRA platform modules for the JIRA administration console. These can be used to inject menu sections and items in the Add-ons menu.

**On this page:**

-   [Add-ons]

## Add-ons

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Adds sections and items to the 'Add-ons' menu of the JIRA administration area.</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>admin_plugins_menu</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-530588221" class="expand-container">
<div id="expander-control-530588221" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39988333/39988345.png" width="700" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-40086089" class="expand-container">
<div id="expander-control-40086089" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code>...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example_menu_section&quot;,
            &quot;location&quot;: &quot;admin_plugins_menu&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example section link&quot;,</code></pre>
<pre><code>            &quot;location&quot;: &quot;example_menu_section&quot;,</code></pre>
<pre><code>            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            }</code></pre>
<pre><code>        }
    ],
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

  [Add-ons]: #JIRAplatformmodules-Administrationconsole-Add-ons

