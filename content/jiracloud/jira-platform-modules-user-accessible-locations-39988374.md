---
title: JIRA Platform Modules User Accessible Locations 39988374
aliases:
    - /jiracloud/jira-platform-modules-user-accessible-locations-39988374.html
---
# JIRA Cloud : JIRA platform modules - User-accessible locations

This pages lists the JIRA platform modules for various locations in the JIRA UI that are accessible by non-admin users.

**On this page:**

-   [Top navigation bar location]
-   [User name drop-down location]
-   [User profile page dropdown location]
-   [Hover profile links location]
-   [Dialog box hint location]

## Top navigation bar location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Defines web sections and items in JIRA's top navigation bar, which are accessible from all JIRA areas (except JIRA's administration area/mode).</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>system.top.navigation.bar</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-1506023295" class="expand-container">
<div id="expander-control-1506023295" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39988374/40515906.png" width="727" height="40" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-451812551" class="expand-container">
<div id="expander-control-451812551" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code>...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example_menu_section&quot;,
            &quot;location&quot;: &quot;system.top.navigation.bar&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example section link&quot;,</code></pre>
<pre><code>            &quot;location&quot;: &quot;example_menu_section&quot;,</code></pre>
<pre><code>            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            }</code></pre>
<pre><code>        }
    ],
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## User name drop-down location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Defines web sections and items in JIRA's user name drop-down menu, which is accessible from all JIRA screens.</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>system.user.options</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-30615742" class="expand-container">
<div id="expander-control-30615742" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39988374/40515909.png" height="250" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-1721031951" class="expand-container">
<div id="expander-control-1721031951" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code>...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example_menu_section&quot;,
            &quot;location&quot;: &quot;system.user.options&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example section link&quot;,</code></pre>
<pre><code>            &quot;location&quot;: &quot;example_menu_section&quot;,</code></pre>
<pre><code>            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            }</code></pre>
<pre><code>        }
    ],
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## User profile page dropdown location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Defines web items of the more (<img src="/jiracloud/attachments/39988374/40515912.png" class="confluence-thumbnail" width="20" />) dropdown menu on a JIRA user's user profile page. This location has only one section (<code>operations</code>) to which custom web items can be added.</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>system.user.profile.links</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-998506742" class="expand-container">
<div id="expander-control-998506742" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39988374/40515911.png" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-1808649100" class="expand-container">
<div id="expander-control-1808649100" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code>...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example_menu_section&quot;,
            &quot;location&quot;: &quot;system.user.profile.links/operations&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example section link&quot;,</code></pre>
<pre><code>            &quot;location&quot;: &quot;example_menu_section&quot;,</code></pre>
<pre><code>            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            }</code></pre>
<pre><code>        }
    ],
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Hover profile links location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Defines web items in JIRA's hover profile feature, which is accessible when a user hovers their mouse pointer over a JIRA user's name throughout JIRA's user interface</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>system.user.hover.links</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-2010993101" class="expand-container">
<div id="expander-control-2010993101" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39988374/40515913.png" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-953120737" class="expand-container">
<div id="expander-control-953120737" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code>...</code></pre>
<pre><code>&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example_menu_section&quot;,
            &quot;location&quot;: &quot;system.user.hover.links&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example section link&quot;,</code></pre>
<pre><code>            &quot;location&quot;: &quot;example_menu_section&quot;,</code></pre>
<pre><code>            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            }</code></pre>
<pre><code>        }
    ],
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Dialog box hint location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Defines web items that allow you to add hints on JIRA's dialog boxes. You can add hints to most JIRA dialog boxes.</p>
<p>To add your own web item to JIRA's dialog box hints location for a specific dialog box, your web item must include a <code>section</code> attribute with the value<code>&quot;jira.hints/LOCATION_CONTEXT&quot;.</code>The <code style="line-height: 1.42857;">LOCATION_CONTEXT</code> is a predefined 'context' in JIRA that determines on which dialog box your hints will appear:</p>
<ul>
<li><code>TRANSITION</code> — Hints on a 'transition issue' dialog box.</li>
<li><code>ASSIGN</code> — Hints on the 'Assign' dialog box.</li>
<li><code>LABELS</code> — Hints on the 'Labels' dialog box.</li>
<li><code>COMMENT</code> — Hints on 'Comment' dialog boxes.</li>
<li><code>CLONE</code> — Hints on 'Clone Issue' dialog boxes.</li>
<li><code>DELETE_FILTER</code> — Hints on 'Deletre Filter' dialog boxes.</li>
<li><code>ATTACH</code> — Hints on 'Attach Files' dialog boxes, not the 'Attach Screenshot' dialog.</li>
<li><code>DELETE_ISSUE</code> — Hints on 'Delete issue' dialog boxes.</li>
<li><code>LINK</code> — Hints on 'Link issue' dialog boxes.</li>
<li><code>LOG_WORK</code> — Hints on 'Log work' dialog boxes.</li>
</ul></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>jira.hints</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-607638928" class="expand-container">
<div id="expander-control-607638928" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39988374/40515916.png" width="600" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-677763988" class="expand-container">
<div id="expander-control-677763988" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code>...</code></pre>
<pre><code>&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example_menu_section&quot;,
            &quot;location&quot;: &quot;jira.hints/ASSIGN&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example section link&quot;,</code></pre>
<pre><code>            &quot;location&quot;: &quot;example_menu_section&quot;,</code></pre>
<pre><code>            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            }</code></pre>
<pre><code>        }
    ],
}
...</code></pre>
<pre><code> </code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

  [Top navigation bar location]: #JIRAplatformmodules-User-accessiblelocations-Topnavigationbarlocation
  [User name drop-down location]: #JIRAplatformmodules-User-accessiblelocations-Usernamedrop-downlocation
  [User profile page dropdown location]: #JIRAplatformmodules-User-accessiblelocations-Userprofilepagedropdownlocation
  [Hover profile links location]: #JIRAplatformmodules-User-accessiblelocations-Hoverprofilelinkslocation
  [Dialog box hint location]: #JIRAplatformmodules-User-accessiblelocations-Dialogboxhintlocation
  []: /jiracloud/attachments/39988374/40515911.png
  [1]: /jiracloud/attachments/39988374/40515913.png

