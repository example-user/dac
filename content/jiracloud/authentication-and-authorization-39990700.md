---
title: Authentication and Authorization 39990700
aliases:
    - /jiracloud/authentication-and-authorization-39990700.html
---
# JIRA Cloud : Authentication and authorization

There are two levels to secure your Atlassian Connect add-on or JIRA integration: authentication and authorization. Authentication tells JIRA Cloud the identity of your add-on, authorization determines what actions it can take within JIRA.

## Authentication

Authentication tells JIRA Cloud the identity of the add-on and is the basis of all other security. There are two types of authentication for JIRA Cloud.

### Authenticating REST API requests directly

The JIRA platform, JIRA Software, and JIRA Service Desk REST APIs can authenticate clients directly through one of three different methods:

-   [Basic authentication], using a predefined set of user credentials
-   [Cookie-based authentication], which uses basic authentication to obtain a session cookie, then uses the cookie for additional requests
-   [OAuth token-based (1.0) authentication], which uses OAuth request tokens generated from JIRA Cloud

### Authentication for Atlassian Connect add-ons for JIRA Cloud

Atlassian Connect uses a technology called <a href="http://tools.ietf.org/html/draft-ietf-oauth-json-web-token%E2%80%8E" class="external-link">JWT (JSON Web Token)</a> to authenticate add-ons. Basically a security context is exchanged when the add-on is installed, and this context is used to create and validate JWT tokens, embedded in API calls. The use of JWT tokens guarantees that:

-   JIRA Cloud can verify it is talking to the add-on, and vice versa (authenticity).
-   None of the query parameters of the HTTP request, nor the path (excluding the context path), nor the HTTP method, were altered in transit (integrity).

If you are using one of Atlassian's supported Atlassian Connect libraries, JWT handling will already be in place.

Read more details in our [authentication documentation].

## Authorization

If you're calling a JIRA Cloud REST API directly using basic auth, cookie-based auth, or OAuth, the actions your client can perform are based on the permissions of the user credentials you are using. For example, if you are using basic authentication, your user must have Edit Issue permission on an issue in order to make a PUT request to the `/issue` resource.

For Atlassian Connect add-ons, there are two levels of authorization. Your add-on uses **scopes** to declare the maximum set of actions it can take upfront, and then more granular restrictions are available to JIRA administrators via **add-on users**.

### Static Authorization: Scopes

An add-on must declare the maximum set of actions that it may perform: read, write, etc. This security level is enforced by Atlassian Connect and cannot be bypassed by add-on implementations. Therefore administrators can be assured that the add-ons with READ permission can only read data and not modify it, and that add-ons with WRITE permission cannot delete data, etc.

You declare your add-on's required scope in your add-on descriptor, and JIRA Cloud will present the permissions your add-on has requested during installation.

Read more about scopes in our [scopes documentation].

#### JIRA REST API methods and scopes

Each JIRA Cloud REST API resource is individually whitelisted for Atlassian Connect add-ons, and different methods are available depending on the scope you have requested. Please refer to the scope overviews in the Atlassian Connect documentation for each API:

-   [JIRA platform REST API scopes]
-   [JIRA Software REST API scopes]
-   [JIRA Service Desk REST API scopes]

### Run-time Authorization: Add-on Users

Some administrators have some content that they wish to protect from add-ons. For example, some companies store sensitive details such as payroll and future business plans in JIRA Cloud, and wish to be assured that it is accessible to few add-ons or no add-ons at all.

This protected data varies across instances, as does its protections. Atlassian and add-on vendors have no way of knowing in advance what individual administrators will decide must be protected from add-ons nor what the protections should be (for example, perhaps one add-on is allowed read access to one particular super-secret-project while another is allowed write access).

We enable administrators to arbitrarily limit add-on access to user data at run-time by assigning every add-on its own user. Administrators can then apply permissions to these add-on users in very similar ways to how they permission normal users. Every incoming server-to-server request from a Connect add-on is assigned the add-on's user and authorization proceeds as normal from that point onwards, with the add-on user's permissions limiting what API features the incoming requests may target. Add-ons are not told which user they are assigned and do not need to specify it: it is automatically mapped from the identity of the add-on itself.

### In-browser requests

Atlassian Connect add-ons can also make requests to REST APIs from within the user's browser after the Atlassian Connect iframe has loaded using the [`AP.request()` method]. In contrast to server-to-server requests, in-browser requests are evaluated in the context of the current user. They are still subject to your add-on's declared scopes, but JIRA Cloud will treat the request like it was made by the current user on the page.

### Combining Static and Run-time Authorization

The set of actions that an add-on is capable of performing is the intersection of the static scopes and the permissions of the user assigned to the request. It is entirely possible that any request may be rejected because the assigned user lacks the necessary permission, so the add-on should always defensively detect <a href="http://en.wikipedia.org/wiki/HTTP_403" class="external-link">HTTP 403 forbidden</a> responses from the product and, if possible, display an appropriate message to the user.

<a href="https://developer.atlassian.com/display/jiracloud/Getting+started" class="aui-button aui-button-primary">Next: Build your first add-on for JIRA</a>

  [Basic authentication]: /jiracloud/jira-rest-api-basic-authentication-39991466.html
  [Cookie-based authentication]: /jiracloud/jira-rest-api-cookie-based-authentication-39991470.html
  [OAuth token-based (1.0) authentication]: /jiracloud/jira-rest-api-oauth-authentication-39991476.html
  [authentication documentation]: https://developer.atlassian.com/static/connect/docs/latest/concepts/authentication.html
  [scopes documentation]: https://developer.atlassian.com/static/connect/docs/latest/scopes/scopes.html
  [JIRA platform REST API scopes]: https://developer.atlassian.com/static/connect/docs/latest/scopes/jira-rest-scopes.html
  [JIRA Software REST API scopes]: https://developer.atlassian.com/static/connect/docs/latest/scopes/jira-software-rest-scopes.html
  [JIRA Service Desk REST API scopes]: https://developer.atlassian.com/static/connect/docs/latest/scopes/jira-service-desk-rest-scopes.html
  [`AP.request()` method]: https://developer.atlassian.com/static/connect/docs/latest/javascript/module-request.html

