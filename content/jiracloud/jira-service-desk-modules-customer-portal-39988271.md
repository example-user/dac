---
title: JIRA Service Desk Modules Customer Portal 39988271
aliases:
    - /jiracloud/jira-service-desk-modules-customer-portal-39988271.html
---
# JIRA Cloud : JIRA Service Desk modules - Customer portal

This pages lists the JIRA Service Desk modules for the customer portal. These can be used to inject panels and actions to various locations in JIRA Service Desk customer portal.

A panel is simply a section of HTML content on the page. An action is a clickable link or menu entry for user to perform specific operation. The target location of this link or menu entry is defined in the **atlassian-connect.json** descriptor file. The target location can be a URL hosted in the host application (JIRA Service Desk) or your Atlassian Connect add-on.

**On this page:**

-   [Header panel]
-   [Sub header panel]
-   [Footer panel]
-   [Request view panel]
-   [Profile page panel]
-   [Request view action]
-   [Profile page action]
-   [User menu action]

# Panels

## Header panel

Description
A panel rendered at the top of customer portal pages.
Module type
`serviceDeskPortalHeader`
Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988271/40513014.png" width="850" /> 

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

    ...
    "modules": {
        "serviceDeskPortalHeaders": [
            {
                "key": "sd-portal-header",
                "url": "/sd-portal-header"
            }
        ]
    }
    ...

Properties
**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description** : A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

**`url`**

-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`weight`**

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`conditions`**

-   **Type**: \[ [`Single Condition`], [`Composite Condition`], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

**`pages`**

-   **Type**: `[ string, ... ]`
-   **Allowed values**: help\_center, portal, create\_request, view\_request, my\_requests, approvals, profile
-   **Description**: Restrict the module to only be visible in specified customer portal page(s).

## Sub header panel

Description
A panel rendered underneath the title of customer portal pages.
Module type
` serviceDeskPortalSubHeader `
Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988271/40513016.png" width="850" /> 

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

    ...
    "modules": {
        "serviceDeskPortalSubHeaders": [
            {
                "key": "sd-portal-subheader",
                "url": "/sd-portal-subheader"
            }
        ]
    }
    ...

Properties
**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

**`url`**

-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`weight`**

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`conditions`**

-   **Type**: \[ [`Single Condition`], [`Composite Condition`], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

**`pages`**

-   **Type**: `[ string, ... ]`
-   **Allowed values**: ` help_center,  portal,  create_request,  view_request,  my_requests,  approvals, ` `profile`
-     **Description**: Restrict the module to only be visible in specified customer portal page(s).

## Footer panel

Description
A panel rendered at the bottom of customer portal pages.
Module type
`serviceDeskPortalFooter `
Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988271/40513017.png" width="850" height="481" /> 

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

    ...
    "modules": {
        "serviceDeskPortalFooters": [
            {
                "key": "sd-portal-footer",
                "url": "/sd-portal-footer"
            }
        ]
    }
    ...

Properties
**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

**`url`**

-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`weight`**

 

 

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`conditions`**

-   **Type**: \[ [`Single Condition`], [`Composite Condition`], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

**`pages`**

-   **Type**: `[ string, ... ]`
-   **Allowed values**: ` help_center,  portal,  create_request,  view_request,  my_requests,  approvals,  profile`
-     **Description**: Restrict the module to only be visible in specified customer portal page(s).

## Request view panel

Description
A panel that shows up in the bottom-right corner of request view page.
Module type
    serviceDeskPortalRequestViewPanel

Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988271/40513018.png" width="850" /> 

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

    ...
    "modules": {
        "serviceDeskPortalRequestViewPanels": [
            {
                "key": "sd-portal-request-view-content",
                "url": "/sd-portal-request-view-content"
            }
        ]
    }
    ...

Properties
**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

**`url`**

-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`weight`**

 

 

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`conditions`**

-   **Type**: \[ [`Single Condition`], [`Composite Condition`], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

## Profile page panel

Description
A panel that shows up in the profile page of the customer portal.

Module type
    serviceDeskPortalProfilePanel

Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988271/40513019.png" width="850" /> 

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

    ...
    "modules": {
        "serviceDeskPortalProfilePanels": [
            {
                "key": "sd-portal-profile-content",
                "url": "/sd-portal-profile-content"
            }
        ]
    }
    ...

Properties
**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

**`url`**

-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`weight`**

 

 

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`conditions`**

-   **Type**: \[ [`Single Condition`], [`Composite Condition`], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

# Actions

## Request view action

Description
A link that shows up in the request view of the customer portal. 

Module type
    serviceDeskPortalRequestViewAction

Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988271/40513020.png" width="850" /> 

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

    ...
    "modules": {
        "serviceDeskPortalRequestViewActions": [
            {
                "key": "sd-portal-request-view-action",
                "name": {
                    "value": "My request view action"
                },
                "url": "/sd-portal-request-view-content"
            }
        ]
    }
    ...

Properties
**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

**name**
-   **Type**: [`i18n Property`]
-   **Required**: yes
-   **Description**: A human readable name.

**`context`**
-   **Type**: `string`
-   **Default**: `addon`
-   **Allowed values**: ` page,  PAGE,  addon,  ADDON,  product,  PRODUCT`
-   **Description**: The context for the URL parameter, if the URL is specified as a relative (not absolute) URL. This context can be either `addon`, which renders the URL relative to the add-on's base URL, `page` which targets a page module by specifying the page's module key as the url or `product`, which renders the URL relative to the product's base URL.

**`url`**

-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`target`**
-   **Type**: [`Web Item Target` ]
-   **Description:** Defines the way the url is opened in the browser, such as in its own page or a modal dialog. If omitted, the url behaves as a regular hyperlink.

**`weight`**

 

 

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`conditions`**

-   **Type**: \[ [`Single Condition`], [`Composite Condition`], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

## Profile page action

Description
A link that shows up in the profile page. 

Module type
    serviceDeskPortalProfileAction

Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988271/40513021.png" width="850" /> 

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

    ...
    "modules": {
        "serviceDeskPortalProfileActions": [
            {
                "key": "sd-portal-profile-action",
                "name": {
                    "value": "My profile page action"
                },
                "url": "/sd-portal-profile-content"
            }
        ]
    }
    ...

Properties
**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`  name `
-   **Type**: [`i18n Property`]
-   **Required**: yes
-   **Description**: A human readable name.

**`context`**
-   **Type**: `string`
-   **Default**: `addon`
-   **Allowed values**: ` page,  PAGE,  addon,  ADDON,  product,  PRODUCT`
-   **Description**: The context for the URL parameter, if the URL is specified as a relative (not absolute) URL. This context can be either `addon`, which renders the URL relative to the add-on's base URL, `page` which targets a page module by specifying the page's module key as the url or `product`, which renders the URL relative to the product's base URL.

**`url`**

-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`target`**
-   **Type**: [`Web Item Target` ]
-   **Description:** Defines the way the url is opened in the browser, such as in its own page or a modal dialog. If omitted, the url behaves as a regular hyperlink.

**`weight`**

 

 

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`conditions`**

-   **Type**: \[ [`Single Condition`], [`Composite Condition`], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

## User menu action

Description
A menu entry that shows up in the user menu.

Module type
    serviceDeskPortalUserMenuAction

Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988271/40513022.png" width="850" /> 

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

    ...
    "modules": {
        "serviceDeskPortalUserMenuActions": [
            {
                "key": "sd-portal-user-menu-action",
                "name": {
                    "value": "My user menu action"
                },
                "url": "/sd-portal-user-menu-content"
            }
        ]
    }
    ...

Properties
**`key`**

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

` name`
-   **Type**: [`i18n Property`]
-   **Required**: yes
-   **Description**: A human readable name.

**`context`**
-   **Type**: `string`
-   **Default**: `addon`
-   **Allowed values**: `page, PAGE, addon, ADDON, product, PRODUCT`
-   **Description**: The context for the URL parameter, if the URL is specified as a relative (not absolute) URL. This context can be either `addon`, which renders the URL relative to the add-on's base URL, `page` which targets a page module by specifying the page's module key as the url or `product`, which renders the URL relative to the product's base URL.

**`url`**

-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`target`**
-   **Type**: [`Web Item Target` ]
-   **Description:** Defines the way the url is opened in the browser, such as in its own page or a modal dialog. If omitted, the url behaves as a regular hyperlink.

**`weight`**

 

 

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`conditions`**

-   **Type**: \[ [`Single Condition`], [`Composite Condition`], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

  [Header panel]: #JIRAServiceDeskmodules-Customerportal-Headerpanel
  [Sub header panel]: #JIRAServiceDeskmodules-Customerportal-Subheaderpanel
  [Footer panel]: #JIRAServiceDeskmodules-Customerportal-Footerpanel
  [Request view panel]: #JIRAServiceDeskmodules-Customerportal-Requestviewpanel
  [Profile page panel]: #JIRAServiceDeskmodules-Customerportal-Profilepagepanel
  [Request view action]: #JIRAServiceDeskmodules-Customerportal-Requestviewaction
  [Profile page action]: #JIRAServiceDeskmodules-Customerportal-Profilepageaction
  [User menu action]: #JIRAServiceDeskmodules-Customerportal-Usermenuaction
  [additional context]: /jiracloud/jira-service-desk-modules-customer-portal-39988271.html
  [`Single Condition`]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html
  [`Composite Condition`]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html
  [Conditions]: https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html
  [`i18n Property`]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/i18n-property.html
  [`Web Item Target` ]: /jiracloud/jira-service-desk-modules-web-item-target-40004227.html

