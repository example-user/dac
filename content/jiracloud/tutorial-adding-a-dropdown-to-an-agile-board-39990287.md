---
title: Tutorial Adding a Dropdown to an Agile Board 39990287
aliases:
    - /jiracloud/tutorial-adding-a-dropdown-to-an-agile-board-39990287.html
---
# JIRA Cloud : Tutorial - Adding a dropdown to an agile board

This tutorial will show you how to add a new dropdown to a board in JIRA Software, by using a JIRA Software module in your Connect add-on. This dropdown will appear in the top right corner of an Agile board, next to the **Board** dropdown.

To add this new dropdown, you will be using a JIRA Software module. To do this, you will create a new web-section at the location: `jira.agile.board.tools`.  You will then add a web-item or web-panel inside the web-section.

*Screenshot: Example web-section with a web-panel in jira.agile.board.tools.*

<img src="/jiracloud/attachments/39990287/39990286.png" width="435" />

About these Instructions

You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Sublime Text editor on Mac OS X. If you are using a different IDE and/or OS, you should use the equivalent operations for your specific environment.

This tutorial was last tested with JIRA 6.5-OD-07 and JIRA Agile 6.7.6. Note, the JIRA Software module used in this tutorial is only available in JIRA Agile 6.6.50 and later.

**
**

## Before you begin

-   If you haven't built a Connect add-on before, read the [Getting started] tutorial. You'll learn the fundamentals of Connect development and build a basic Connect add-on that you can use with this tutorial.

## Instructions

#### Step 1. Create a web-section and web-panel in your atlassian-connect.json

1.  Edit the **atlassian-connect.json** file in your add-on project.

2.  Add the following code for the `web-section` to the `modules` context:

    ``` brush:
    "webSections": [
          {
            "key": "board-links",
            "location": "jira.agile.board.tools",
            "weight": 10,
            "name": {
              "value": "My Extension"
            }
          }
        ],
    ```

3.  Add the following code for the `web-panel` to the `modules` context:

    ``` brush:
    "webPanels": [
          {
            "key": "my-web-panel",
            "url": "web-panel?id={board.id}&mode={board.screen}",
            "name": {
              "value": "My Web Panel"
            },
            "location": "board-links",
            "layout": {
              "width": "100px",
              "height": "100px"
            }
          }
        ]
    ```

#### Step 2. Add server side code to render the web panel

The server-side code required to render the web panel is shown below. This code is for <a href="https://bitbucket.org/atlassian/atlassian-connect-express" class="external-link">atlassian-connect-express</a>, the node.js framework (with Express), but you can use a different technology stack if you wish.

1.  Navigate to the **routes** directory in your add-on project and edit the **index.js** file.
2.  Add the following code to the file and save it:

    ``` brush:
    app.get('/dropdown', function(req,res){
        res.render("dropdown", {id : req.query['id'], mode : req.query['mode'] });
    });
    ```

    This adds the 'configuration' route to your app.

3.  Navigate to the **views** directory in your add-on project and create a new **dropdown.hbs** file. 

4.  Add the following code to the **dropdown.hbs** file and save it:

    ``` brush:
    <!DOCTYPE html>
    <html>
      <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="ap-local-base-url" content="http://localhost:2990">
        <link rel="stylesheet" type="text/css" href="http://localhost:2990/jira/atlassian-connect/all.css">
        <script src="http://localhost:2990/jira/atlassian-connect/all-debug.js" type="text/javascript"></script>
      </head>
      <body>
        <p id="custom-message">We are viewing board {{id}} in {{mode}} mode.</p>
      </body>
    </html>
    ```

#### Step 3. Check that it all works

1.  Start up and deploy your add-on (`npm start`) to your JIRA Cloud instance, if it's not running already. 
2.  Navigate to the board for any JIRA Software project. You should see a page like this:
    <img src="/jiracloud/attachments/39990287/39990285.png" width="850" />

 

<img src="/jiracloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" />   **Congratulations!** You've just added a dropdown to an Agile board in your Connect add-on.

  [Getting started]: /jiracloud/getting-started-39988011.html

