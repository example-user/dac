---
title: JIRA Cloud Platform REST API 39987036
aliases:
    - /jiracloud/jira-cloud-platform-rest-api-39987036.html
---
# JIRA Cloud : JIRA Cloud platform REST API

The JIRA REST APIs are used to interact with the JIRA Cloud applications remotely, for example, when building Connect add-ons or configuring webhooks. The JIRA Cloud platform provides a REST API for common features, like issues and workflows. Read the reference documentation below to get started.

<a href="https://docs.atlassian.com/jira/REST/cloud/" class="external-link"><br />
</a>

<a href="https://docs.atlassian.com/jira/REST/cloud/" class="external-link"><img src="/jiracloud/attachments/39987036/39987144.png" class="image-center confluence-thumbnail" height="150" /></a>

#### <a href="https://docs.atlassian.com/jira/REST/cloud/" class="external-link">JIRA Cloud platform REST API reference</a>

<sub>*If\\ you\\ haven't\\ used\\ the\\ JIRA\\ REST\\ APIs\\ before,\\ make\\ sure\\ you\\ read\\ the [Atlassian\\ REST\\ API\\ policy].*</sub>

 

## Other JIRA REST APIs

The JIRA Software and JIRA Service Desk applications also have REST APIs for their application-specific features, like sprints (JIRA Software) or customer requests (JIRA Service Desk).

-   [JIRA Software Cloud REST API]
-   [JIRA Service Desk Cloud REST API]

## Using the REST APIs

The best way to get started with the JIRA Cloud platform REST API is to jump into the reference documentation. However, here are a few other resources to help you along the way.

### Authentication guides

The REST APIs support basic authentication, cookie-based (session) authentication, and OAuth. The following pages will get you started on using each of these authentication types with the REST APIs:

-   [JIRA REST API Example - Basic Authentication]
-   [JIRA REST API Example - Cookie-based Authentication]
-   [JIRA REST API Example - OAuth authentication]

  [Atlassian\\ REST\\ API\\ policy]: https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy
  [JIRA Software Cloud REST API]: /jiracloud/jira-software-cloud-rest-api-39988028.html
  [JIRA Service Desk Cloud REST API]: /jiracloud/jira-service-desk-cloud-rest-api-39988033.html
  [JIRA REST API Example - Basic Authentication]: https://developer.atlassian.com/display/JIRADEV/JIRA+REST+API+Example+-+Basic+Authentication
  [JIRA REST API Example - Cookie-based Authentication]: https://developer.atlassian.com/display/JIRADEV/JIRA+REST+API+Example+-+Cookie-based+Authentication
  [JIRA REST API Example - OAuth authentication]: https://developer.atlassian.com/display/JIRADEV/JIRA+REST+API+Example+-+OAuth+authentication

