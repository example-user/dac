---
title: JIRA Platform Modules View Issue Page 39988380
aliases:
    - /jiracloud/jira-platform-modules-view-issue-page-39988380.html
---
# JIRA Cloud : JIRA platform modules - View issue page

This pages lists the JIRA platform modules for the view issue page.

**On this page:**

-   [Issue operations bar locations]
-   [Right-side of the 'View Issue' page location]

## Issue operations bar locations

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>The two <code>opsbar-operations</code> and <code>opsbar-transitions</code> locations together define web sections and items in the issue operations bar, which is visible on the 'View Issue' page.</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>opsbar-operations</code> and <code>opsbar-transitions</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-1543374594" class="expand-container">
<div id="expander-control-1543374594" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39988380/40515897.png" width="850" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-1683885429" class="expand-container">
<div id="expander-control-1683885429" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code> </code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Right-side of the 'View Issue' page location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Defines web panels for the right hand side of the 'View Issue' page, as shown in the screenshot below.</p></td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webPanel</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td> atl.jira.view.issue.right.context</td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-1597323753" class="expand-container">
<div id="expander-control-1597323753" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p> <img src="/jiracloud/attachments/39988380/40515899.png" width="850" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-309761984" class="expand-container">
<div id="expander-control-309761984" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code> </code></pre>
<pre><code>...
&quot;modules&quot;: {
    &quot;webPanels&quot;: [
        {
            &quot;key&quot;: &quot;example_panel&quot;,
            &quot;location&quot;: &quot;atl.jira.view.issue.right.context&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example panel&quot;
            }
        }
    ],    </code></pre>
<pre><code>}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

  [Issue operations bar locations]: #JIRAplatformmodules-Viewissuepage-Issueoperationsbarlocations
  [Right-side of the 'View Issue' page location]: #JIRAplatformmodules-Viewissuepage-Right-sideofthe'ViewIssue'pagelocation

