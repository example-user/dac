---
title: JIRA Software Cloud REST API 39988028
aliases:
    - /jiracloud/jira-software-cloud-rest-api-39988028.html
---
# JIRA Cloud : JIRA Software Cloud REST API

The JIRA REST APIs are used to interact with the JIRA Cloud applications remotely, for example, when building Connect add-ons or configuring webhooks. JIRA Software Cloud provides a REST API for application-specific features, like boards and sprints. Read the reference documentation below to get started.

 

#### <img src="/jiracloud/attachments/39988028/39988023.png" class="image-center confluence-thumbnail" height="150" />

#### <a href="https://docs.atlassian.com/jira-software/REST/cloud/" class="external-link">JIRA Software Cloud REST API</a>

<sub>*If\\ you\\ haven't\\ used\\ the\\ JIRA\\ REST\\ APIs\\ before,\\ make\\ sure\\ you\\ read\\ the [Atlassian\\ REST\\ API\\ policy].*</sub>

 

## Other JIRA REST APIs

The JIRA Cloud platform provides a REST API for common features, like issues and workflows. 

-   [JIRA Cloud platform REST API]

 JIRA Service Desk Cloud also has its own REST API for application-specific features, like queues and requests.

-   [JIRA Service Desk Cloud REST API]

## Using the REST APIs

The best way to get started with the REST APIs is to jump into the reference documentation above. However, here are a few other resources to help you along the way.

### Authentication guides

The REST APIs support basic authentication, cookie-based (session) authentication, and OAuth. The following pages will get you started on using each of these authentication types with the REST APIs:

-   [Authentication and authorization] overview
-   [JIRA REST API - Basic authentication]
-   [JIRA REST API - Cookie-based Authentication]
-   [JIRA REST API - OAuth authentication]

  [Atlassian\\ REST\\ API\\ policy]: https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy
  [JIRA Cloud platform REST API]: /jiracloud/jira-cloud-platform-rest-api-39987036.html
  [JIRA Service Desk Cloud REST API]: /jiracloud/jira-service-desk-cloud-rest-api-39988033.html
  [Authentication and authorization]: /jiracloud/authentication-and-authorization-39990700.html
  [JIRA REST API - Basic authentication]: /jiracloud/jira-rest-api-basic-authentication-39991466.html
  [JIRA REST API - Cookie-based Authentication]: /jiracloud/jira-rest-api-cookie-based-authentication-39991470.html
  [JIRA REST API - OAuth authentication]: /jiracloud/jira-rest-api-oauth-authentication-39991476.html

