---
title: JIRA Platform Modules 39987040
aliases:
    - /jiracloud/jira-platform-modules-39987040.html
---
# JIRA Cloud : JIRA platform modules

JIRA modules allow add-ons to extend the functionality of the JIRA platform or a JIRA application. JIRA modules are commonly used to extend the user interface by adding links, panels, etc. However, some JIRA platform modules can also be used to extend other parts of JIRA, like permissions and workflows. 

JIRA Software and JIRA Service Desk also have their own application-specific modules (UI-related only). For more information, see the following pages:

-   [JIRA Software UI modules]
-   [JIRA Service Desk UI modules]

## Using JIRA modules

You can use a JIRA module by declaring it in your add-on descriptor (under `modules`), with the appropriate properties. For example, the following code adds the `generalPages` module at the `system.top.navigation.bar` location to your add-on, which adds a link in the JIRA header.

**atlassian-connect.json**

``` 22;
...
"modules": {
          "generalPages": [
              {
                  "key": "activity",
                  "location": "system.top.navigation.bar",
                  "name": {
                      "value": "Activity"
                  }
              }
          ]
      }
...
```

## JIRA platform modules

There are two types of modules: basic iframes that allow you to display content in different locations in JIRA, and more advanced modules that let you provide advanced JIRA-specific functionality.

### Basic modules 

-   [Dialog]: shows content inside of a modal dialog
-   [Page]: displays content on a full screen page within JIRA
-   [Web panel]: displays content in a panel (like on the View Issue screen)
-   [Web item]: adds a link or button to a defined location in JIRA (usually used on conjunction with dialogs or pages)
-   [Web section]: defines a new section to add multiple web items
-   [Webhook]: see the [Webhooks] page for more information

### Basic module locations

-   [View issue page]
-   [Project sidebar]
-   [Administration console]
-   [System navigation bar]

### Advanced module locations

-   [JIRA Software locations]
-   [JIRA Service Desk locations]

 

### Advanced modules for JIRA

These advanced modules provide access to pre-defined locations or features in JIRA. 

-   [Dashboard item]: provides a new gadget to display on JIRA dashboards
-   [Entity property]: see the [JIRA Entity properties][Entity property] page for more information
-   [Global permission]: defines a new global permission in JIRA
-   [Project admin tab pane]l: adds a new page to JIRA project settings
-   [Project permission]: defines a new project-level permission in JIRA
-   [Report]: adds a new type of report to the JIRA reports page
-   [Search request view]: renders a custom view of a search result that's accessible from the JIRA issue navigator
-   [Tab panel]: adds panels to the JIRA project sidebar, user profile page, or view issue page
-   [Workflow post-function]: adds a new post-function to a JIRA workflow

  [JIRA Software UI modules]: /jiracloud/jira-software-ui-modules-39987281.html
  [JIRA Service Desk UI modules]: /jiracloud/jira-service-desk-ui-modules-39988267.html
  [Dialog]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/dialog.html
  [Page]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/page.html
  [Web panel]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-panel.html
  [Web item]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-item.html
  [Web section]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-section.html
  [Webhook]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/webhook.html
  [Webhooks]: /jiracloud/webhooks-39987038.html
  [View issue page]: https://developer.atlassian.com/jiracloud/jira-platform-modules-view-issue-page-39988380.html
  [Project sidebar]: https://developer.atlassian.com/jiradev/jira-platform/guides/projects/design-guide-jira-project-centric-view/development-guide-jira-project-centric-view
  [Administration console]: https://developer.atlassian.com/jiracloud/jira-platform-modules-administration-console-39988333.html
  [System navigation bar]: https://developer.atlassian.com/jiracloud/jira-platform-modules-user-accessible-locations-39988374.html
  [JIRA Software locations]: https://developer.atlassian.com/jiracloud/jira-software-ui-modules-39987281.html
  [JIRA Service Desk locations]: https://developer.atlassian.com/jiracloud/jira-service-desk-ui-modules-39988267.html
  [Dashboard item]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/dashboard-item.html
  [Entity property]: /jiracloud/jira-entity-properties-39988397.html
  [Global permission]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/global-permission.html
  [Project admin tab pane]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/project-admin-tab-panel.html
  [Project permission]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/project-permission.html
  [Report]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/report.html
  [Search request view]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/search-request-view.html
  [Tab panel]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/tab-panel.html
  [Workflow post-function]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/workflow-post-function.html

