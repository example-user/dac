---
title: JIRA Software Webhooks 39990316
aliases:
    - /jiracloud/jira-software-webhooks-39990316.html
---
# JIRA Cloud : JIRA Software webhooks

JIRA Software webhooks are simply JIRA webhooks that are registered for JIRA Software events. JIRA Software webhooks are configured in the same way that you configure JIRA platform webhooks. You can register them via the JIRA administration console, you can add them as a post function to a workflow, you can inject variables into the webhook URL, etc. 

To learn more, read the JIRA platform documentation on webhooks: **[Webhooks]**

 

 

  [Webhooks]: /jiracloud/webhooks-39987038.html

