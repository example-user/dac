---
title: Support 39988134
aliases:
    - /jiracloud/support-39988134.html
---
# JIRA Cloud : Support

Get help from other developers, provide us with feedback on what's working and not working for you, and give back to the community (and be recognised for it!). 

If you need help with anything else that is not covered by this page, get in touch with our Developer Relations team at: <https://developer.atlassian.com/help#contact-us>

## <img src="/jiracloud/attachments/39988134/39991453.png" class="image-center confluence-thumbnail" width="100" />Design

The <a href="https://design.atlassian.com/product/" class="external-link">Atlassian Design Guidelines</a> shape the look and feel of our products. The <a href="https://docs.atlassian.com/aui/latest/" class="external-link">Atlassian User Interface</a> is a front-end library to bring the ADG to life.

## <img src="/jiracloud/attachments/39988134/39991454.png" class="image-center confluence-thumbnail" width="100" />Discuss

Join the conversation with fellow add-on developers <a href="https://groups.google.com/forum/#!forum/atlassian-connect-dev" class="external-link">in our Google Group</a>, or ask a question to the community on <a href="https://answers.atlassian.com/questions/topics/754005/atlassian-connect" class="external-link">Atlassian answers</a>.

## <img src="/jiracloud/attachments/39988134/39991455.png" class="image-center confluence-thumbnail" width="100" />Get help

Stumped? No worries.
<a href="https://ecosystem.atlassian.net/servicedesk/customer/portal/12" class="external-link">Submit a developer support request </a>(or report a bug if you find one!)

Help a fellow developer

Want to give back? You can help us make the documentation better! Sign up for an [Atlassian contributor's licence] and you'll be able to update the developer documentation.

## Other resources

Here are some other resources for JIRA development:

**Developer resources:**

-   **<a href="https://www.atlassian.com/resources/experts" class="external-link">Atlassian Experts</a>**: Need a custom add-on, but don't want to build it yourself? Our experts can help.
-   **<a href="https://confluence.atlassian.com/display/JIRA/Building+JIRA+from+source" class="external-link">JIRA source</a>**: Browse the full JIRA source code for reference (commercial users only).
-   **<a href="https://marketplace.atlassian.com/plugins/com.atlassian.jira.plugins.jira-data-generator" class="external-link">JIRA Data Generator add-on</a>***: *Use this add-on to generate test data in JIRA for testing your add-on.
-   **[JIRA Server developer documentation]:** Browse the developer documentation for JIRA Server.

**JIRA product documentation**:

-   **<a href="https://confluence.atlassian.com/display/AdminJIRACloud/" class="external-link">JIRA administration (Cloud)</a>**
-   **<a href="https://confluence.atlassian.com/display/JIRASOFTWARECLOUD/" class="external-link">JIRA Software Cloud</a>**
-   **<a href="https://confluence.atlassian.com/display/SERVICEDESKCLOUD/" class="external-link">JIRA Service Desk Cloud</a>**
-   **<a href="https://confluence.atlassian.com/display/JIRACORECLOUD/" class="external-link">JIRA Core Cloud</a>**

  [Atlassian contributor's licence]: https://developer.atlassian.com/display/ABOUT/Atlassian+Contributor+License+Agreement
  [JIRA Server developer documentation]: https://developer.atlassian.com/display/JIRADEV

