---
title: JIRA Cloud for Developers 39375886
aliases:
    - /jiracloud/jira-cloud-for-developers-39375886.html
---
# JIRA Cloud : JIRA Cloud for Developers

## Build what's next for JIRA Cloud

##### Extend, enhance, and automate JIRA Software, JIRA Service Desk, and JIRA Core.

 
 

 

 

<img src="/jiracloud/attachments/39375886/39982976.png" class="image-center confluence-thumbnail" width="150" />

### Add-ons *in* JIRA

[Atlassian Connect] is a framework for building immersive add-ons that feel like they are part of JIRA.

-   Display [content inside of JIRA issues]

-   [Add gadgets] to JIRA dashboards

-   [Extend the JIRA Service Desk customer portal]

 

<a href="https://developer.atlassian.com/display/jiracloud/Getting+started" class="aui-button aui-button-primary">Build your first JIRA Cloud add-on</a>
[Show me the API]

 

 

------------------------------------------------------------------------

 

## JIRA Cloud Platform

##### Atlassian Connect and JIRA Cloud are the foundation for add-ons for any JIRA product.

 

 <img src="/jiracloud/attachments/39375886/39983055.png" class="confluence-thumbnail" width="150" />

[Atlassian Connect and JIRA overview]

<a href="https://docs.atlassian.com/jira/REST/cloud/" class="external-link">JIRA REST API</a>

[JIRA webhooks]

 

 

------------------------------------------------------------------------

 

## JIRA Cloud Products

### [<img src="/jiracloud/attachments/39375886/39983057.png" class="confluence-thumbnail" width="150" />]

### [JIRA Software Cloud][<img src="/jiracloud/attachments/39375886/39983057.png" class="confluence-thumbnail" width="150" />]

Build awesome add-ons for software teams with a dedicated API and webhooks for JIRA Software features like boards, backlogs, and estimation.

[JIRA Software overview for developers][<img src="/jiracloud/attachments/39375886/39983057.png" class="confluence-thumbnail" width="150" />]

<a href="https://docs.atlassian.com/jira-software/REST/cloud/" class="external-link">JIRA Software REST API</a>

[JIRA Software webhooks]

 

### [<img src="/jiracloud/attachments/39375886/39983058.png" class="confluence-thumbnail" width="150" />]

### [JIRA Service Desk Cloud][<img src="/jiracloud/attachments/39375886/39983058.png" class="confluence-thumbnail" width="150" />]

Build awesome add-ons for IT teams with a dedicated API and webhooks for JIRA Service Desk features like queues, SLAs, and the customer portal.

[JIRA Service Desk overview for developers][<img src="/jiracloud/attachments/39375886/39983058.png" class="confluence-thumbnail" width="150" />]

<a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/" class="external-link">JIRA Service Desk REST API</a>

[JIRA Service Desk webhooks]

 

------------------------------------------------------------------------

 

## Get help

##### Our team is ready to help, don't hesitate to get in touch.

 
<a href="https://design.atlassian.com/" class="external-link"><img src="/jiracloud/attachments/39375886/39983073.png" class="confluence-thumbnail" width="150" /></a>

 

### Design

The <a href="https://design.atlassian.com/product/" class="external-link">Atlassian Design Guidelines</a> shape the look and feel of our products. <a href="https://docs.atlassian.com/aui/latest/" class="external-link">Atlassian User Interface</a> is a front-end library to bring the ADG to life.

<a href="https://groups.google.com/forum/#!forum/atlassian-connect-dev" class="external-link"><img src="/jiracloud/attachments/39375886/39983074.png" class="confluence-thumbnail" width="150" /></a>

 

### Discuss

Join the conversation with fellow add-on developers <a href="https://groups.google.com/forum/#!forum/atlassian-connect-dev" class="external-link">in our Google Group</a>, or ask a question to the community on <a href="https://answers.atlassian.com/questions/topics/754005/atlassian-connect" class="external-link">Atlassian answers</a>.

<a href="https://ecosystem.atlassian.net/servicedesk/customer/portal/12" class="external-link"><img src="/jiracloud/attachments/39375886/39983075.png" class="confluence-thumbnail" width="150" /></a>

 

### Support

Stumped? No worries.
-   <a href="https://ecosystem.atlassian.net/servicedesk/customer/portal/12" class="external-link">Submit a developer support request for JIRA Platform and JIRA Software</a>(or report a bug if you find one!)
-   Submit a JIRA Service Desk developer support request

  [Atlassian Connect]: https://developer.atlassian.com/static/connect/docs/latest/index.html
  [content inside of JIRA issues]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-panel.html
  [Add gadgets]: https://developer.atlassian.com/static/connect/docs/latest/modules/jira/dashboard-item.html
  [Extend the JIRA Service Desk customer portal]: https://developer.atlassian.com/static/connect/docs/latest/tutorials/my-requests-tutorial.html
  [Show me the API]: https://developer.atlassian.com/jiracloud/jira-cloud-platform-rest-api-39987036.html
  [Atlassian Connect and JIRA overview]: /jiracloud/jira-cloud-development-platform-39981102.html
  [JIRA webhooks]: /jiracloud/webhooks-39987038.html
  [<img src="/jiracloud/attachments/39375886/39983057.png" class="confluence-thumbnail" width="150" />]: /jiracloud/jira-software-cloud-development-39981104.html
  [JIRA Software webhooks]: /jiracloud/jira-software-webhooks-39990316.html
  [<img src="/jiracloud/attachments/39375886/39983058.png" class="confluence-thumbnail" width="150" />]: /jiracloud/jira-service-desk-cloud-development-39981106.html
  [JIRA Service Desk webhooks]: /jiracloud/jira-service-desk-webhooks-39988324.html

