---
title: JIRA Service Desk Modules Agent View 39988009
aliases:
    - /jiracloud/jira-service-desk-modules-agent-view-39988009.html
---
# JIRA Cloud : JIRA Service Desk modules - Agent view

This pages lists the JIRA Service Desk modules for the agent view. These can be used to inject new groups (tabs) in the JIRA Service Desk agent view.

**On this page:**

-   [Queues area]
    -   [Queue group]
    -   [Queue]
-   [Reports area]
    -   [Report group]
    -   [Report]

# Queues area

## Queue group

Description
A group of [serviceDeskQueues][Queue]
Module type
    serviceDeskQueueGroups

Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988009/39990761.png" width="850" height="473" /> 

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

``` false;
"modules": {
    "serviceDeskQueueGroups": [ 
        { 
            "key": "my-custom-queues-section", 
            "name": { 
                "value": "My custom queues section" 
            }
        }
    ]
}
```

*![(warning)] Note that the group will not be displayed if there are no [serviceDeskQueues][Queue] that reference it in the "group" property or if all the queues that reference it are not displayed because of conditions.*

Properties
**`key`**
-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

 
**name**
-   **Type**: `i18n Property`
-   **Required**: yes
-   **Description**: A human readable name.

 
**`weight`**
-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

 
**`conditions`**
-   **Type**: \[ [ `Single Condition` ], [ `Composite Condition` ], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

 

## Queue

Description
A queue in the queues sidebar
Module type
    serviceDeskQueues

Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988009/39990761.png" width="850" height="473" /> 

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

``` false;
"modules": {
    "serviceDeskQueues": [ 
        { 
            "key": "my-custom-queue", 
            "name": { 
                "value": "My custom queue" 
            }, 
            "group": "my-custom-queues-section", 
            "url": "/sd-queue-content" 
        }
    ]
}
```

Properties
**`key`**
-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

**name**
-   **Type**: `i18n Property`
-   **Required**: yes
-   **Description**: A human readable name.

**`weight`**
-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`group`**
-   **Type**: `string`
-   **Required**: no
-   **Description**: References the key of a [serviceDeskQueueGroup][Queue group]. If this property is not provided, the queue will appear in a generic "Add-ons" group.

**`url`**
-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`conditions`**
-   **Type**: \[ [ `Single Condition` ], [ `Composite Condition` ], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

# Reports area

## Report group

Description
A group of [serviceDeskReports][Report]
Module type
    serviceDeskReportGroups

Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988009/39988243.png" width="850" height="473" />

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

``` false;
"modules": {
    "serviceDeskReportGroups": [ 
        { 
            "key": "my-custom-reports-section", 
            "name": { 
                "value": "My custom reports section" 
            }
        }
    ]
}
```

*![(warning)] Note that the group will not be displayed if there are no [serviceDeskReports][Report] that reference it in the "group" property or if all the reports that reference it are not displayed because of conditions.*

Properties
**`key`**
-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

 
**name**
-   **Type**: `i18n Property`
-   **Required**: yes
-   **Description**: A human readable name.

 
**`weight`**
-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

 
**`conditions`**
-   **Type**: \[ [ `Single Condition` ], [ `Composite Condition` ], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

 

## Report

Description
A report in the reports sidebar
Module type
    serviceDeskReports

Screenshot
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

<img src="/jiracloud/attachments/39988009/39988243.png" width="850" height="473" />

Sample JSON
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...

``` false;
"modules": {
    "serviceDeskReports": [ 
        { 
            "key": "my-custom-report", 
            "name": { 
                "value": "My custom report" 
            }, 
            "group": "my-custom-reports-section", 
            "url": "/sd-report-content" 
        }
    ]
}
```

Properties
**`key`**
-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

**name**
-   **Type**: `i18n Property`
-   **Required**: yes
-   **Description**: A human readable name.

**`weight`**
-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

**`group`**
-   **Type**: `string`
-   **Required**: no
-   **Description**: References the key of a [serviceDeskReportGroup][Report group]. If this property is not provided, the report will appear in a generic "Add-ons" group.

**`url`**
-   **Type**: `string, uri-template`
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

**`conditions`**
-   **Type**: \[ [ `Single Condition` ], [ `Composite Condition` ], … \]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

  [Queues area]: #JIRAServiceDeskmodules-Agentview-Queuesarea
  [Queue group]: #JIRAServiceDeskmodules-Agentview-Queuegroup
  [Queue]: #JIRAServiceDeskmodules-Agentview-Queue
  [Reports area]: #JIRAServiceDeskmodules-Agentview-Reportsarea
  [Report group]: #JIRAServiceDeskmodules-Agentview-Reportgroup
  [Report]: #JIRAServiceDeskmodules-Agentview-Report
  [(warning)]: /jiracloud/images/icons/emoticons/warning.png
  [ `Single Condition` ]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html
  [ `Composite Condition` ]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html
  [Conditions]: https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html
  [additional context]: https://developer.atlassian.com/display/jiracloud/JIRA+Service+Desk+modules+-+Customer+portal

