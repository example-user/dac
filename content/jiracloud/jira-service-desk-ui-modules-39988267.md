---
title: JIRA Service Desk UI Modules 39988267
aliases:
    - /jiracloud/jira-service-desk-ui-modules-39988267.html
---
# JIRA Cloud : JIRA Service Desk UI modules

JIRA modules allow add-ons to extend the functionality of the JIRA platform or a JIRA application. JIRA modules are commonly used to extend the user interface by adding links, panels, etc. However, some JIRA platform modules can also be used to extend other parts of JIRA, like permissions and workflows.

The JIRA platform and JIRA Software also have their own modules. For more information, see the following pages:

-   [JIRA platform modules]
-   [JIRA Software UI modules]

## Using JIRA Service Desk UI modules

You can use a JIRA Service Desk UI module by declaring it in your add-on descriptor (under `modules`), with the appropriate properties. For example, the following code adds the `serviceDeskPortalHeaders` module to your add-on, which injects a panel at the top of customer portal pages in JIRA Service Desk.

**atlassian-connect.json**

``` 22;
...
"modules": {
    "serviceDeskPortalHeaders": [
        {
            "key": "sd-portal-header",
            "url": "/sd-portal-header"
        }
    ]
}
...
```

## JIRA Service Desk UI modules

-   [JIRA Service Desk modules - Agent view]
-   [JIRA Service Desk modules - Customer portal]
-   [JIRA Service Desk Remote Action module]

  [JIRA platform modules]: /jiracloud/jira-platform-modules-39987040.html
  [JIRA Software UI modules]: /jiracloud/jira-software-ui-modules-39987281.html
  [JIRA Service Desk modules - Agent view]: /jiracloud/jira-service-desk-modules-agent-view-39988009.html
  [JIRA Service Desk modules - Customer portal]: /jiracloud/jira-service-desk-modules-customer-portal-39988271.html
  [JIRA Service Desk Remote Action module]: /jiracloud/jira-service-desk-remote-action-module-41222507.html

