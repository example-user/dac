---
title: JIRA Software Modules Boards 39990330
aliases:
    - /jiracloud/jira-software-modules-boards-39990330.html
---
# JIRA Cloud : JIRA Software modules - Boards

This pages lists the JIRA Software modules for boards. 

**On this page:**

-   [Board area]
-   [Board configuration]

## Board area

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>Adds a dropdown menu to a board, next to the <strong>Boards</strong> menu.</td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webSection + webPanel</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>jira.agile.board.tools</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-1808353162" class="expand-container">
<div id="expander-control-1808353162" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39990330/39990323.png" width="850" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-1359620165" class="expand-container">
<div id="expander-control-1359620165" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code>...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;board-links&quot;,
            &quot;location&quot;: &quot;jira.agile.board.tools&quot;,
          &quot;weight&quot;: 10,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;My Extension&quot;
            }
        }
    ],   
    &quot;webPanels&quot;: [
        {
            &quot;key&quot;: &quot;my-web-panel&quot;,
            &quot;url&quot;: &quot;web-panel?id={board.id}&amp;mode={board.screen}&quot;,
            &quot;location&quot;: &quot;board-links&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;My Web Panel&quot;
            },</code></pre>
<pre><code>           &quot;layout&quot;: {</code></pre>
<pre><code>              &quot;width&quot;: &quot;100px&quot;,</code></pre>
<pre><code>                &quot;height&quot;: &quot;100px&quot;</code></pre>
<pre><code>            }</code></pre>
<pre><code>        }
    ]
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Board configuration

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>Adds a board configuration page.</td>
</tr>
<tr class="even">
<td>Module type</td>
<td><code>webPanel</code></td>
</tr>
<tr class="odd">
<td>Location key</td>
<td><code>jira.agile.board.configuration</code></td>
</tr>
<tr class="even">
<td>Screenshot</td>
<td><div id="expander-1647543207" class="expand-container">
<div id="expander-control-1647543207" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<p><img src="/jiracloud/attachments/39990330/39990327.png" width="850" /></p>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Sample JSON</td>
<td><div class="content-wrapper">
<div id="expander-376518213" class="expand-container">
<div id="expander-control-376518213" class="expand-control">
<img src="/jiracloud/images/icons/grey_arrow_down.png" class="expand-control-image" />Show me...
</div>
<div id="expander-content-$toggleId" class="expand-content">
<pre><code>...
&quot;modules&quot;: {       
    &quot;webPanels&quot;: [
        {
            &quot;key&quot;: &quot;my-configuration-page&quot;,
            &quot;url&quot;: &quot;configuration?id={board.id}&amp;type={board.type}&quot;,
            &quot;location&quot;: &quot;jira.agile.board.configuration&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;My board configuration page&quot;
            },</code></pre>
<pre><code>           &quot;weight&quot;: 1
        }
    ]
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

  [Board area]: #JIRASoftwaremodules-Boards-Boardarea
  [Board configuration]: #JIRASoftwaremodules-Boards-Boardconfiguration

